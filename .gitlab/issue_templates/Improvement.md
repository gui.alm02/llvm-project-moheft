<!--
PLEASE READ THIS!

Before opening a new issue, make sure to search for keywords in the issues list 
filtered by the "Improvement" label:

https://gitlab.com/brcloud/llvm-project/-/issues?label_name=Improvement

and verify the issue you're about to submit isn't a duplicate.

Also, DO NOT FORGET to:

- Assign the issue to yourself or another responsible
- Link the issue to a project milestone
- Add all necessary labels
- Add a due date
-->

### Summary

<!--
Summarize the improvement that the resolution of this issue would be adding to
the project as a whole
-->

### Description

<!--
If needed, better describe the improvement adding any relevant information
-->

### Possible comparisons

<!--
If needed, list which comparisons should be made once the improvement is
developed in order to mark this issue as resolved.

Examples of comparison can be:

- [ ] Performance comparison using tool X
- [ ] Memory usage comparison using tool Y
-->

### Tasks

<!-- 
If needed, list the tasks needed to complete the described improvement.

Use the following template in order to better keep track of the issue progress:

- [ ] Task 1
- [ ] Task 2
  - [ ] Sub-task 2.1
-->

### New tests

<!--
If necessary, compile a list of new tests that could ensure that the
improvement is working properly
-->

### References

<!--
If necessary, add a proper list of references that can backup your issue
description. Use the following as an example:

1. Names of the authors, Title, Publication reference
2. Website title, Date of access, Website link

Remember to link the reference to someplace in your issue description
-->

Related Issues: <!-- List of related issues in [#<issue_number>,] form -->

/label ~"To Do" ~"Improvement"
