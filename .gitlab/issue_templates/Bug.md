<!--
PLEASE READ THIS!

Before opening a new issue, make sure to search for keywords in the issues list 
filtered by the "Bug" label:

https://gitlab.com/brcloud/llvm-project/-/issues?label_name=Bug

and verify the issue you're about to submit isn't a duplicate.

Also, DO NOT FORGET to:

- Assign the issue to yourself or another responsible
- Link the issue to a project milestone
- Add all necessary labels
- Add a due date
-->

### Summary

<!-- Summarize the bug encountered concisely -->

### Steps to reproduce

<!--
THIS IS VERY IMPORTANT:

Give step-by-step instructions on how to reproduce the bug. Such steps can
include, but are not limited to:

- Tested commit hash (other information like branch name and issue are also
  welcome, but the commit hash is the most important)
- How the clang and the OpenMP runtime were compiled
- The test programs that were used alongside its inputs
- The wrong output or crash that was noticed 
-->

### Outputs

<!-- 
If possible, compress any relevant output files and add it to this section.

If there is any previous expected results, please add them here as well.
-->

### New tests

<!--
If possible, compile a list of new tests that could be developed in order
to this bug not come back in the future.
-->

<!--
Issue-bug track represents the point in the project when the bug was most
possibly introduced, allowing for a better understanding of the development
context at that time.

If this information is not easily obtainable, one can use from features like git
bisect to find it.
-->
Issue-bug track: #<!--Issue Number--> (<!--Commit Hash-->)
Related Issues: <!-- List of related issues in [#<issue_number>,] form -->

/label ~"To Do" ~"Bug"
