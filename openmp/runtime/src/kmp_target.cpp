/*
 * kmp_target.cpp -- OpenMP integration with target library
 */

//===----------------------------------------------------------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#include "kmp.h"

#include <cassert>
#include <cstdint>

#include <iostream>

#include <algorithm>
#include <atomic>
#include <mutex>
#include <unordered_map>
#include <list>
#include <functional>

#define assertm(exp, msg) assert(((void)msg, exp));

// Address dep collision structure
typedef struct addr_ref {
  kmp_target_task_t *last_out = nullptr;
  std::list<kmp_target_task_t *> last_ins;
} addr_ref_t;

// Global graph variables and configurations
// =============================================================================
using TaskPtrMap = std::unordered_map<kmp_int32, kmp_target_task_t *>;
struct TargetTaskContext {
  // Current active tasks
  std::unordered_map<kmp_int32, kmp_target_task_t> active_tasks;

  // Current root tasks in the task graph
  TaskPtrMap root_tasks;
  // Current dependency history referenced by the active tasks
  std::unordered_map<kmp_intptr_t, addr_ref_t> dep_history;
  // Hash map of the tasks copied to each task map created the user. Used during
  // the task creation and remove process.
  std::unordered_map<kmp_target_task_map_t *, TaskPtrMap> user_hash_map;

  // Global thread id of the current thread that is creating the graph.
  int graph_builder_gtid;

  // Mutex used to protect this structure.
  std::mutex ctx_mutex;
};
static TargetTaskContext *gcontext = nullptr;
static std::atomic<int> gcontext_referece_count(0);

// Initialize and finalize the target task integration
// =============================================================================
__attribute__((constructor)) static void init_target_task_context() {
  int old_count = gcontext_referece_count++;

  if (old_count == 0) {
    gcontext = new TargetTaskContext;
    gcontext->graph_builder_gtid = -1;
  } else {
    // The other threads must wait for `gcontext` to be constructed. Since
    // this only happens when more than one thread enters this function
    // at once, busy waiting should be enough.
    while (gcontext == nullptr);
  }

  KA_TRACE(20, ("init_target_task_context: new user registered, currently %d "
                "users depend on target-task integration",
                old_count + 1));
}

__attribute__((destructor)) static void deinit_target_task_context() {
  int old_count = gcontext_referece_count--;
  assert(old_count > 0);

  if (old_count == 1 && gcontext != nullptr) {
    delete gcontext;
  }

  KA_TRACE(20, ("deinit_target_task_context: user unregistered, currently %d "
                "users depend on target-task integration",
                old_count - 1));
}

// Helper functions
// =============================================================================
template <typename T> static T *thread_malloc(kmp_info_t *thread, size_t size) {
  // Use fast user space allocation if an OpenMP thread context is available.
  if (thread != nullptr) {
#if USE_FAST_MEMORY
    return (T *)__kmp_fast_allocate(thread, size * sizeof(T));
#else
    return (T *)__kmp_thread_malloc(thread, size * sizeof(T));
#endif
  }

  // Fallback to normal allocation otherwise.
  return (T *)__kmp_allocate(size * sizeof(T));
}

static void thread_free(kmp_info_t *thread, void *ptr) {
  // Use fast user space de-allocation if an OpenMP thread context is available.
  if (thread != nullptr) {
#if USE_FAST_MEMORY
    __kmp_fast_free(thread, ptr);
#else
    __kmp_thread_free(thread, ptr);
#endif
  } else {
    // Fallback to normal de-allocation otherwise.
    __kmp_free(ptr);
  }
}

static void add_to_succ_list(kmp_info_t *thread, kmp_target_task_t *target_task,
                             kmp_target_task_t *succ_task,
                             bool update_pred = true) {
  // Add `succ_task` to the list of successors of `target_task`
  auto *succ = thread_malloc<kmp_target_task_list_node_t>(thread, 1);
  succ->task = succ_task;
  succ->next = target_task->successors;
  target_task->successors = succ;

  // Add `target_task` to the list of predecessors of `succ_task`
  auto *pred = thread_malloc<kmp_target_task_list_node_t>(thread, 1);
  pred->task = target_task;
  pred->next = succ_task->predecessors;
  succ_task->predecessors = pred;

  if (update_pred) {
    succ_task->num_predecessors++;
  }
}

/// Remove \p pred_task from the predecessor list of \p task and free the node.
///
/// This function expects \p pred_task to actually be in the list of
/// predecessors, otherwise it will result in a SEGFAULT.
static void remove_from_pred_list(kmp_info_t *thread, kmp_target_task_t *task,
                                  kmp_target_task *pred_task) {
  kmp_target_task_list_node_t *prev = nullptr;
  kmp_target_task_list_node_t *curr = task->predecessors;

  assertm(task != nullptr && pred_task != nullptr,
          "task and pred_task cannot be null");

  while (curr != nullptr && curr->task != pred_task) {
    prev = curr;
    curr = curr->next;
  }

  assertm(curr != nullptr && curr->task == pred_task,
          "Expected to find predecessor task in the current list");

  if (prev == nullptr) {
    // If prev is null, it means that the task we are looking for is the first
    // in the list, so we modify `task->predecessors` directly.
    task->predecessors = curr->next;
  } else {
    // Otherwise make the previous node in the list point to the next of the
    // current (which we are removing).
    prev->next = curr->next;
  }

  thread_free(thread, curr);
}

// Returns the thread info of the current OpenMP thread. If the caller is not an
// OpenMP thread, returns nullptr.
static kmp_info_t *get_omp_thread() {
  // Get the current omp global thread id (valid if gtid >= 0).
  const int omp_gtid = __kmp_get_gtid();

  if (omp_gtid >= 0) {
    return __kmp_thread_from_gtid(omp_gtid);
  }

  return nullptr;
}

// Target task data
// =============================================================================
kmp_target_task_data::kmp_target_task_data(kmp_int64 device_id_p,
                                           void *outlined_fn_id_p,
                                           kmp_int32 num_args_p,
                                           void **args_base_p, void **args_p,
                                           kmp_int64 *arg_sizes_p,
                                           kmp_int64 *arg_types_p)
    : device_id(device_id_p), outlined_fn_id(outlined_fn_id_p),
      num_args(num_args_p) {
  allocBuffers();
  copyBuffers(args_base_p, args_p, arg_sizes_p, arg_types_p);
}

kmp_target_task_data::~kmp_target_task_data() { deleteBuffers(); }

void *kmp_target_task_data::operator new(size_t size) {
  // TODO: Check why we cannot use thread fast allocation without getting
  // address errors when freeing them.
  return __kmp_allocate(size);
}

void kmp_target_task_data::operator delete(void *ptr) { __kmp_free(ptr); }

kmp_target_task_data &
kmp_target_task_data::operator=(const kmp_target_task_data &other) {
  // Skip self assignment.
  if (this == &other) {
    return *this;
  }

  // Realloc internal arrays on size change.
  if (num_args != other.num_args) {
    deleteBuffers();
    num_args = other.num_args;
    allocBuffers();
  }

  // Assign and copy new data from other.
  device_id = other.device_id;
  outlined_fn_id = other.outlined_fn_id;
  copyBuffers(other.args_base, other.args, other.arg_sizes, other.arg_types);

  return *this;
}

void kmp_target_task_data::allocBuffers() {
  if (num_args > 0) {
    // TODO: Check why we cannot use thread fast allocation without getting
    // address errors when freeing them.
    args_base = (void **)__kmp_allocate(sizeof(void *) * num_args);
    args = (void **)__kmp_allocate(sizeof(void *) * num_args);
    arg_sizes = (kmp_int64 *)__kmp_allocate(sizeof(kmp_int64) * num_args);
    arg_types = (kmp_int64 *)__kmp_allocate(sizeof(kmp_int64) * num_args);
  }
}

void kmp_target_task_data::deleteBuffers() {
  if (num_args > 0) {
    __kmp_free(args_base);
    __kmp_free(args);
    __kmp_free(arg_sizes);
    __kmp_free(arg_types);
  }
  args_base = nullptr;
  args = nullptr;
  arg_sizes = nullptr;
  arg_types = nullptr;
}

void kmp_target_task_data::copyBuffers(void **args_base_p, void **args_p,
                                       kmp_int64 *arg_sizes_p,
                                       kmp_int64 *arg_types_p) {
  if (num_args > 0) {
    std::copy_n(args_base_p, num_args, args_base);
    std::copy_n(args_p, num_args, args);
    std::copy_n(arg_sizes_p, num_args, arg_sizes);
    std::copy_n(arg_types_p, num_args, arg_types);
  }
}

// Task and graph management
// =============================================================================
void __kmpc_add_target_task_with_deps(
    const kmp_taskdata_t *taskdata, kmp_int32 ndeps,
    const kmp_depend_info_t *dep_list, kmp_int32 ndeps_noalias,
    const kmp_depend_info_t *dep_list_noalias) {

  std::unique_lock<std::mutex> ctx_lk(gcontext->ctx_mutex);

  assertm(taskdata != nullptr, "Invalid taskdata pointer");
  assertm((ndeps > 0) == (dep_list != nullptr), "Invalid dependencies list");
  assertm((ndeps_noalias > 0) == (dep_list_noalias != nullptr),
          "Invalid no-alias dependencies list");
  assertm(gcontext->active_tasks.count(taskdata->td_task_id) == 0,
          "Same task added multiple times to the target task graph");

  // Get general data
  kmp_info_t *thread = get_omp_thread();
  assertm(thread != nullptr,
          "Function should be called inside an OpenMP thread");

  // Set the new graph creator
  int gtid = __kmp_gtid_from_thread(thread);
  if (gcontext->graph_builder_gtid != gtid) {
    gcontext->graph_builder_gtid = gtid;
    KA_TRACE(20, ("__kmpc_add_target_task_with_deps: T#%d is the new "
                  "target-task graph builder",
                  gtid));
  }

  // Create target task structure
  const kmp_int32 task_id = taskdata->td_task_id;

  kmp_target_task_t &new_task = gcontext->active_tasks[taskdata->td_task_id];
  new_task.task_id = task_id;
  new_task.is_target_task = (taskdata->td_target_info != nullptr);
  if (new_task.is_target_task) {
    new_task.target_data = *(taskdata->td_target_info);
  }

  // Process dependencies
  new_task.deps =
      thread_malloc<kmp_target_dep_t>(thread, ndeps + ndeps_noalias);
  new_task.num_deps = 0;

  // Filter aliased addresses and add dependencies to task
  for (int i = 0; i < ndeps; ++i) {
    const kmp_depend_info_t &dep = dep_list[i];
    bool is_alias = false;

    for (int j = 0; j < new_task.num_deps; ++j) {
      kmp_target_dep_t &prev_dep = new_task.deps[j];
      if (prev_dep.addr == dep.base_addr) {
        prev_dep.dep_type.in |= dep.flags.in;
        prev_dep.dep_type.out |= dep.flags.out;
        prev_dep.dep_type.mtx |= dep.flags.mtx;
        is_alias = true;
        break;
      }
    }

    if (!is_alias) {
      new_task.deps[new_task.num_deps++] = kmp_target_dep_t{
          .addr = dep.base_addr,
          .dep_type = {.in = static_cast<bool>(dep.flags.in),
                       .out = static_cast<bool>(dep.flags.out),
                       .mtx = static_cast<bool>(dep.flags.mtx)}};
    }
  }

  // Add no-alias dependencies
  for (int i = 0; i < ndeps_noalias; ++i) {
    const kmp_depend_info_t &dep = dep_list_noalias[i];
    new_task.deps[new_task.num_deps + i] =
        kmp_target_dep_t{.addr = dep.base_addr,
                         .dep_type = {.in = static_cast<bool>(dep.flags.in),
                                      .out = static_cast<bool>(dep.flags.out),
                                      .mtx = static_cast<bool>(dep.flags.mtx)}};
  }
  new_task.num_deps += ndeps_noalias;

  // Add task to the graph
  for (int i = 0; i < new_task.num_deps; ++i) {
    const kmp_target_dep_t &dep = new_task.deps[i];
    addr_ref_t &ref = gcontext->dep_history[dep.addr];

    if (dep.dep_type.out) {
      // If we have an out dep, link the previous in tasks as our predecessors
      // or, if no in deps is available, link the previous out dep.
      for (auto &in_task : ref.last_ins) {
        add_to_succ_list(thread, in_task, &new_task);
      }

      if (ref.last_ins.empty() && (ref.last_out != nullptr)) {
        add_to_succ_list(thread, ref.last_out, &new_task);
      }

      ref.last_ins.clear();
      ref.last_out = &new_task;
    } else if (dep.dep_type.in) {
      // If we have an in dep, add it to the ins list and link the previous out
      // dep as our predecessor.
      assert(dep.dep_type.in);

      if (ref.last_out != nullptr) {
        add_to_succ_list(thread, ref.last_out, &new_task);
      }

      ref.last_ins.push_front(&new_task);
    }

    // Note: Mtx deps are not considered when constructing the graph and should
    // be handled by the graph user.
  }

  if (new_task.num_predecessors == 0) {
    gcontext->root_tasks.emplace(task_id, &new_task);
  }
}

void __kmpc_remove_root_target_task(kmp_int32 task_id) {
  // Only one OpenMP thread can remove a task each time
  std::unique_lock<std::mutex> ctx_lk(gcontext->ctx_mutex);

  // This function can be called for tasks without dependencies. Immediately
  // return in these cases.
  if (gcontext->active_tasks.count(task_id) == 0) {
    return;
  }
  assertm(gcontext->root_tasks.count(task_id) == 1,
          "Trying to remove non-root task");

  // Get general data
  kmp_info_t *thread = get_omp_thread();
  assertm(thread != nullptr,
          "Function should be called inside an OpenMP thread");

  // Remove from root map, clear successors list and find new root tasks
  gcontext->root_tasks.erase(task_id);
  kmp_target_task_t &task = gcontext->active_tasks[task_id];
  while (task.successors != nullptr) {
    kmp_target_task_list_node_t *succ = task.successors;
    task.successors = task.successors->next;

    kmp_target_task_t *succ_task = succ->task;
    succ_task->num_predecessors--;
    if (succ_task->num_predecessors == 0) {
      gcontext->root_tasks.emplace(succ_task->task_id, succ_task);
    }

    remove_from_pred_list(thread, succ_task, &task);
    thread_free(thread, succ);
  }

  // Remove any address reference
  for (int i = 0; i < task.num_deps; ++i) {
    auto addr_ref = gcontext->dep_history.find(task.deps[i].addr);
    if (addr_ref != gcontext->dep_history.end()) {
      addr_ref_t &ref = addr_ref->second;

      if (ref.last_out == &task) {
        ref.last_out = nullptr;
      }

      ref.last_ins.remove(&task);

      // If the removed reference is empty, remove it from the history
      if (ref.last_out == nullptr && ref.last_ins.empty()) {
        gcontext->dep_history.erase(addr_ref);
      }
    }
  }
  thread_free(thread, task.deps);

  gcontext->active_tasks.erase(task_id);
}

// Task graph access functions
// =============================================================================
kmp_target_task_map_t *__kmpc_target_task_map_create() {
  // Init global target task context
  init_target_task_context();

  // Guarantee that no task is being removed when someone creates a task map.
  // This also synchronizes the access to the 'user_hash_map'
  std::unique_lock<std::mutex> ctx_lk(gcontext->ctx_mutex);

  if (gcontext->active_tasks.empty()) {
    return nullptr;
  }

  // Allocate user task map data
  auto *user_map = thread_malloc<kmp_target_task_map_t>(nullptr, 1);
  gcontext->user_hash_map.emplace(user_map, TaskPtrMap());

  user_map->num_tasks = gcontext->active_tasks.size();
  user_map->tasks =
      thread_malloc<kmp_target_task_t>(nullptr, user_map->num_tasks);

  user_map->num_roots = gcontext->root_tasks.size();
  user_map->roots =
      thread_malloc<kmp_target_task_t *>(nullptr, user_map->num_roots);

  // Copy tasks and set roots
  int task_idx = 0;
  int root_idx = 0;

  for (const auto &task_hash_item : gcontext->active_tasks) {
    const kmp_target_task_t &source_task = task_hash_item.second;
    assert(task_idx < user_map->num_tasks);

    // Copy task data
    kmp_target_task_t &user_task = user_map->tasks[task_idx++];
    user_task = source_task;

    user_task.successors = nullptr;
    user_task.predecessors = nullptr;

    user_task.deps =
        thread_malloc<kmp_target_dep_t>(nullptr, user_task.num_deps);
    std::copy_n(source_task.deps, source_task.num_deps, user_task.deps);

    gcontext->user_hash_map[user_map][user_task.task_id] = &user_task;

    if (gcontext->root_tasks.count(user_task.task_id) == 1) {
      assert(root_idx < user_map->num_roots);
      user_map->roots[root_idx++] = &user_task;
    }
  }

  // Copy graph structure
  for (task_idx = 0; task_idx < user_map->num_tasks; ++task_idx) {
    kmp_target_task_t &user_task = user_map->tasks[task_idx];
    assert(gcontext->active_tasks.count(user_task.task_id) == 1);
    const kmp_target_task_t &source_task =
        gcontext->active_tasks[user_task.task_id];

    kmp_target_task_list_node_t *succ = source_task.successors;
    while (succ != nullptr) {
      assert(succ->task != nullptr);
      assert(gcontext->user_hash_map[user_map].count(succ->task->task_id) == 1);

      kmp_target_task_t *succ_task =
          gcontext->user_hash_map[user_map][succ->task->task_id];
      add_to_succ_list(nullptr, &user_task, succ_task, false);

      succ = succ->next;
    }
  }

  return user_map;
}

void __kmpc_target_task_map_free(kmp_target_task_map_t *task_map) {
  // Synchronize the access to the 'user_hash_map'
  std::unique_lock<std::mutex> ctx_lk(gcontext->ctx_mutex);

  if (task_map == nullptr) {
    return;
  }

  assertm(gcontext->user_hash_map.count(task_map) == 1,
          "Invalid task map pointer");
  gcontext->user_hash_map.erase(task_map);

  for (int i = 0; i < task_map->num_tasks; ++i) {
    kmp_target_task_t &task = task_map->tasks[i];

    thread_free(nullptr, task.deps);

    while (task.successors != nullptr) {
      kmp_target_task_list_node_t *node = task.successors;
      task.successors = node->next;
      thread_free(nullptr, node);
    }
  }

  thread_free(nullptr, task_map->roots);
  thread_free(nullptr, task_map->tasks);

  thread_free(nullptr, task_map);

  // Deinit global target task context
  deinit_target_task_context();
}

kmp_target_task_t *__kmpc_target_task_map_get(kmp_target_task_map_t *task_map,
                                              kmp_int32 task_id) {
  // Synchronize the access to the 'user_hash_map'
  std::unique_lock<std::mutex> ctx_lk(gcontext->ctx_mutex);

  if (task_map == nullptr) {
    return nullptr;
  }

  if (gcontext->user_hash_map.count(task_map) == 0) {
    return nullptr;
  }

  if (gcontext->user_hash_map[task_map].count(task_id) == 0) {
    return nullptr;
  }

  return gcontext->user_hash_map[task_map][task_id];
}

// Misc. functions
// =============================================================================
kmp_int32 __kmpc_target_task_current_task_id() {
  const kmp_info_t *thread = get_omp_thread();

  if (thread == nullptr) {
    return -1;
  }

  const kmp_taskdata_t *task = thread->th.th_current_task;

  // Only explicit tasks can be created by the user and should be accounted.
  // Implicit tasks are automatically generated by the OpenMP Runtime and should
  // not be considered in the target-task integration.
  if (task == nullptr || task->td_flags.tasktype != TASK_EXPLICIT) {
    return -1;
  }

  return thread->th.th_current_task->td_task_id;
}

bool __kmpc_is_graph_builder_tasking() {
  if (get_omp_thread() == nullptr || gcontext->graph_builder_gtid < 0) {
    return false;
  }

  const kmp_info_t *builder_thread =
      __kmp_thread_from_gtid(gcontext->graph_builder_gtid);

  if (builder_thread == nullptr) {
    return false;
  }

  // The builder thread is executing explicit (user generated) tasks only at
  // task level 1 or above. Further tasking levels indicate tasks created
  // inside other explicit tasks and should also be accounted for.
  return builder_thread->th.th_tasking_level;
}
