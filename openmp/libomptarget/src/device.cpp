//===--------- device.cpp - Target independent OpenMP target RTL ----------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Functionality for managing devices that are handled by RTL plugins.
//
//===----------------------------------------------------------------------===//

#include "device.h"
#include "omptarget.h"
#include "private.h"
#include "rtl.h"
#include "scheduler.h"

#include <algorithm>
#include <cassert>
#include <climits>
#include <cstdint>
#include <cstdio>
#include <optional>
#include <string>
#include <thread>
#include <utility>

static const char *toString(MapTypeTy MapType) {
  switch (MapType) {
  case MapTypeTy::LOCAL:
    return "LOCAL";
  case MapTypeTy::PROXY:
    return "PROXY";
  case MapTypeTy::LINK:
    return "LINK";
  }
}

int HostDataToTargetTy::addEventIfNecessary(DeviceTy &Device,
                                            AsyncInfoTy &AsyncInfo) const {
  // First, check if the user disabled atomic map transfer/malloc/dealloc.
  if (!PM->UseEventsForAtomicTransfers)
    return OFFLOAD_SUCCESS;

  void *Event = getEvent();
  bool NeedNewEvent = Event == nullptr;
  if (NeedNewEvent && Device.createEvent(&Event) != OFFLOAD_SUCCESS) {
    REPORT("Failed to create event\n");
    return OFFLOAD_FAIL;
  }

  // We cannot assume the event should not be nullptr because we don't
  // know if the target support event. But if a target doesn't,
  // recordEvent should always return success.
  if (Device.recordEvent(Event, AsyncInfo) != OFFLOAD_SUCCESS) {
    REPORT("Failed to set dependence on event " DPxMOD "\n", DPxPTR(Event));
    return OFFLOAD_FAIL;
  }

  if (NeedNewEvent)
    setEvent(Event);

  return OFFLOAD_SUCCESS;
}

DeviceTy::DeviceTy(RTLInfoTy *RTL)
    : DeviceID(-1), RTL(RTL), RTLDeviceID(-1), IsInit(false), InitFlag(),
      HasPendingGlobals(false), PendingCtorsDtors(), ShadowPtrMap(),
      PendingGlobalsMtx(), ShadowMtx(), ExecCount(0), ProxyDevice(nullptr),
      IsProxyDevice(false) {}

DeviceTy::~DeviceTy() {
  if (DeviceID == -1 || !(getInfoLevel() & OMP_INFOTYPE_DUMP_TABLE))
    return;

  ident_t Loc = {0, 0, 0, 0, ";libomptarget;libomptarget;0;0;;"};
  dumpTargetPointerMappings(&Loc, *this);
}

int DeviceTy::associatePtr(void *HstPtrBegin, void *TgtPtrBegin, int64_t Size) {
  HDTTMapAccessorTy HDTTMap = HostDataToTargetMap.getExclusiveAccessor();

  // Check if entry exists
  auto It = HDTTMap->find(HstPtrBegin);
  if (It != HDTTMap->end()) {
    HostDataToTargetTy &HDTT = *It->HDTT;
    // Mapping already exists
    bool IsValid = HDTT.HstPtrEnd == (uintptr_t)HstPtrBegin + Size &&
                   HDTT.TgtPtrBegin == (uintptr_t)TgtPtrBegin;
    if (IsValid) {
      DP("Attempt to re-associate the same device ptr+offset with the same "
         "host ptr, nothing to do\n");
      return OFFLOAD_SUCCESS;
    }
    REPORT("Not allowed to re-associate a different device ptr+offset with "
           "the same host ptr\n");
    return OFFLOAD_FAIL;
  }

  // Mapping does not exist, allocate it with refCount=INF
  const HostDataToTargetTy &NewEntry =
      *HDTTMap
           ->emplace(new HostDataToTargetTy(
               /*HstPtrBase=*/(uintptr_t)HstPtrBegin,
               /*HstPtrBegin=*/(uintptr_t)HstPtrBegin,
               /*HstPtrEnd=*/(uintptr_t)HstPtrBegin + Size,
               /*TgtPtrBegin=*/(uintptr_t)TgtPtrBegin,
               /*UseHoldRefCount=*/false, /*Name=*/nullptr,
               /*IsRefCountINF=*/true))
           .first->HDTT;
  DP("Creating new map entry: HstBase=" DPxMOD ", HstBegin=" DPxMOD
     ", HstEnd=" DPxMOD ", TgtBegin=" DPxMOD ", DynRefCount=%s, "
     "HoldRefCount=%s\n",
     DPxPTR(NewEntry.HstPtrBase), DPxPTR(NewEntry.HstPtrBegin),
     DPxPTR(NewEntry.HstPtrEnd), DPxPTR(NewEntry.TgtPtrBegin),
     NewEntry.dynRefCountToStr().c_str(), NewEntry.holdRefCountToStr().c_str());
  (void)NewEntry;

  return OFFLOAD_SUCCESS;
}

int DeviceTy::disassociatePtr(void *HstPtrBegin) {
  HDTTMapAccessorTy HDTTMap = HostDataToTargetMap.getExclusiveAccessor();

  auto It = HDTTMap->find(HstPtrBegin);
  if (It != HDTTMap->end()) {
    HostDataToTargetTy &HDTT = *It->HDTT;
    // Mapping exists
    if (HDTT.getHoldRefCount()) {
      // This is based on OpenACC 3.1, sec 3.2.33 "acc_unmap_data", L3656-3657:
      // "It is an error to call acc_unmap_data if the structured reference
      // count for the pointer is not zero."
      REPORT("Trying to disassociate a pointer with a non-zero hold reference "
             "count\n");
    } else if (HDTT.isDynRefCountInf()) {
      DP("Association found, removing it\n");
      void *Event = HDTT.getEvent();
      delete &HDTT;
      if (Event)
        destroyEvent(Event);
      HDTTMap->erase(It);
      return OFFLOAD_SUCCESS;
    } else {
      REPORT("Trying to disassociate a pointer which was not mapped via "
             "omp_target_associate_ptr\n");
    }
  } else {
    REPORT("Association not found\n");
  }

  // Mapping not found
  return OFFLOAD_FAIL;
}

LookupResult DeviceTy::lookupMapping(HDTTMapAccessorTy &HDTTMap,
                                     void *HstPtrBegin, int64_t Size) {

  uintptr_t HP = (uintptr_t)HstPtrBegin;
  LookupResult LR;

  DP("Looking up mapping(HstPtrBegin=" DPxMOD ", Size=%" PRId64 ")...\n",
     DPxPTR(HP), Size);

  if (HDTTMap->empty())
    return LR;

  auto Upper = HDTTMap->upper_bound(HP);

  if (Size == 0) {
    // specification v5.1 Pointer Initialization for Device Data Environments
    // upper_bound satisfies
    //   std::prev(upper)->HDTT.HstPtrBegin <= hp < upper->HDTT.HstPtrBegin
    if (Upper != HDTTMap->begin()) {
      LR.Entry = std::prev(Upper)->HDTT;
      auto &HT = *LR.Entry;
      // the left side of extended address range is satisified.
      // hp >= HT.HstPtrBegin || hp >= HT.HstPtrBase
      LR.Flags.IsContained = HP < HT.HstPtrEnd || HP < HT.HstPtrBase;
    }

    if (!LR.Flags.IsContained && Upper != HDTTMap->end()) {
      LR.Entry = Upper->HDTT;
      auto &HT = *LR.Entry;
      // the right side of extended address range is satisified.
      // hp < HT.HstPtrEnd || hp < HT.HstPtrBase
      LR.Flags.IsContained = HP >= HT.HstPtrBase;
    }
  } else {
    // check the left bin
    if (Upper != HDTTMap->begin()) {
      LR.Entry = std::prev(Upper)->HDTT;
      auto &HT = *LR.Entry;
      // Is it contained?
      LR.Flags.IsContained = HP >= HT.HstPtrBegin && HP < HT.HstPtrEnd &&
                             (HP + Size) <= HT.HstPtrEnd;
      // Does it extend beyond the mapped region?
      LR.Flags.ExtendsAfter = HP < HT.HstPtrEnd && (HP + Size) > HT.HstPtrEnd;
    }

    // check the right bin
    if (!(LR.Flags.IsContained || LR.Flags.ExtendsAfter) &&
        Upper != HDTTMap->end()) {
      LR.Entry = Upper->HDTT;
      auto &HT = *LR.Entry;
      // Does it extend into an already mapped region?
      LR.Flags.ExtendsBefore =
          HP < HT.HstPtrBegin && (HP + Size) > HT.HstPtrBegin;
      // Does it extend beyond the mapped region?
      LR.Flags.ExtendsAfter = HP < HT.HstPtrEnd && (HP + Size) > HT.HstPtrEnd;
    }

    if (LR.Flags.ExtendsBefore) {
      DP("WARNING: Pointer is not mapped but section extends into already "
         "mapped data\n");
    }
    if (LR.Flags.ExtendsAfter) {
      DP("WARNING: Pointer is already mapped but section extends beyond mapped "
         "region\n");
    }
  }

  return LR;
}

TargetPointerResultTy DeviceTy::getTargetPointer(
    void *HstPtrBegin, void *HstPtrBase, int64_t Size,
    map_var_info_t HstPtrName, bool HasFlagTo, bool HasFlagAlways,
    bool IsImplicit, bool UpdateRefCount, bool HasCloseModifier,
    bool HasPresentModifier, bool HasHoldModifier, MapTypeTy MapType,
    bool UseBroadcast, AsyncInfoTy &AsyncInfo) {
  void *TargetPointer = nullptr;
  bool IsHostPtr = false;
  bool IsNew = false;
  bool IsLink = false;
  bool UseExchange = false;
  bool UseProxySubmit = false;
  bool UseInfiniteRef = false;

  HostDataToTargetTy *ProxyEntry = nullptr;
  std::optional<std::lock_guard<decltype(*ProxyEntry)>> ProxyEntryLock;

  // Acquire the proxy for link entries.
  if (MapType == MapTypeTy::LINK) {
    TargetPointerResultTy TgtPtrRes = ProxyDevice->getTargetPointer(
        HstPtrBegin, HstPtrBase, Size, HstPtrName, false /* HasFlagTo */,
        false /* HasFlagAlways */, IsImplicit, UpdateRefCount, HasCloseModifier,
        HasPresentModifier, HasHoldModifier, MapTypeTy::PROXY, false,
        AsyncInfo);

    ProxyEntry = TgtPtrRes.Entry;
    if (!ProxyEntry) {
      REPORT("Proxy entry not found when mapping the link entry"
             "(HstPtrBegin=" DPxMOD ", Size=%" PRId64 ", Name=%s)",
             DPxPTR(HstPtrBegin), Size, getNameFromMapping(HstPtrName).c_str());
      return {};
    }

    ProxyEntryLock.emplace(*ProxyEntry);
    if (!ProxyEntry->isProxy()) {
      REPORT("Proxy device contains a non-proxy entry"
             "(HstPtrBegin=" DPxMOD ", Size=%" PRId64 ", Name=%s)",
             DPxPTR(HstPtrBegin), Size, getNameFromMapping(HstPtrName).c_str());
      return {};
    }

    // Update Size on implicit entries based on the proxy device.
    if (IsImplicit && Size == 0) {
      Size = ProxyEntry->HstPtrEnd - ProxyEntry->HstPtrBegin;
    }

    // Link entries are manually maintained. Use infinite ref count to pin data.
    UseInfiniteRef = true;
  }

  HDTTMapAccessorTy HDTTMap = HostDataToTargetMap.getExclusiveAccessor();
  LookupResult LR = lookupMapping(HDTTMap, HstPtrBegin, Size);
  auto *Entry = LR.Entry;

  // Check if the pointer is contained.
  // If a variable is mapped to the device manually by the user - which would
  // lead to the IsContained flag to be true - then we must ensure that the
  // device address is returned even under unified memory conditions.
  if (LR.Flags.IsContained ||
      ((LR.Flags.ExtendsBefore || LR.Flags.ExtendsAfter) && IsImplicit)) {
    auto &HT = *LR.Entry;
    const char *RefCountAction;
    if (UpdateRefCount) {
      // After this, reference count >= 1. If the reference count was 0 but the
      // entry was still there we can reuse the data on the device and avoid a
      // new submission.
      HT.incRefCount(HasHoldModifier);
      RefCountAction = " (incremented)";
    } else {
      // It might have been allocated with the parent, but it's still new.
      IsNew = HT.getTotalRefCount() == 1;
      RefCountAction = " (update suppressed)";
    }
    const char *DynRefCountAction = HasHoldModifier ? "" : RefCountAction;
    const char *HoldRefCountAction = HasHoldModifier ? RefCountAction : "";
    uintptr_t Ptr = HT.TgtPtrBegin + ((uintptr_t)HstPtrBegin - HT.HstPtrBegin);
    INFO(OMP_INFOTYPE_MAPPING_EXISTS, DeviceID,
         "Mapping exists%s with HstPtrBegin=" DPxMOD ", TgtPtrBegin=" DPxMOD
         ", Size=%" PRId64 ", DynRefCount=%s%s, HoldRefCount=%s%s, Name=%s\n",
         (IsImplicit ? " (implicit)" : ""), DPxPTR(HstPtrBegin), DPxPTR(Ptr),
         Size, HT.dynRefCountToStr().c_str(), DynRefCountAction,
         HT.holdRefCountToStr().c_str(), HoldRefCountAction,
         (HstPtrName) ? getNameFromMapping(HstPtrName).c_str() : "unknown");
    TargetPointer = (void *)Ptr;
    IsLink = Entry->isLink();
  } else if ((LR.Flags.ExtendsBefore || LR.Flags.ExtendsAfter) && !IsImplicit) {
    // Explicit extension of mapped data - not allowed.
    MESSAGE("explicit extension not allowed: host address specified is " DPxMOD
            " (%" PRId64
            " bytes), but device allocation maps to host at " DPxMOD
            " (%" PRId64 " bytes)",
            DPxPTR(HstPtrBegin), Size, DPxPTR(Entry->HstPtrBegin),
            Entry->HstPtrEnd - Entry->HstPtrBegin);
    if (HasPresentModifier)
      MESSAGE("device mapping required by 'present' map type modifier does not "
              "exist for host address " DPxMOD " (%" PRId64 " bytes)",
              DPxPTR(HstPtrBegin), Size);
  } else if (PM->RTLs.RequiresFlags & OMP_REQ_UNIFIED_SHARED_MEMORY &&
             !HasCloseModifier) {
    // If unified shared memory is active, implicitly mapped variables that are
    // not privatized use host address. Any explicitly mapped variables also use
    // host address where correctness is not impeded. In all other cases maps
    // are respected.
    // In addition to the mapping rules above, the close map modifier forces the
    // mapping of the variable to the device.
    if (Size) {
      DP("Return HstPtrBegin " DPxMOD " Size=%" PRId64 " for unified shared "
         "memory\n",
         DPxPTR((uintptr_t)HstPtrBegin), Size);
      IsHostPtr = true;
      TargetPointer = HstPtrBegin;
    }
  } else if (HasPresentModifier) {
    DP("Mapping required by 'present' map type modifier does not exist for "
       "HstPtrBegin=" DPxMOD ", Size=%" PRId64 "\n",
       DPxPTR(HstPtrBegin), Size);
    MESSAGE("device mapping required by 'present' map type modifier does not "
            "exist for host address " DPxMOD " (%" PRId64 " bytes)",
            DPxPTR(HstPtrBegin), Size);
  } else if (Size) {
    // If it is not contained and Size > 0, we should create a new entry for it.
    IsNew = true;
    uintptr_t Ptr = IsProxyDevice ? (uintptr_t) nullptr
                                  : (uintptr_t)allocData(Size, HstPtrBegin);
    Entry = HDTTMap
                ->emplace(new HostDataToTargetTy(
                    (uintptr_t)HstPtrBase, (uintptr_t)HstPtrBegin,
                    (uintptr_t)HstPtrBegin + Size, Ptr, HasHoldModifier,
                    MapType, this, HstPtrName, UseInfiniteRef))
                .first->HDTT;
    INFO(OMP_INFOTYPE_MAPPING_CHANGED, DeviceID,
         "Creating new map entry with HstPtrBase=" DPxMOD
         ", HstPtrBegin=" DPxMOD ", TgtPtrBegin=" DPxMOD ", Size=%ld, "
         "DynRefCount=%s, HoldRefCount=%s, Name=%s\n, IsProxyDevice=%s\n",
         DPxPTR(HstPtrBase), DPxPTR(HstPtrBegin), DPxPTR(Ptr), Size,
         Entry->dynRefCountToStr().c_str(), Entry->holdRefCountToStr().c_str(),
         (HstPtrName) ? getNameFromMapping(HstPtrName).c_str() : "unknown",
         IsProxyDevice ? "True" : "False");
    TargetPointer = (void *)Ptr;
    IsLink = Entry->isLink();
  }

  // Update the DM state on both the proxy and the current entry.
  if (Entry && Entry->isLink() && IsNew) {
    // We should already have looked for the proxy entry at the beginning of
    // this function.
    assert(ProxyEntry);

    Entry->setProxyEntry(ProxyEntry);
    ProxyEntry->getActiveEntries().emplace_front(Entry);

    INFO(OMP_INFOTYPE_MAPPING_CHANGED, DeviceID,
         "DM updated on entry (HstPtrBegin=" DPxMOD ", Name=%s). Linking entry "
         "(DeviceId=%d, TgtPtrBegin=" DPxMOD ") to proxy entry "
         "(DeviceId=%d, TgtPtrBegin=" DPxMOD ")\n",
         DPxPTR(Entry->HstPtrBegin),
         getNameFromMapping(Entry->HstPtrName).c_str(),
         Entry->getOwner()->DeviceID, DPxPTR(Entry->TgtPtrBegin),
         ProxyEntry->getOwner()->DeviceID, DPxPTR(ProxyEntry->TgtPtrBegin));

    // Choose which type of data transfer will be used to keep the data coherent
    // on all devices.
    if (ProxyEntry->getActiveEntries().size() > 1 && !UseBroadcast) {
      // Use proxy submit when the program demands an update from the host.
      UseProxySubmit = HasFlagTo && HasFlagAlways;
      // Use exchange when no forced update is required.
      UseExchange = !UseProxySubmit;
      HasFlagTo = true;
    }
  }

  // If the target pointer is valid, and we need to transfer data, issue the
  // data transfer.
  if (TargetPointer && !IsHostPtr && HasFlagTo && (IsNew || HasFlagAlways)) {
    PROF_SCOPED_WITH_IDENT(PROF_LVL_USER, "VarName", HstPtrName);

    // Lock the entry before releasing the mapping table lock such that another
    // thread that could issue data movement will get the right result.
    std::lock_guard<decltype(*Entry)> LG(*Entry);
    // Release the mapping table lock right after the entry is locked.
    HDTTMap.destroy();

    DP("Moving %" PRId64 " bytes (hst:" DPxMOD ") -> (tgt:" DPxMOD ")\n", Size,
       DPxPTR(HstPtrBegin), DPxPTR(TargetPointer));

    int Ret = OFFLOAD_SUCCESS;
    if (UseExchange) {
      Ret = dataProxyExchange(ProxyEntry, TargetPointer, Size, AsyncInfo);
    } else {
      if (UseProxySubmit) {
        // Send data to every entry that is not the current one.
        Ret = dataProxySubmit(ProxyEntry, Entry, HstPtrBegin, Size, AsyncInfo);
      }
      Ret |= submitData(TargetPointer, HstPtrBegin, Size, AsyncInfo);
    }
    if (Ret != OFFLOAD_SUCCESS) {
      REPORT("Copying data to device failed.\n");
      // We will also return nullptr if the data movement fails because that
      // pointer points to a corrupted memory region so it doesn't make any
      // sense to continue to use it.
      TargetPointer = nullptr;
    } else if (Entry->addEventIfNecessary(*this, AsyncInfo) != OFFLOAD_SUCCESS)
      return {{false /* IsNewEntry */, false /* IsHostPointer */},
              nullptr /* Entry */,
              nullptr /* TargetPointer */};
  } else {
    // Release the mapping table lock directly.
    HDTTMap.destroy();
    // If not a host pointer and no present modifier, we need to wait for the
    // event if it exists.
    // Note: Entry might be nullptr because of zero length array section.
    if (Entry && !IsHostPtr && !HasPresentModifier) {
      std::lock_guard<decltype(*Entry)> LG(*Entry);
      void *Event = Entry->getEvent();
      if (Event) {
        int Ret = waitEvent(Event, AsyncInfo);
        if (Ret != OFFLOAD_SUCCESS) {
          // If it fails to wait for the event, we need to return nullptr in
          // case of any data race.
          REPORT("Failed to wait for event " DPxMOD ".\n", DPxPTR(Event));
          return {{false /* IsNewEntry */, false /* IsHostPointer */},
                  nullptr /* Entry */,
                  nullptr /* TargetPointer */};
        }
      }
    }
  }

  return {{IsNew, IsHostPtr, IsLink}, Entry, TargetPointer};
}

// Used by targetDataBegin, targetDataEnd, targetDataUpdate and target.
// Return the target pointer begin (where the data will be moved).
// Decrement the reference counter if called from targetDataEnd.
TargetPointerResultTy
DeviceTy::getTgtPtrBegin(void *HstPtrBegin, int64_t Size, bool &IsLast,
                         bool UpdateRefCount, bool UseHoldRefCount,
                         bool &IsHostPtr, bool MustContain, bool ForceDelete) {
  HDTTMapAccessorTy HDTTMap = HostDataToTargetMap.getExclusiveAccessor();

  void *TargetPointer = NULL;
  bool IsNew = false;
  bool IsProxy = false;
  bool IsLink = false;
  IsHostPtr = false;
  IsLast = false;
  DeviceTy *Owner = this;
  LookupResult LR = lookupMapping(HDTTMap, HstPtrBegin, Size);

  if (LR.Flags.IsContained ||
      (!MustContain && (LR.Flags.ExtendsBefore || LR.Flags.ExtendsAfter))) {
    auto &HT = *LR.Entry;
    IsProxy = HT.isProxy();
    IsLink = HT.isLink();
    IsLast = HT.decShouldRemove(UseHoldRefCount, ForceDelete);

    if (ForceDelete) {
      HT.resetRefCount(UseHoldRefCount);
      assert(IsLast == HT.decShouldRemove(UseHoldRefCount) &&
             "expected correct IsLast prediction for reset");
    }

    const char *RefCountAction;
    if (!UpdateRefCount) {
      RefCountAction = " (update suppressed)";
    } else if (IsLast) {
      // Mark the entry as to be deleted by this thread. Another thread might
      // reuse the entry and take "ownership" for the deletion while this thread
      // is waiting for data transfers. That is fine and the current thread will
      // simply skip the deletion step then.
      HT.setDeleteThreadId();
      HT.decRefCount(UseHoldRefCount);
      assert(HT.getTotalRefCount() == 0 &&
             "Expected zero reference count when deletion is scheduled");
      if (ForceDelete)
        RefCountAction = " (reset, delayed deletion)";
      else
        RefCountAction = " (decremented, delayed deletion)";
    } else {
      HT.decRefCount(UseHoldRefCount);
      RefCountAction = " (decremented)";
    }
    const char *DynRefCountAction = UseHoldRefCount ? "" : RefCountAction;
    const char *HoldRefCountAction = UseHoldRefCount ? RefCountAction : "";
    uintptr_t TP = HT.TgtPtrBegin + ((uintptr_t)HstPtrBegin - HT.HstPtrBegin);
    INFO(OMP_INFOTYPE_MAPPING_EXISTS, DeviceID,
         "Mapping exists with HstPtrBegin=" DPxMOD ", TgtPtrBegin=" DPxMOD ", "
         "Size=%" PRId64 ", DynRefCount=%s%s, HoldRefCount=%s%s\n",
         DPxPTR(HstPtrBegin), DPxPTR(TP), Size, HT.dynRefCountToStr().c_str(),
         DynRefCountAction, HT.holdRefCountToStr().c_str(), HoldRefCountAction);
    TargetPointer = (void *)TP;
  } else if (PM->RTLs.RequiresFlags & OMP_REQ_UNIFIED_SHARED_MEMORY) {
    // If the value isn't found in the mapping and unified shared memory
    // is on then it means we have stumbled upon a value which we need to
    // use directly from the host.
    DP("Get HstPtrBegin " DPxMOD " Size=%" PRId64 " for unified shared "
       "memory\n",
       DPxPTR((uintptr_t)HstPtrBegin), Size);
    IsHostPtr = true;
    TargetPointer = HstPtrBegin;
  }

  if (IsProxy) {
    std::scoped_lock EntryLk(*LR.Entry);
    HDTTMap.destroy();
    // Found proxy entry, gather real information from most recent link.
    LR.Entry = LR.Entry->getActiveEntries().back();
    TargetPointer = (void *)(LR.Entry->TgtPtrBegin +
                             ((uintptr_t)HstPtrBegin - LR.Entry->HstPtrBegin));
    Owner = LR.Entry->getOwner();
  } else if (IsLink) {
    HDTTMap.destroy();
    // Update reference count of proxy entry as well and use its IsLast result.
    DeviceTy *ProxyDevice = LR.Entry->getProxyEntry()->getOwner();
    TargetPointerResultTy TPR = ProxyDevice->getTgtPtrBegin(
        HstPtrBegin, Size, IsLast, UpdateRefCount, UseHoldRefCount, IsHostPtr,
        MustContain, ForceDelete);

    // Return an empty result if proxy entry was removed during secondary query.
    if (!TPR.TargetPointer) {
      return {};
    }
  }

  return {
      {IsNew, IsHostPtr, IsProxy || IsLink}, LR.Entry, TargetPointer, Owner};
}

// Return the target pointer begin (where the data will be moved).
void *DeviceTy::getTgtPtrBegin(HDTTMapAccessorTy &HDTTMap, void *HstPtrBegin,
                               int64_t Size) {
  uintptr_t HP = (uintptr_t)HstPtrBegin;
  LookupResult LR = lookupMapping(HDTTMap, HstPtrBegin, Size);
  if (LR.Flags.IsContained || LR.Flags.ExtendsBefore || LR.Flags.ExtendsAfter) {
    auto &HT = *LR.Entry;
    uintptr_t TP = HT.TgtPtrBegin + (HP - HT.HstPtrBegin);
    return (void *)TP;
  }

  return NULL;
}

int DeviceTy::deallocTgtPtr(HDTTMapAccessorTy &HDTTMap, LookupResult LR,
                            int64_t Size) {
  // Check if the pointer is contained in any sub-nodes.
  if (!(LR.Flags.IsContained || LR.Flags.ExtendsBefore ||
        LR.Flags.ExtendsAfter)) {
    REPORT("Section to delete (hst addr " DPxMOD ") does not exist in the"
           " allocated memory\n",
           DPxPTR(LR.Entry->HstPtrBegin));
    return OFFLOAD_FAIL;
  }

  auto HT = LR.Entry;
  const std::string EntryName = getNameFromMapping(HT->HstPtrName);
  const uintptr_t HstPtrBegin = HT->HstPtrBegin;
  // Verify this thread is still in charge of deleting the entry.
  assert(
      HT->isLink() ||
      HT->getTotalRefCount() == 0 &&
          HT->getDeleteThreadId() == std::this_thread::get_id() &&
          "Trying to delete entry that is in use or owned by another thread.");

  // Remove entries out of the data maps while the lock is acquired. No buffer
  // will be removed here, only its entries.
  int Ret = OFFLOAD_SUCCESS;
  std::vector<std::tuple<DeviceTy *, uintptr_t, void *>> RemovedEntries;
  if (HT->isProxy()) {
    for (auto &LinkEntry : HT->getActiveEntries()) {
      DeviceTy *LinkDevice = LinkEntry->getOwner();
      HDTTMapAccessorTy LinkHDTTMap =
          LinkDevice->HostDataToTargetMap.getExclusiveAccessor();
      RemovedEntries.emplace_back(LinkDevice, LinkEntry->TgtPtrBegin,
                                  LinkEntry->getEvent());
      LinkHDTTMap->erase(LinkEntry);
    }
  } else {
    RemovedEntries.emplace_back(this, HT->TgtPtrBegin, HT->getEvent());
  }
  HDTTMap->erase(HT);
  HDTTMap.destroy();

  // Destroy collected entries.
  for (auto &[OwnerDevice, TgtPtrBegin, Event] : RemovedEntries) {
    INFO(OMP_INFOTYPE_MAPPING_CHANGED, OwnerDevice->DeviceID,
         "Removing map entry with HstPtrBegin=" DPxMOD ", TgtPtrBegin=" DPxMOD
         ", Size=%" PRId64 ", Name=%s\n",
         DPxPTR(HstPtrBegin), DPxPTR(TgtPtrBegin), Size, EntryName.c_str());
    Ret |= OwnerDevice->deleteData((void *)TgtPtrBegin);
    if (Event && OwnerDevice->destroyEvent(Event) != OFFLOAD_SUCCESS) {
      REPORT("Failed to destroy event " DPxMOD "\n", DPxPTR(Event));
      Ret = OFFLOAD_FAIL;
    }
  }

  return Ret;
}

/// Init device, should not be called directly.
void DeviceTy::init() {
  // Make call to init_requires if it exists for this plugin.
  if (RTL->init_requires)
    RTL->init_requires(PM->RTLs.RequiresFlags);
  int32_t Ret = RTL->init_device(RTLDeviceID);
  if (Ret != OFFLOAD_SUCCESS)
    return;

  IsInit = true;
}

/// Thread-safe method to initialize the device only once.
int32_t DeviceTy::initOnce() {
  std::call_once(InitFlag, &DeviceTy::init, this);

  // At this point, if IsInit is true, then either this thread or some other
  // thread in the past successfully initialized the device, so we can return
  // OFFLOAD_SUCCESS. If this thread executed init() via call_once() and it
  // failed, return OFFLOAD_FAIL. If call_once did not invoke init(), it means
  // that some other thread already attempted to execute init() and if IsInit
  // is still false, return OFFLOAD_FAIL.
  if (IsInit)
    return OFFLOAD_SUCCESS;
  return OFFLOAD_FAIL;
}

void DeviceTy::deinit() {
  if (RTL->deinit_device)
    RTL->deinit_device(RTLDeviceID);
}

// Load binary to device.
__tgt_target_table *DeviceTy::loadBinary(void *Img) {
  std::lock_guard<decltype(RTL->Mtx)> LG(RTL->Mtx);
  return RTL->load_binary(RTLDeviceID, Img);
}

// Load data from checkpoint (saved by \p rank with buffer id = \p id)
int32_t DeviceTy::data_recovery(int32_t device_id, void *tgt_ptr, int64_t size,
                                int32_t buffer_id, int32_t cp_rank,
                                int32_t cp_ver,
                                __tgt_async_info *AsyncInfoPtr) {
  if (RTL->IsMPI && !RTL->DisableDataManager) {
    if (!AsyncInfoPtr || !RTL->data_recovery_async || !RTL->synchronize)
      return RTL->data_recovery(device_id, tgt_ptr, size, buffer_id, cp_rank,
                                cp_ver);
    else
      return RTL->data_recovery_async(device_id, tgt_ptr, size, buffer_id,
                                      cp_rank, cp_ver, AsyncInfoPtr);
  }
  return OFFLOAD_FAIL;
}

void *DeviceTy::allocData(int64_t Size, void *HstPtr, int32_t Kind) {
  return RTL->data_alloc(RTLDeviceID, Size, HstPtr, Kind);
}

int32_t DeviceTy::deleteData(void *TgtPtrBegin) {
  return RTL->data_delete(RTLDeviceID, TgtPtrBegin);
}

// Submit data to device
int32_t DeviceTy::submitData(void *TgtPtrBegin, void *HstPtrBegin, int64_t Size,
                             AsyncInfoTy &AsyncInfo) {
  if (getInfoLevel() & OMP_INFOTYPE_DATA_TRANSFER) {
    HDTTMapAccessorTy HDTTMap = HostDataToTargetMap.getExclusiveAccessor();
    LookupResult LR = lookupMapping(HDTTMap, HstPtrBegin, Size);
    auto *HT = &*LR.Entry;

    INFO(OMP_INFOTYPE_DATA_TRANSFER, DeviceID,
         "Copying data from host to device, HstPtr=" DPxMOD ", TgtPtr=" DPxMOD
         ", Size=%" PRId64 ", Name=%s\n",
         DPxPTR(HstPtrBegin), DPxPTR(TgtPtrBegin), Size,
         (HT && HT->HstPtrName) ? getNameFromMapping(HT->HstPtrName).c_str()
                                : "unknown");
  }

  if (!AsyncInfo || !RTL->data_submit_async || !RTL->synchronize)
    return RTL->data_submit(RTLDeviceID, TgtPtrBegin, HstPtrBegin, Size);
  return RTL->data_submit_async(RTLDeviceID, TgtPtrBegin, HstPtrBegin, Size,
                                AsyncInfo);
}

// Retrieve data from device
int32_t DeviceTy::retrieveData(void *HstPtrBegin, void *TgtPtrBegin,
                               int64_t Size, AsyncInfoTy &AsyncInfo) {
  if (getInfoLevel() & OMP_INFOTYPE_DATA_TRANSFER) {
    HDTTMapAccessorTy HDTTMap = HostDataToTargetMap.getExclusiveAccessor();
    LookupResult LR = lookupMapping(HDTTMap, HstPtrBegin, Size);
    auto *HT = &*LR.Entry;
    INFO(OMP_INFOTYPE_DATA_TRANSFER, DeviceID,
         "Copying data from device to host, TgtPtr=" DPxMOD ", HstPtr=" DPxMOD
         ", Size=%" PRId64 ", Name=%s\n",
         DPxPTR(TgtPtrBegin), DPxPTR(HstPtrBegin), Size,
         (HT && HT->HstPtrName) ? getNameFromMapping(HT->HstPtrName).c_str()
                                : "unknown");
  }

  if (!RTL->data_retrieve_async || !RTL->synchronize)
    return RTL->data_retrieve(RTLDeviceID, HstPtrBegin, TgtPtrBegin, Size);
  return RTL->data_retrieve_async(RTLDeviceID, HstPtrBegin, TgtPtrBegin, Size,
                                  AsyncInfo);
}

// Copy data from current device to destination device directly
int32_t DeviceTy::dataExchange(void *SrcPtr, DeviceTy &DstDev, void *DstPtr,
                               int64_t Size, AsyncInfoTy &AsyncInfo) {
  if (!AsyncInfo || !RTL->data_exchange_async || !RTL->synchronize) {
    assert(RTL->data_exchange && "RTL->data_exchange is nullptr");
    return RTL->data_exchange(RTLDeviceID, SrcPtr, DstDev.RTLDeviceID, DstPtr,
                              Size);
  }
  return RTL->data_exchange_async(RTLDeviceID, SrcPtr, DstDev.RTLDeviceID,
                                  DstPtr, Size, AsyncInfo);
}

// Run region on device
int32_t DeviceTy::runRegion(void *TgtEntryPtr, void **TgtVarsPtr,
                            ptrdiff_t *TgtOffsets, int32_t TgtVarsSize,
                            AsyncInfoTy &AsyncInfo) {
  if (!RTL->run_region || !RTL->synchronize)
    return RTL->run_region(RTLDeviceID, TgtEntryPtr, TgtVarsPtr, TgtOffsets,
                           TgtVarsSize);
  return RTL->run_region_async(RTLDeviceID, TgtEntryPtr, TgtVarsPtr, TgtOffsets,
                               TgtVarsSize, AsyncInfo);
}

// Run region on device
bool DeviceTy::printDeviceInfo(int32_t RTLDevId) {
  if (!RTL->print_device_info)
    return false;
  RTL->print_device_info(RTLDevId);
  return true;
}

// Run team region on device.
int32_t DeviceTy::runTeamRegion(void *TgtEntryPtr, void **TgtVarsPtr,
                                ptrdiff_t *TgtOffsets, int32_t TgtVarsSize,
                                int32_t NumTeams, int32_t ThreadLimit,
                                uint64_t LoopTripCount,
                                AsyncInfoTy &AsyncInfo) {
  if (!RTL->run_team_region_async || !RTL->synchronize)
    return RTL->run_team_region(RTLDeviceID, TgtEntryPtr, TgtVarsPtr,
                                TgtOffsets, TgtVarsSize, NumTeams, ThreadLimit,
                                LoopTripCount);
  return RTL->run_team_region_async(RTLDeviceID, TgtEntryPtr, TgtVarsPtr,
                                    TgtOffsets, TgtVarsSize, NumTeams,
                                    ThreadLimit, LoopTripCount, AsyncInfo);
}

// Add information about data that is going to be submitted to the device.
int32_t DeviceTy::register_bcast_ptr(int32_t arg_num, void **args_base,
                                     void **args, int64_t *arg_sizes,
                                     int64_t *arg_types, bool AddToList) {
  return RTL->register_bcast_ptr(arg_num, args_base, args, arg_sizes, arg_types,
                                 AddToList);
}

// Whether data can be copied to DstDevice directly
bool DeviceTy::isDataExchangable(const DeviceTy &DstDevice) {
  if (RTL != DstDevice.RTL || !RTL->is_data_exchangable)
    return false;

  if (RTL->is_data_exchangable(RTLDeviceID, DstDevice.RTLDeviceID))
    return (RTL->data_exchange != nullptr) ||
           (RTL->data_exchange_async != nullptr);

  return false;
}

int32_t DeviceTy::synchronize(AsyncInfoTy &AsyncInfo) {
  if (RTL->synchronize)
    return RTL->synchronize(RTLDeviceID, AsyncInfo);
  return OFFLOAD_SUCCESS;
}

int32_t DeviceTy::synchronize(int32_t task_id, AsyncInfoTy &AsyncInfo) {
  if (RTL->synchronize_id)
    return RTL->synchronize_id(RTLDeviceID, task_id, AsyncInfo);
  return OFFLOAD_SUCCESS;
}

int32_t DeviceTy::createEvent(void **Event) {
  if (RTL->create_event)
    return RTL->create_event(RTLDeviceID, Event);

  return OFFLOAD_SUCCESS;
}

int32_t DeviceTy::recordEvent(void *Event, AsyncInfoTy &AsyncInfo) {
  if (RTL->record_event)
    return RTL->record_event(RTLDeviceID, Event, AsyncInfo);

  return OFFLOAD_SUCCESS;
}

int32_t DeviceTy::waitEvent(void *Event, AsyncInfoTy &AsyncInfo) {
  if (RTL->wait_event)
    return RTL->wait_event(RTLDeviceID, Event, AsyncInfo);

  return OFFLOAD_SUCCESS;
}

int32_t DeviceTy::syncEvent(void *Event) {
  if (RTL->sync_event)
    return RTL->sync_event(RTLDeviceID, Event);

  return OFFLOAD_SUCCESS;
}

int32_t DeviceTy::destroyEvent(void *Event) {
  if (RTL->create_event)
    return RTL->destroy_event(RTLDeviceID, Event);

  return OFFLOAD_SUCCESS;
}

/// Check whether a device has an associated RTL and initialize it if it's not
/// already initialized.
bool deviceIsReady(int DeviceNum) {
  DP("Checking whether device %d is ready.\n", DeviceNum);
  // Devices.size() can only change while registering a new
  // library, so try to acquire the lock of RTLs' mutex.
  size_t DevicesSize;
  {
    std::lock_guard<decltype(PM->RTLsMtx)> LG(PM->RTLsMtx);
    DevicesSize = PM->Devices.size();
  }
  if (DevicesSize <= (size_t)DeviceNum) {
    DP("Device ID  %d does not have a matching RTL\n", DeviceNum);
    return false;
  }

  // Get device info
  DeviceTy &Device = *PM->Devices[DeviceNum];

  DP("Is the device %d (local ID %d) initialized? %d\n", DeviceNum,
     Device.RTLDeviceID, Device.IsInit);

  // Init the device if not done before
  if (!Device.IsInit && Device.initOnce() != OFFLOAD_SUCCESS) {
    DP("Failed to init device %d\n", DeviceNum);
    return false;
  }

  DP("Device %d is ready to use.\n", DeviceNum);

  return true;
}

// Checks whether the first device of the current RTL plugin type have a proxy
// entry for the (HstPtrBegin, Size) memory pair.
bool DeviceTy::hasProxy(void *HstPtrBegin, int64_t Size) {
  HDTTMapAccessorTy HDTTMap =
      ProxyDevice->HostDataToTargetMap.getExclusiveAccessor();
  LookupResult LR = ProxyDevice->lookupMapping(HDTTMap, HstPtrBegin, Size);

  const bool Found =
      LR.Flags.IsContained || LR.Flags.ExtendsAfter || LR.Flags.ExtendsBefore;
  return Found && LR.Entry->isProxy();
}

DeviceTy *DeviceTy::getLastActiveDevice(void *HstPtrBegin, int64_t Size) {
  HDTTMapAccessorTy HDTTMap =
      ProxyDevice->HostDataToTargetMap.getExclusiveAccessor();
  LookupResult LR = ProxyDevice->lookupMapping(HDTTMap, HstPtrBegin, Size);

  if (!LR.Flags.IsContained || !LR.Entry->isProxy()) {
    return nullptr;
  }

  return LR.Entry->getActiveEntries().back()->getOwner();
}

// Removes active entries that conflicts with the current one owned by the
// current device. Map conflicts happens in the following scenarios:
// 1. The current target region is nowait with dep out on the data address
//    (HstPtrBegin);
// 2. The current target region is synchronous (non-nowait: there is currently
//    no way of knowing if the target region will write to the buffer).
int32_t DeviceTy::removeInactiveLinks(void *HstPtrBegin, int64_t Size) {
  // Get proxy entry for the (HstPtrBegin, Size) memory pair.
  HostDataToTargetTy *ProxyEntry = nullptr;
  std::optional<std::lock_guard<decltype(*ProxyEntry)>> ProxyEntryLock;
  {
    HDTTMapAccessorTy HDTTMap =
        ProxyDevice->HostDataToTargetMap.getExclusiveAccessor();
    LookupResult LR = ProxyDevice->lookupMapping(HDTTMap, HstPtrBegin, Size);

    if (LR.Flags.IsContained) {
      ProxyEntry = LR.Entry;
      ProxyEntryLock.emplace(*ProxyEntry);
    }
  }

  // Get the current entry for the (HstPtrBegin, Size) memory pair and check if
  // it is either a link or a proxy entry.
  HostDataToTargetTy *CurrEntry = nullptr;
  std::optional<std::lock_guard<decltype(*CurrEntry)>> CurrEntryLock;
  {
    HDTTMapAccessorTy HDTTMap = HostDataToTargetMap.getExclusiveAccessor();
    LookupResult LR = lookupMapping(HDTTMap, HstPtrBegin, Size);

    if (LR.Flags.IsContained) {
      CurrEntry = LR.Entry;
      CurrEntryLock.emplace(*CurrEntry);
    }
  }

  const bool ProxyFound = ProxyEntry && ProxyEntry->isProxy();
  const bool CurrFound = CurrEntry && CurrEntry->isLink();

  // Current entry was not found or it is a local entry. Returns immediately.
  if (!CurrFound) {
    return OFFLOAD_SUCCESS;
  }

  if (!ProxyFound || ProxyEntry != CurrEntry->getProxyEntry()) {
    REPORT("Proxy entry not found when resolving conflicts for entry "
           "(HstPtrBegin=" DPxMOD ", Size=%" PRId64 ")\n",
           DPxPTR(HstPtrBegin), Size);
    return OFFLOAD_FAIL;
  }

  int32_t Ret = OFFLOAD_SUCCESS;

  // Remove conflicting map entries/copies when we have one of the following:
  // 1. The current target region is nowait with dep out on the data address;
  // 2. The current target region is synchronous (non-nowait: there is no way of
  //    knowing if the target region will write to the buffer).
  const int32_t CurrTaskId = GetCurrentTargetTaskID();
  const bool HasConflicts =
      CurrTaskId == -1 || HasOutDep(CurrTaskId, (int64_t)HstPtrBegin);
  ActiveEntriesListTy &ActiveEntries = ProxyEntry->getActiveEntries();
  if (ActiveEntries.size() > 1 && HasConflicts) {
    INFO(OMP_INFOTYPE_MAPPING_CHANGED, DeviceID,
         "Data conflict found. Removing %zu inactive links "
         "for proxy entry (HstPtrBegin=" DPxMOD ", Size=%" PRId64 ")\n",
         ActiveEntries.size() - 1, DPxPTR(HstPtrBegin), Size);

    ActiveEntries.remove_if([&](HostDataToTargetTy *ConfEntry) {
      assert(ConfEntry->HstPtrBegin == (uintptr_t)HstPtrBegin);
      assert(ConfEntry->isLink());

      // Do not remove the current entry.
      if (ConfEntry == CurrEntry) {
        return false;
      }

      // Setup dummy entry query for deletion.
      DeviceTy *ConfDev = ConfEntry->getOwner();
      auto HDTTMap = ConfDev->HostDataToTargetMap.getExclusiveAccessor();
      LookupResult LR;
      LR.Entry = ConfEntry;
      LR.Flags.IsContained = true;
      const bool UpdateShadowMap = ConfEntry->getMayContainAttachedPointers();

      INFO(OMP_INFOTYPE_MAPPING_CHANGED, ConfDev->RTLDeviceID,
           "Removing conflict entry (HstPtrBegin=" DPxMOD
           ", TgtPtrBegin=" DPxMOD " Size=%" PRId64 ")\n",
           DPxPTR(ConfEntry->HstPtrBegin), DPxPTR(ConfEntry->TgtPtrBegin),
           Size);

      Ret |= ConfDev->deallocTgtPtr(HDTTMap, LR, Size);

      // Remove conflicting entries in the shadow map.
      if (UpdateShadowMap) {
        std::scoped_lock ConfShadowMapLock(ConfDev->ShadowMtx);
        ShadowPtrListTy &ConfShadowMap = ConfDev->ShadowPtrMap;

        uintptr_t LB = (uintptr_t)HstPtrBegin;
        uintptr_t UB = LB + Size;

        for (auto Itr = ConfShadowMap.begin(); Itr != ConfShadowMap.end();) {
          uintptr_t ShadowHstPtrAddr = (uintptr_t)Itr->first;

          // An STL map is sorted on its keys; use this property
          // to quickly determine when to break out of the loop.
          if (ShadowHstPtrAddr < LB) {
            ++Itr;
            continue;
          }
          if (ShadowHstPtrAddr >= UB)
            break;

          Itr = ConfShadowMap.erase(Itr);
        }
      }

      return true;
    });

    assert(ActiveEntries.size() == 1);
  }

  return Ret;
}

// Submit the data pointed by the pair (HstPtrBegin, Size) by using the
// data_exchange function, transferring the data from its latest location to
// current device.
// Note: this function is only used internally and does not acquire the locks
// for the current and the proxy entries in order to avoid double locking.
// Always lock them before using it.
int32_t DeviceTy::dataProxyExchange(const HostDataToTargetTy *ProxyEntry,
                                    void *TgtPtrBegin, int64_t Size,
                                    AsyncInfoTy &AsyncInfo) {
  // Transfer the data to the current device.
  // When getting the first link, always choose the oldest entry. This ensures
  // that we do not use any entry that has their data transfers still in
  // flight through an async context.
  const auto LinkEntry = ProxyEntry->getActiveEntries().back();
  std::lock_guard<decltype(*LinkEntry)> LinkEntryLock(*LinkEntry);
  assert(LinkEntry->getOwner()->isDataExchangable(*this));

  INFO(OMP_INFOTYPE_DATA_TRANSFER, DeviceID,
       "Exchanging entry (HstPtrBegin=" DPxMOD ", Name=%s), "
       "from (DeviceId=%d, TgtPtrBegin=" DPxMOD ") "
       "to (DeviceId=%d, TgtPtrBegin=" DPxMOD ")\n",
       DPxPTR(LinkEntry->HstPtrBegin),
       getNameFromMapping(LinkEntry->HstPtrName).c_str(),
       LinkEntry->getOwner()->DeviceID, DPxPTR(LinkEntry->TgtPtrBegin),
       DeviceID, DPxPTR(TgtPtrBegin));

  // Use the exchange function to transfer data between two remote processes.
  int32_t Ret = LinkEntry->getOwner()->dataExchange(
      (void *)LinkEntry->TgtPtrBegin, *this, TgtPtrBegin, Size, AsyncInfo);

  return Ret;
}

// Submits the data pointed by the (HstPtrBegin, Size) pair to every location
// with an active entry.
int32_t DeviceTy::dataProxySubmit(void *HstPtrBegin, int64_t Size,
                                  AsyncInfoTy &AsyncInfo) {
  // Get proxy entry for the (HstPtrBegin, Size) memory pair.
  HostDataToTargetTy *ProxyEntry = nullptr;
  std::optional<std::lock_guard<decltype(*ProxyEntry)>> ProxyEntryLock;
  {
    HDTTMapAccessorTy HDTTMap =
        ProxyDevice->HostDataToTargetMap.getExclusiveAccessor();
    LookupResult LR = ProxyDevice->lookupMapping(HDTTMap, HstPtrBegin, Size);

    if (LR.Flags.IsContained) {
      ProxyEntry = LR.Entry;
      ProxyEntryLock.emplace(*ProxyEntry);
    }
  }

  if (!ProxyEntry || !ProxyEntry->isProxy()) {
    REPORT("Proxy entry not found when updating all links for entry "
           "(HstPtrBegin=" DPxMOD ", Size=%" PRId64 ")\n",
           DPxPTR(HstPtrBegin), Size);
    return OFFLOAD_FAIL;
  }

  int Ret = OFFLOAD_SUCCESS;

  const ActiveEntriesListTy &ActiveEntries = ProxyEntry->getActiveEntries();

  INFO(OMP_INFOTYPE_DATA_TRANSFER, DeviceID,
       "Sending data to all %zu links for the entry "
       "(HstPtrBegin=" DPxMOD ", Size=%" PRId64 ")\n",
       ActiveEntries.size(), DPxPTR(HstPtrBegin), Size);

  // Submit the data to every active entry.
  for (auto LinkEntry : ActiveEntries) {
    std::lock_guard<decltype(*LinkEntry)> LinkEntryLock(*LinkEntry);
    void *TgtPtrBegin = (void *)LinkEntry->TgtPtrBegin;
    Ret |= LinkEntry->getOwner()->submitData(TgtPtrBegin, HstPtrBegin, Size,
                                             AsyncInfo);
  }

  return Ret;
}

// Retrieves the data from the oldest active entry for the (HstPtrBegin, Size)
// pair.
int32_t DeviceTy::dataProxyRetrieve(void *HstPtrBegin, int64_t Size,
                                    AsyncInfoTy &AsyncInfo) {
  // Get proxy entry for the (HstPtrBegin, Size) memory pair.
  HostDataToTargetTy *ProxyEntry = nullptr;
  std::optional<std::lock_guard<decltype(*ProxyEntry)>> ProxyEntryLock;
  {
    HDTTMapAccessorTy HDTTMap =
        ProxyDevice->HostDataToTargetMap.getExclusiveAccessor();
    LookupResult LR = ProxyDevice->lookupMapping(HDTTMap, HstPtrBegin, Size);

    if (LR.Flags.IsContained) {
      ProxyEntry = LR.Entry;
      ProxyEntryLock.emplace(*ProxyEntry);
    }
  }

  if (!ProxyEntry || !ProxyEntry->isProxy()) {
    REPORT("Proxy entry not found when retrieving data from a link for entry "
           "(HstPtrBegin=" DPxMOD ", Size=%" PRId64 ")\n",
           DPxPTR(HstPtrBegin), Size);
    return OFFLOAD_FAIL;
  }

  const ActiveEntriesListTy &ActiveEntries = ProxyEntry->getActiveEntries();
  if (ActiveEntries.size() == 0) {
    REPORT("No active link entry found at the proxy entry "
           "(HstPtrBegin=" DPxMOD ", Size=%" PRId64 ")\n",
           DPxPTR(HstPtrBegin), Size);
    return OFFLOAD_FAIL;
  }

  HostDataToTargetTy *LinkEntry = ActiveEntries.back();
  std::lock_guard<decltype(*LinkEntry)> LinkEntryLock(*LinkEntry);

  INFO(OMP_INFOTYPE_DATA_TRANSFER, LinkEntry->getOwner()->RTLDeviceID,
       "Retrieving data from the most recent link for entry "
       "(HstPtrBegin=" DPxMOD ", Size=%" PRId64 ", TgtPtrBegin=" DPxMOD ")\n",
       DPxPTR(LinkEntry->HstPtrBegin), Size, DPxPTR(LinkEntry->TgtPtrBegin));

  return LinkEntry->getOwner()->retrieveData(
      HstPtrBegin, (void *)LinkEntry->TgtPtrBegin, Size, AsyncInfo);
}

// Submits the data from the host to all active entries but the current one.
// This function is used internally by getTargetPointer.
int32_t DeviceTy::dataProxySubmit(HostDataToTargetTy *ProxyEntry,
                                  HostDataToTargetTy *CurrEntry,
                                  void *HstPtrBegin, int64_t Size,
                                  AsyncInfoTy &AsyncInfo) {
  const ActiveEntriesListTy &ActiveEntries = ProxyEntry->getActiveEntries();

  INFO(OMP_INFOTYPE_DATA_TRANSFER, DeviceID,
       "Sending data to all %zu links for the entry "
       "(HstPtrBegin=" DPxMOD ", Size=%" PRId64 ")\n",
       ActiveEntries.size(), DPxPTR(HstPtrBegin), Size);

  // Submit the data to every active entry.
  int Ret = OFFLOAD_SUCCESS;
  for (auto LinkEntry : ActiveEntries) {
    // Skip current entry. This is treated by the caller.
    if (LinkEntry == CurrEntry)
      continue;
    std::lock_guard<decltype(*LinkEntry)> LinkEntryMapLock(*LinkEntry);
    void *TgtPtrBegin = (void *)LinkEntry->TgtPtrBegin;
    Ret |= LinkEntry->getOwner()->submitData(TgtPtrBegin, HstPtrBegin, Size,
                                             AsyncInfo);
    Ret |= LinkEntry->addEventIfNecessary(*LinkEntry->getOwner(), AsyncInfo);
  }

  return Ret;
}
