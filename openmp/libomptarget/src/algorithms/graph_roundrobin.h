//===------------- graph_roundrobin.h - Target-Task scheduler -------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Definition of the graph based round-robin scheduler.
//
//===----------------------------------------------------------------------===//

#ifndef _OMPTARGET_GRAPH_ROUNDROBIN_H
#define _OMPTARGET_GRAPH_ROUNDROBIN_H

#include "basescheduler.h"

/// Graph Round-Robin scheduler
///
/// Statically maps tasks to devices in circular order using the task graph.
class GraphRoundRobinScheduler final : public BaseScheduler {
public:
  GraphRoundRobinScheduler() = default;
  ~GraphRoundRobinScheduler() { dumpTaskGraph(); }

private:
  /// Run scheduling algorithm.
  void scheduleGraph(const RTLInfoTy *device) override;

  /// Do nothing.
  void acquireNewGraphPost() override {}

  /// Do nothing.
  void preScheduleTask(int32_t task_id, const RTLInfoTy *device) override {}
};

#endif /* _OMPTARGET_GRAPH_ROUNDROBIN_H */
