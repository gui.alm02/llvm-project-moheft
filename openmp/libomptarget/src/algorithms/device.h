#ifndef _OMPTARGET_DEVICE_ID_H
#define _OMPTARGET_DEVICE_ID_H

#include "basescheduler.h"

namespace detail {

//using namespace llvm;

struct DeviceId {
  /// The MPI rank of the worker node.
  int32_t Rank;
  /// The ID of the event handler thread.
  int32_t Thread;

  explicit DeviceId(int32_t Rank, int32_t Thread = -1)
      : Rank(Rank), Thread(Thread) {}

  explicit DeviceId(const TaskScheduleData &Data)
      : Rank(Data.rank), Thread(Data.thread) {}

  ~DeviceId() = default;

  static DeviceId head() { return DeviceId{0, -1}; }

  bool operator==(const DeviceId &Other) const {
    return Rank == Other.Rank && Thread == Other.Thread;
  }

  /// Hashing function for this type so we can use it as unordered map keys
  struct Hash {
    size_t operator()(const DeviceId &Device) const {
      return std::hash<int32_t>()(Device.Rank) ^
             std::hash<int32_t>()(Device.Thread);
    }
  };
};

}


#endif /* _OMPTARGET_DEVICE_ID_H */
