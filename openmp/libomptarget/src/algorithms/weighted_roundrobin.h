//===----------- weighted_roundrobin.h - Target-Task scheduler ------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Definition of the round-robin scheduler.
//
//===----------------------------------------------------------------------===//

#ifndef _OMPTARGET_WEIGHTED_ROUNDROBIN_H
#define _OMPTARGET_WEIGHTED_ROUNDROBIN_H

#include "basescheduler.h"

#include "ft.h"

#include <atomic>

/// Round-Robin scheduler
///
/// Maps tasks to devices in circular order.
class WeightedRoundRobinScheduler final : public BaseScheduler {
public:
  WeightedRoundRobinScheduler() = default;
  ~WeightedRoundRobinScheduler() { dumpTaskGraph(); }

private:
  /// Do nothing.
  void scheduleGraph(const RTLInfoTy *device) override {}

  /// Do nothing.
  void acquireNewGraphPost() override {}

  /// Schedules the task using round robin.
  void preScheduleTask(int32_t task_id, const RTLInfoTy *device) override;

private:
  /// Number of #preScheduleTask calls to different devices.
  std::atomic<size_t> task_device_counter;
  std::atomic<size_t> task_device_current;
  std::atomic<size_t> task_device_weight;
};

#endif /* _OMPTARGET_WEIGHTED_ROUNDROBIN_H */
