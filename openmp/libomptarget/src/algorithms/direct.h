//===----------------- direct.h - Target-Task scheduler -------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Definition of the direct scheduler.
//
//===----------------------------------------------------------------------===//

#ifndef _OMPTARGET_DIRECT_H
#define _OMPTARGET_DIRECT_H

#include "basescheduler.h"

#include "ft.h"

#include <atomic>

/// Direct scheduler
///
/// Maps tasks to devices in circular order.
class DirectScheduler final : public BaseScheduler {
public:
  DirectScheduler() = default;
  ~DirectScheduler() { dumpTaskGraph(); }

private:
  /// Do nothing.
  void scheduleGraph(const RTLInfoTy *device) override {}

  /// Do nothing.
  void acquireNewGraphPost() override {}

  /// Schedules the task using direct scheduling.
  void preScheduleTask(int32_t task_id, const RTLInfoTy *device) override;
  
};

#endif /* _OMPTARGET_DIRECT_H */
