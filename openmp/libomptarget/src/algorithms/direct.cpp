//===----------------- direct.cpp - Target-Task scheduler -----------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Implementation of the direct scheduler.
//
//===----------------------------------------------------------------------===//

#include "direct.h"

#include "ft.h"

#include <algorithm>

#include <iostream>
#include <fstream>
#include <cstring>
#include <string>

void DirectScheduler::preScheduleTask(int32_t task_id,
                                          const RTLInfoTy *device) {

  std::cout << "in direct.cpp \n";

  assert(schedule_map.count(task_id) == 1);
  auto &schedule_data = schedule_map[task_id];
  auto &task = schedule_data.task;

  std::string myText;
  std::ifstream MyReadFile("/home/gui02/work_dir/my-ompc-examples/dependencies/dependencies.cc");
  while (getline (MyReadFile, myText)) {
    std::cout << "Read record : " << myText <<"\n";
  }

  // Close the file
  MyReadFile.close();

  schedule_data.state = TaskState::SCHEDULED;
  // Target data task are all mapped to the same device because we noticed
  // better performance this way (we cannot predict where the data is actually
  // going to be used with direct strategy).
  if(isTargetDataTask(task))
  {
    std::cout << "isTargetDataTask \n";
    schedule_data.rank = 1;
  }
  else 
  {
    std::string env_var_string = "OMPCLUSTER_DIRECT_" + std::to_string(task_id-100);
    const char* str = env_var_string.c_str();
    char *EnvStr = std::getenv(str);
    schedule_data.rank = std::stoi(EnvStr);
    //std::cout << "task: " << std::to_string(task_id-100) << " task_device: " << std::to_string(schedule_data.rank) << "\n";
  }
}
