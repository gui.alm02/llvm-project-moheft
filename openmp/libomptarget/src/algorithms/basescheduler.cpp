//===-------------- basescheduler.cpp - Target-Task scheduler -------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Implementation of the base abstract scheduler.
//
//===----------------------------------------------------------------------===//

#include "basescheduler.h"
#include "profiler.h"

#include <algorithm>
#include <chrono>
#include <fstream>
#include <thread>
#include <unordered_set>

#include "ft.h"

BaseScheduler::BaseScheduler()
    : remaining_target_tasks(0), non_nowait_counter(0),
      non_task_scheduler(NonTaskScheduler::STATIC), graph_dump_dir_path(),
      graph_counter(0), schedule_map(), task_map(nullptr) {
  // Read environment variables
  if (char *env_str = std::getenv("OMPCLUSTER_TASK_GRAPH_DUMP_PATH")) {
    graph_dump_dir_path = env_str;
  }

  const char *str = std::getenv("OMPCLUSTER_NONTASK_SCHEDULER");
  std::string sched_str = (str != nullptr) ? str : "";

  if (sched_str == "roundrobin") {
    DP("[NON-TASK SCHEDULER] Map synchronous regions using round-robin\n");
    non_task_scheduler = NonTaskScheduler::ROUNDROBIN;
  } else {
    DP("[NON-TASK SCHEDULER] Map synchronous regions to the first worker\n");
  }
}

BaseScheduler::~BaseScheduler() {
  // The task map can still be allocated due to a cancellation point.
  if (task_map != nullptr) {
    __kmpc_target_task_map_free(task_map);
    task_map = nullptr;

    if (remaining_target_tasks != 0) {
      DP("Warning: some target-tasks were discarted at the end of the program, "
         "probably due to a cancellation point.\n");
    }
  }
}

void BaseScheduler::waitOmpGraphCreation() {
  // Wait for the task builder thread (the thread executing the master/single
  // region) to finish the task graph and enter a synchronization point (starts
  // executing tasks itself).
  assert(ft_handler.TaskGetCurrentTargetID() != -1);
  const int32_t omp_gtid = __kmpc_global_thread_num(nullptr);
  while (!__kmpc_is_graph_builder_tasking()) {
    // TODO: better waiting strategy. Maybe used smart sleep to free the core?
    std::this_thread::sleep_for(std::chrono::microseconds(10));
  }
}

void BaseScheduler::acquireNewGraph() {
  // Remove any internal state of the current task graph.
  if (task_map != nullptr) {
    __kmpc_target_task_map_free(task_map);
    task_map = nullptr;

    schedule_map.clear();

    if (remaining_target_tasks != 0) {
      DP("Warning: some target-tasks were discarted in favor of a new "
         "task-graph, probably due to a cancellation point.\n");
    }
    remaining_target_tasks = 0;
  }

  // Wait for the new graph to be created by the OpenMP runtime.
  waitOmpGraphCreation();

  // Acquire a new task graph from the target task integration layer.
  task_map = __kmpc_target_task_map_create();
  assert(task_map != nullptr);

  std::unordered_map<void *, int64_t> size_map;

  for (int idx = 0; idx < task_map->num_tasks; ++idx) {
    const kmp_target_task_t &task = task_map->tasks[idx];

    if (task.is_target_task) {
      // Add new target-tasks to the schedule map
      assertm(schedule_map.count(task.task_id) == 0,
              "New tasks should not be already present in the schedule map.");
      schedule_map[task.task_id] = {TaskState::CREATED, -1, -1, -1, &task};
      remaining_target_tasks++;

      // Correct size of globally mapped variables
      if (isTargetDataTask(task)) {
        for (int32_t i = 0; i < task.target_data.num_args; i++) {
          // Backup initial mapping size
          size_map[task.target_data.args[i]] = task.target_data.arg_sizes[i];
        }
      } else {
        for (int32_t i = 0; i < task.target_data.num_args; i++) {
          // Update mapping size when needed
          auto it = size_map.find(task.target_data.args[i]);
          if (it != size_map.end())
            task.target_data.arg_sizes[i] = it->second;
        }
      }
    }
  }
  if (graph_counter != 0) {
    graph_counter++;
  }
  dumpTaskGraph();
  graph_counter++;
  // Add ref to FT handler (TODO: Is there any way to const this ref?)
  ft_handler.setGraphRefs(&schedule_map, task_map, this);

  acquireNewGraphPost();
}

int32_t BaseScheduler::scheduleTask(int32_t task_id, const RTLInfoTy *device) {
  int32_t scheduled_id = -1;
  const int32_t active_devices_number = ft_handler.DevicesGetActiveNumber();
  if (task_id == -1) {
    // If the target region is not inside a task (non-nowait), use the
    // configured scheduling strategy
    if (non_task_scheduler == NonTaskScheduler::ROUNDROBIN) {
      scheduled_id = (non_nowait_counter++ % active_devices_number);
    } else {
      // Static strategy as default: map to the first device
      scheduled_id = 0;
    }
  } else {
    // If the target region is inside a task (nowait), use the result of the
    // graph based scheduling algorithm.

    // Acquire a new graph if the task id cannot be found in the current one.
    if (task_map == nullptr || schedule_map.count(task_id) == 0) {
      acquireNewGraph();
    }
    assert(schedule_map.count(task_id) == 1);

    preScheduleTask(task_id, device);
    const TaskScheduleData &task_schedule = schedule_map[task_id];

    // Schedule the graph if the current task is not scheduled yet.
    if (task_schedule.state < TaskState::SCHEDULED) {
      scheduleGraph(device);
    }
    assert(task_schedule.rank != -1);
    assert(task_schedule.state == TaskState::SCHEDULED ||
           task_schedule.state == TaskState::FAILED);

    scheduled_id = task_schedule.rank - 1;
  }

  assert(scheduled_id >= 0);
  assert(scheduled_id < active_devices_number);

  return device->Idx + scheduled_id;
}

void BaseScheduler::pinTask(int32_t task_id, int DeviceId) {
  if (task_id != -1) {
    TaskScheduleData &task_schedule = schedule_map[task_id];
    task_schedule.rank = DeviceId + 1;
  }
}

void BaseScheduler::setTaskState(int32_t task_id, TaskState new_state) {
  assert(task_map != nullptr);
  assert(schedule_map.count(task_id) == 1);
  assert(new_state >= TaskState::DISPATCHED);

  TaskScheduleData &task_schedule = schedule_map[task_id];
  // If the current task state is FAILED, it means that the counter was already
  // decremented for this task, so we increment again to make the values
  // consistent
  if (task_schedule.state == TaskState::FAILED) {
    // A FAILED task cannot have its state changed directly to EXECUTED, it
    // needs to be DISPATCHED first
    // if (new_state != TaskState::DISPATCHED)
    remaining_target_tasks++;
  }

  task_schedule.state = new_state;
  if (new_state == TaskState::EXECUTED) {
    remaining_target_tasks--;
    assert(remaining_target_tasks >= 0);
  } else if (new_state == TaskState::FAILED && ft_handler.isActive()) {
    ft_handler.addFailedTask(task_id);
  }
}

void BaseScheduler::setTaskState(int32_t task_id, int device_id,
                                 TaskState new_state) {
  // Update rank that executed, in case of failures, the translation will say
  // the real device that executed the task. Do not update if the task is FAILED
  // and the new state is EXECUTED, because this is the case where the graph was
  // just re-scheduled and we will ignore this state transition
  if (schedule_map[task_id].state == TaskState::FAILED &&
      new_state == TaskState::EXECUTED) {
    remaining_target_tasks--;
    return;
  }

  schedule_map[task_id].rank = device_id + 1;
  setTaskState(task_id, new_state);
}

void BaseScheduler::setTaskSourceLocation(int32_t task_id, ident_t *loc) {
  assert(task_map != nullptr);

  TaskScheduleData &task_schedule = schedule_map[task_id];

  SourceInfo tinfo(loc);
  if (tinfo.isAvailible()) {
    task_schedule.source_location = std::string(tinfo.getFilename()) + ":" +
                                    std::to_string(tinfo.getLine());
  }
}

TaskScheduleData BaseScheduler::getTask(int task_id) {
  if (task_id != -1) {
    if (schedule_map.count(task_id) == 0) {
      acquireNewGraph();
    }
    assert(schedule_map.count(task_id) == 1);

    return schedule_map[task_id];
  }

  return TaskScheduleData{.state = TaskState::UNDEFINED,
                          .rank = -1,
                          .translated_rank = -1,
                          .task = nullptr};
}

kmp_target_dep BaseScheduler::getDep(int32_t task_id, int64_t addr) {
  auto dep = kmp_target_dep{.addr = -1, .dep_type = {.in = 0, .out = 0}};

  kmp_target_task_t *task = __kmpc_target_task_map_get(task_map, task_id);

  if (task != nullptr) {
    for (int i = 0; i < task->num_deps; ++i) {
      if (task->deps[i].addr == addr) {
        dep = task->deps[i];
        break;
      }
    }
  }

  return dep;
}

void BaseScheduler::reScheduleGraph(const RTLInfoTy *device) {
  // Clear the contexts for the new scheduling. In future, the states of
  // previous schedules are needed, since they can contain FAILED tasks that
  // will need to be rescheduled
  acquireNewGraphPost();
  scheduleGraph(device);
}

void BaseScheduler::dumpTaskGraph() {
  PROF_SCOPED(PROF_LVL_ALL, "BaseScheduler / Dump Task Graph");

  if (graph_dump_dir_path.empty())
    return;

  std::ofstream dot(graph_dump_dir_path + "_graph_" +
                    std::to_string(graph_counter) + ".dot");
  graph_counter++;
  // Print header
  dot << "// Generated in BaseScheduler\n"
      << "digraph TargetTaskGraph {\n"
      << "  labelloc=\"t\";\n"
      << "  label=\"Generated in BaseScheduler\";\n";

  if (task_map == nullptr) {
    dot << "}\n";
    return;
  }

  for (const auto &task : Tasks(task_map)) {
    int32_t tid = task.task_id;
    int32_t trank = schedule_map[tid].rank;
    std::string tinfo = schedule_map[tid].source_location;

    // Print graph node
    dot << "  V" << tid << " [label=<<B>ID " << tid
        << "</B><BR /><FONT POINT-SIZE=\"10\">Rank " << trank
        << ((schedule_map[tid].state == TaskState::SAVED) ? "<BR />Saved\""
                                                          : "<BR />Unsaved\"");

    if (!tinfo.empty()) {
      dot << "<BR />" << tinfo;
    }

    dot << "</FONT>>";

    if (isTargetDataTask(task))
      dot << " color=gray70 fontcolor=gray50";
    if (isRootTask(task) || isExitTask(task))
      dot << " shape=octagon";

    dot << "];\n";

    for (const auto &succ : Successors(task)) {
      int32_t sid = succ.task_id;
      int32_t srank = schedule_map[sid].rank;
      const auto *color = detail::getEdgeColor(trank, srank);

      // Print graph edge
      dot << "  V" << tid << " -> V" << sid << " [color=\"" << color
          << "\"];\n";
    }
  }

  dot << "}\n";
}

// Task Iterators
// =============================================================================

std::list<TaskPtr> TopologicalOrder(kmp_target_task_map_t &TaskMap) {
  // Set of all nodes with no incoming edge
  std::unordered_set<TaskPtr> Set;
  // Hash map to keep track of incoming edges
  std::unordered_map<TaskPtr, std::list<TaskPtr>> Incoming;
  // Empty list that will contain the sorted elements
  std::list<TaskPtr> List;

  Set.reserve(TaskMap.num_tasks);
  Incoming.reserve(TaskMap.num_tasks);

  // Initialization
  for (const auto &Task : Tasks(TaskMap)) {
    if (Task.predecessors == nullptr)
      Set.insert(&Task);
    for (const auto &Succ : Successors(Task))
      Incoming[&Succ].push_back(&Task);
  }

  // Main algorithm
  while (not Set.empty()) {
    TaskPtr Task = *(Set.begin());
    Set.erase(Task);
    List.push_back(Task);
    for (const auto &Succ : Successors(Task)) {
      Incoming[&Succ].remove(Task);
      if (Incoming[&Succ].empty())
        Set.insert(&Succ);
    }
  }

  assert(std::all_of(Incoming.begin(), Incoming.end(),
                     [](const auto &pair) { return pair.second.empty(); }) &&
         "There should be no cyclic dependencies in the task graph");

  assert(std::all_of(List.begin(), List.end(),
                     [](TaskPtr item) { return item != nullptr; }) &&
         "There should be no null pointers in the topologically ordered list");

  return List;
}

std::list<TaskPtr> TopologicalOrder(kmp_target_task_map_t *TaskMap) {
  assert(TaskMap != nullptr);
  return TopologicalOrder(*TaskMap);
}

std::list<TaskPtr> ReverseTopologicalOrder(kmp_target_task_map_t &TaskMap) {
  auto List = TopologicalOrder(TaskMap);
  List.reverse();
  return List;
}

std::list<TaskPtr> ReverseTopologicalOrder(kmp_target_task_map_t *TaskMap) {
  assert(TaskMap != nullptr);
  return ReverseTopologicalOrder(*TaskMap);
}
