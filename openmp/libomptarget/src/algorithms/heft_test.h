//===------------------ heft_test.h - HEFTTest scheduler ------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Heterogeneous Earliest Finish Time algorithm.
//
//===----------------------------------------------------------------------===//

#ifndef _OMPTARGET_HEFTTest_H
#define _OMPTARGET_HEFTTest_H

#include "basescheduler.h"
#include "device.h"

#include <deque>

#include "llvm/Support/JSON.h"

// Forward declaration
class HEFTTestScheduler;

namespace detail {

using namespace llvm;

// Types
// =============================================================================

/// Priority queue, ordered from the higher upward rank to the lowest.
using PriorityQueue = std::deque<std::pair<TaskPtr, long>>;

// Classes
// =============================================================================

/// Context of the HEFT scheduler.
///
/// This is the class that does all of the heavy lifting in terms of computing
/// the intermediate values for the HEFT algorithm. For clarification about what
/// each of these data structures mean, refer to the original HEFT paper.
///
/// \note As an implementation detail, this class makes heavy use of
/// `std::unordered_map` (hash) maps because of its almost-constant lookup time,
/// in constrast to `std::map`'s logarithm lookup.
///
/// \see HEFTScheduler
/// \see https://ieeexplore.ieee.org/document/993206
class HEFTTestContext {
public:
  using IntMap = std::unordered_map<int32_t, int32_t>;

  using LongMap = std::unordered_map<int32_t, long>;
  
  using LongMapSet = std::unordered_map<int32_t, LongMap>;

  using LongMapSetSet = std::unordered_map<int32_t, LongMapSet>;

  /// Set of Schedules for different partial tradeoff solutions
  using ScheduleMapSet = std::unordered_map<int32_t, ScheduleMap>;

  /// Map a (task, device) pair to a timepoint.
  ///
  /// This table is filled by the scheduler in order to find the device which
  /// minimizes the earliest finish time.
  using EarliestTimeMap =
      std::unordered_map<TaskPtr,
                         std::unordered_map<DeviceId, long, DeviceId::Hash>>;

  /// Set of EarliestTimeMap for different partial tradeoff solutions
  using EarliestTimeMapSet = std::unordered_map<int32_t,EarliestTimeMap>;

  /// Maps a task to a timepoint.
  ///
  /// This table indicates the actual start or finish time of a task after being
  /// scheduled.
  using ActualTimeMap = std::unordered_map<TaskPtr, long>;

  /// Set of ActualTimeMap for different partial tradeoff solutions
  using ActualTimeMapSet = std::unordered_map<int32_t, ActualTimeMap>;

  /// Device availability map.
  ///
  /// Maps a device ID to the next timepoint it will be available for executing
  /// a new task.
  using DeviceAvailMap = std::unordered_map<DeviceId, long, DeviceId::Hash>;

  /// Set of DeviceAvailMap for different partial tradeoff solutions
  using DeviceAvailMapSet = std::unordered_map<int32_t, DeviceAvailMap>;

  /// Upward rank map.
  ///
  /// Maps a task to its upward rank priority.
  using UpwardRankMap = std::unordered_map<TaskPtr, long>;

  /// Communication map.
  ///
  /// Indicates how many bytes should be transferred from one task to another.
  using CommunicationMap =
      std::unordered_map<TaskPtr, std::unordered_map<TaskPtr, long>>;

  /// Computation map.
  ///
  /// Indicates how long each task should take to execute after being scheduled.
  using ComputationMap = std::unordered_map<TaskPtr, long>;

  friend class ::HEFTTestScheduler;

public:
  /// Default constructor.
  HEFTTestContext(ScheduleMap &Schedule, kmp_target_task_map_t *TaskMap);

  /// Destructor.
  ~HEFTTestContext() = default;

  /// Compute upward rank for every task in the graph.
  ///
  /// This should be called **before** starting the scheduling process.
  void computeUpwardRanks();

  /// Compute the earliest finish time for a specific (task, rank) pair.
  ///
  /// \returns the computed EFT.
  //long computeEFT(TaskPtr Task, DeviceId Device, int32_t Tradeoff);

  /// Schedule the task on the best rank available based on the EFT
  /// calculations.
  ///
  /// \returns the actual finish time of the task.
  //long scheduleTaskOnBestRank(TaskPtr Task);

  void scheduleTaskOnRank(TaskPtr Task, DeviceId Device, int32_t Tradeoff);

  /// Force a task to be scheduled on a specific rank irrespective its
  /// predecessors.
  ///
  /// \param task the task
  /// \param rank the rank
  //void forceScheduleTask(TaskPtr Task, DeviceId Device);

  /// Defer scheduling of task for later.
  ///
  /// \param task the task
  void deferTask(TaskPtr Task);

  /// Returns a boolean indicating if this task is deferred.
  ///
  /// \param task the task
  inline bool isDeferredTask(const kmp_target_task_t &Task) const {
    bool failed = isTargetDataTask(Task) &&
                  Schedule[Task.task_id].state == TaskState::FAILED;
    return Schedule[Task.task_id].state == TaskState::DEFERRED || failed;
  }

  /// Build a priority queue from the upward rank map.
  ///
  /// \returns the priority queue built.
  PriorityQueue buildQueue() const;

  /// Build a list of device ids from the total number of ranks and handler
  /// threads.
  std::vector<DeviceId> buildDeviceList(int32_t NumRanks) const;

  /// Clear the structures used by the context.
  void clear();

  /// Set a new task map.
  void setTaskMap(kmp_target_task_map_t *NewTaskMap);

  // Dump internal HEFTTest information to a file.
  void dump(int32_t NumDevices);

private:
  /// Compute the earliest start time for a specific (task, rank) pair.
  ///
  /// \returns the computed EST.
  //long computeEST(TaskPtr Task, DeviceId Device, int32_t Tradeoff);

  /// Commit a task to run on a specific rank.
  ///
  /// The task must not be rescheduled after commit.
  ///
  /// \returns the actual finish time of the task.
  //long scheduleTask(TaskPtr Task, DeviceId Device);

  /// Estimate the communication cost between two adjacent tasks.
  ///
  /// \param A the current task
  /// \param RankA the rank where A's data should read from
  /// \param B the successor task
  /// \param RankB the rank where B's data should write to
  ///
  /// \returns the communication cost from A -> B.
  long computeCommCost(const kmp_target_task_t &A, DeviceId DeviceA,
                         const kmp_target_task_t &B, DeviceId DeviceB);
                         
  ///
  long computeEnerCommCost(const kmp_target_task_t &A, DeviceId DeviceA,
                         const kmp_target_task_t &B, DeviceId DeviceB);

  /// Estimate the average communication cost between two adjacent tasks.
  ///
  /// \param A the current task
  /// \param B the successor task
  ///
  /// \returns the average communication cost from A -> B
  long computeAvgCommCost(const kmp_target_task_t &A,
                            const kmp_target_task_t &B);

  /// Estimate the average computation cost of a task.
  ///
  /// \param task the task
  ///
  /// \retunrs the average computation cost
  long computeAvgCompCost(const kmp_target_task_t &Task);

  /// Estimate the average computation cost of a task running on a specific
  /// rank.
  ///
  /// \param task the task
  /// \param trank the rank of the task
  ///
  /// \retunrs the average computation cost
  long computeCompCost(const kmp_target_task_t &Task, DeviceId Device);
  
  ///
  long computeEnerCompCost(const kmp_target_task_t &Task, DeviceId Device);

  ///
  void copySchedule(int Destination, int Origin);

  ///
  long** sortCrowdingDistances(long **Objectives, int CurrNumSchedules);

  ///
  int removeDominated(long **Objectives, int CurrNumSchedules);

  ///
  void selectFirstSchedules(long **CrowdingDistanceSet, long **Objectives, int NumSchedules);

  ///
  void selectSchedule(int ScheduleIndex);

private:
  /// Number of tradeoff schedules.
  int NumSchedules;
  /// Type of schedule selection:
  /// 0 - direct index - "index" (default)
  /// 1 - max absolute increase in relation to min makespan - "max_total"
  /// 2 - max rate increase in relation to min makespan - "max_rate"
  /// Index of the schedule to be chosen from tradeoffs. (Or the highest tradeoff, if this
  /// index is higher than the number of final tradeoffs.)
  bool ScheduleSelectionIndex;
  int SelectScheduleIndex;
  bool ScheduleSelectionTotal;
  long SelectScheduleTotal;
  bool ScheduleSelectionRatio;
  double SelectScheduleRatio;
  bool ScheduleSelectionRate;
  double SelectScheduleRate;

  /// Array of objectives values for tradeoffs.
  long **Objectives;

  /// Current graph id.
  int CurrentGraph;
  /// Number of tasks from input.
  int NumInputTasks;
  /// Number of nodes from input.
  int NumInputRanks;
  /// Number of devices from input.
  int NumInputDevices;
  /// Number of green energy intervals from input. // GUI for experiment only
  int NumInputIntervals; // GUI for experiment only
  /// Number of threads in each node to choose from.
  IntMap NumThreads;
  /// Communication cost of nodes
  LongMapSet CommCosts;
  /// Computation cost of tasks
  LongMapSetSet CompCosts;
  /// Energy consumption in each node while executing.
  LongMap ExecEner;
  /// Energy consumption in each node while idle.
  LongMap IdleEner;
  /// Amount of green energy available in each node per time unit. // GUI for experiment only
  LongMapSet GreenEnerAvail; // GUI for experiment only
  /// Energy consumed by communication of nodes
  LongMapSet EnerCommCosts;
  /// Energy consumed by computation of tasks
  LongMapSet EnerCompCosts;
  /// Average communication cost of tasks
  long AvgCommCost;
  /// List of tasks to be scheduled
  kmp_target_task_map_t *TaskMap;
  
  /// Task ID in the current Graph.
  IntMap TaskGraphID;

  /// State of the scheduler
  ScheduleMap &Schedule;
  /// Earliest start time
  //EarliestTimeMap EST;
  /// Earliest finish time
  //EarliestTimeMap EFT;
  /// Actual start time
  //ActualTimeMap AST;
  /// Actual finish time
  //ActualTimeMap AFT;
  /// Device availability
  //DeviceAvailMap DeviceAvail;

  /// Set of temporary tradeoff schedules
  ScheduleMapSet Schedules;
  /// Set of final tradeoff schedules
  //ScheduleMapSet FinalSchedules;
  /// Tradeoffs earliest start time
  //EarliestTimeMapSet EST;
  /// Tradeoffs earliest finish time
  //EarliestTimeMapSet EFT;
  /// Tradeoffs actual start time
  ActualTimeMapSet AST;
  /// Tradeoffs actual finish time
  ActualTimeMapSet AFT;
  /// Tradeoffs device availability
  DeviceAvailMapSet DeviceAvail;
  /// Makespans ggg
  LongMap Makespans;
  /// Makespans ggg // GUI for experiment only
  LongMap TotalBrownConsumptions; // GUI for experiment only
  /// Makespans ggg 
  LongMap TotalConsumptions;
  /// IntervalDeviceConsumption ggg
  LongMapSetSet IntervalDeviceConsumption;
  
  //DeviceAvailMapSet *IntervalDeviceConsumption;
  /// Final tradeoffs actual start time
  //ActualTimeMapSet FinalASTs;
  /// Final tradeoffs actual finish time
  //ActualTimeMapSet FinalAFTs;
  /// Final tradeoffs device availability
  //DeviceAvailMapSet FinalDeviceAvails;
  /// Tradeoffs device total time idle  //TODO review this 
  //DeviceAvailMapSet DeviceIdleTime[1200];
  /// Tradeoffs device total time in use  //TODO review this 
  //DeviceAvailMapSet DeviceActiveTime[1200];
  /// Tradeoffs device energy consumption while in use //TODO review this 
  //DeviceAvailMapSet DeviceActiveEnergy[1200];
  /// Total energy consumption for communication //TODO review this 
  //long TotalCommEnergy[1200];

  /// Upward ranks
  UpwardRankMap UpwardRank;
  /// Communication map
  CommunicationMap CommunicationCost;
  /// Computation map
  //ComputationMap ComputationCost;
  /// File where to dump HEFTTest information
  std::string HEFTLogPath;
  /// JSON object with profile info
  json::Object ProfileInfo;
};

} // namespace detail

/// Implementation of the scheduler interface for HEFTTest.
///
/// This class provides a simple interface that external users can call to run
/// the HEFTTest scheduler.
///
/// \see HEFTTestContext
class HEFTTestScheduler final : public BaseScheduler {
public:
  /// Default constructor.
  HEFTTestScheduler() : Ctx(this->schedule_map, this->task_map) {}

  /// Destructor.
  ~HEFTTestScheduler() {}

public:
  std::unordered_map<int32_t, std::set<int>> dataTaskSuccessorRanks;

private:
  /// Run HEFTTest scheduling algorithm.
  void scheduleGraph(const RTLInfoTy *device) override;

  /// Clear context after acquiring new graph.
  void acquireNewGraphPost() override;

  /// Do nothing.
  void preScheduleTask(int32_t task_id, const RTLInfoTy *device) override {}

public:
  /// Dump the task graph with HEFTTest-specific information.
  void dumpTaskGraph() override;

private:
  detail::HEFTTestContext Ctx;

private:
  struct DumpOptions {
    DumpOptions();
    /// Whether to dump the communication cost in the edges
    bool EdgeLabel = true;
    /// Whether to show predecessors or not
    bool Predecessors = false;
    /// Wether to dump a fake, unique entry node
    bool FakeEntryNode = false;
    /// Wether to dump a fake, unique exit node
    bool FakeExitNode = false;
  };

  DumpOptions Dump;
};

#endif /* _OMPTARGET_HEFTTest_H */
