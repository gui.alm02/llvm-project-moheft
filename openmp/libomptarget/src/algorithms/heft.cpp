//===---------------------- heft.cpp - HEFT scheduler ---------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Multi-Objective Heterogeneous Earliest Finish Time scheduling algorithm.
//
//===----------------------------------------------------------------------===//

#include "heft.h"
#include "ft.h"
#include "profiler.h"

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

namespace detail {

// Implementation of HEFTContext
// =============================================================================

HEFTContext::HEFTContext(ScheduleMap &Schedule, kmp_target_task_map_t *TaskMap)
    : CurrentGraph(0), 
    TaskMap(TaskMap), Schedule(Schedule), ProfileInfo({}) {
  PROF_SCOPED(PROF_LVL_ALL, "HEFTContext / Constructor");

  std::cout << "in heft.cpp \n"; //GGGDisplay

  if (TaskMap != nullptr)
    computeUpwardRanks();
  
  int rankId = 0; // used for indexing current rank input being treated
  int rank2Id = 0; // used for indexing current rank input being treated when two ranks being indexed
  int taskId = 0; // used for indexing current task input being treated
  int intervalId = 0; // used for indexing current interval input being treated
  bool InputComplete = false; // indicates if all needed inputs were received and treated
  bool treatInputDirectory = false; // indicates if all needed inputs read from files were received and treated //GGGNote may be merged with the field above

  NumInputDevices = 0; // Part of context, will contain the number of devices to distribute the tasks to
  NumInputRanks = 0; // Part of context, will contain the number of ranks
  NumInputIntervals = 0;

  std::string env_var_string = "OMPCLUSTER_SCHEDULER_INPUT_DIRECTORY"; // Environment variable containing path to input files
  std::string directoryString; // Path to input files directory
  std::string readRecord; // Current record read from current file
  
  // Check if current directory path is present
  const char* str = env_var_string.c_str();
  if (char *EnvStr = std::getenv(str))
  {
    treatInputDirectory = true;
    directoryString = EnvStr;
  }

  // Treats all input variables
  while (treatInputDirectory)
  {
    std::ifstream readFile1(directoryString+"/OMPCLUSTER_HEFT_RANKS");
    getline (readFile1, readRecord);
    //std::cout << "Read record 1: " << readRecord <<"\n";
    if(readRecord != "OMPCLUSTER_HEFT_RANKS")
    {
      treatInputDirectory = false;
      break;
    }
    getline (readFile1, readRecord);
    //std::cout << "Read record 2: " << readRecord <<"\n";
    if(readRecord == "OMPCLUSTER_NUM_EXEC_EVENT_HANDLERS_P_x")
    {
      //NumThreads = new LongMap; //
      getline (readFile1, readRecord);
      //std::cout << "Read record 3: " << readRecord <<"\n";
      int substrStart = 0;
      int substrCur = 0;
      int substrEnd = readRecord.length();
      for(substrCur = 0; substrCur < substrEnd; substrCur++)
      {
        if(readRecord[substrCur] == ',')
        {
          NumThreads[rankId] = std::stoi(readRecord.substr(substrStart, substrCur-substrStart));
          NumInputDevices += NumThreads[rankId++];
          substrStart = substrCur+1;
        }
      }
      NumThreads[rankId] = std::stoi(readRecord.substr(substrStart, substrCur-substrStart));
      NumInputDevices += NumThreads[rankId++];
      NumInputRanks = rankId;
    }
    else
    {
      treatInputDirectory = false;
      break;
    }

    for (int i = 0; i < NumInputRanks; i++) { //GGG
      //std::cout << "NumThreads[" << i << "]: " << NumThreads[i] << "\n"; //GGG
    } //GGG
    //std::cout << "NumInputRanks: " << NumInputRanks << "\n"; //GGG
    //std::cout << "NumInputDevices: " << NumInputDevices << "\n"; //GGG

    getline (readFile1, readRecord);
    //std::cout << "Read record 4: " << readRecord <<"\n";
    if(readRecord == "OMPCLUSTER_HEFT_COMM_P_x_P_y")
    {
      AvgCommCost = 0;
      //getline (readFile, readRecord);
      //std::cout << "Read record 5: " << readRecord <<"\n";
      rankId = 0;
      for (rankId = 0; rankId < NumInputRanks; rankId++) {
        getline (readFile1, readRecord);
        //std::cout << "Read record 6: " << readRecord <<"\n";
        rank2Id = 0;
        int substrStart = 0;
        int substrCur = 0;
        int substrEnd = readRecord.length();
        //std::cout << "GGG step 01 \n";
        for(substrCur = 0; substrCur < substrEnd; substrCur++)
        {
          //std::cout << "GGG step 02 \n";
          if(readRecord[substrCur] == ',')
          {
            //std::cout << "GGG step 03 "<<rankId<<" - "<<rank2Id<<" \n";
            CommCosts[rankId][rank2Id] = (long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))*1000);
            //std::cout << "G CommCosts["<<rankId<<"]["<<rank2Id<<"]: " << CommCosts[rankId][rank2Id] <<"\n";
            AvgCommCost += (CommCosts[rankId][rank2Id] * (NumThreads[rankId] * NumThreads[rank2Id]));
            
            //std::cout << "CommCosts[" << rankId << "][" << rank2Id << "]: " << CommCosts[rankId][rank2Id] <<"\n";
            //std::cout << "AvgCommCost tot: " << AvgCommCost <<"\n";
            substrStart = substrCur+1;
            rank2Id++;
          }
        }
        CommCosts[rankId][rank2Id] = (long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))*1000);
        AvgCommCost += (CommCosts[rankId][rank2Id] * (NumThreads[rankId] * NumThreads[rank2Id]));
        //std::cout << "CommCosts[" << rankId << "][" << rank2Id << "]: " << CommCosts[rankId][rank2Id] <<"\n";
        //std::cout << "AvgCommCost tot: " << AvgCommCost <<"\n";
      }
      AvgCommCost /= (NumInputDevices * (NumInputDevices-1)); //-1 from main thread in main rank //GUI ver como tratar rank 0 e suas threads
      //std::cout << "AvgCommCost: " << AvgCommCost << "\n"; //GGG
    }
    else
    {
      treatInputDirectory = false;
      break;
    }

    std::cout << "END read file\n"; //GGG
    InputComplete = true;
    break;
  }

  if (char *EnvStr = std::getenv("OMPCLUSTER_HEFT_LOG"))
    HEFTLogPath = EnvStr;

  //TODO use information from Profile file
  char *HEFTProfilePath = std::getenv("OMPCLUSTER_HEFT_USE_PROFILE");
  std::ifstream ProfileFile{HEFTProfilePath};
  std::stringstream Buffer;

  if (ProfileFile) {
    Buffer << ProfileFile.rdbuf();
    Expected<json::Value> V = json::parse(Buffer.str());
    json::Object *ProfileInfoPtr = V ? V.get().getAsObject() : nullptr;
    if (ProfileInfoPtr) {
      ProfileInfo = *ProfileInfoPtr;
    } else {
      DP("[HEFT] JSON profile `%s` is malformed, ignoring variable.\n",
         HEFTProfilePath);
    }
  } else {
    DP("[HEFT] JSON profile `%s` not found, ignoring variable.\n",
       HEFTProfilePath);
  }
}

void HEFTContext::computeUpwardRanks() {
  PROF_SCOPED(PROF_LVL_ALL, "HEFTContext / Compute Upward Ranks");
  DP("[HEFT] Computing upward rank for all tasks.\n");
  assert(TaskMap != nullptr && "The task map should not be NULL");

  int taskGraphId = 0;
  TaskGraphID.clear();
  for (auto *Task : ReverseTopologicalOrder(TaskMap)) {
    //TODO create task_graph_id
    //std::cout << "Task task_id: " << Task->task_id << "\n"; //GGG

    if (TaskGraphID.find(Task->task_id) != TaskGraphID.end())
      std::cout << "task_id " << Task->task_id << " is repeated \n";

    TaskGraphID[Task->task_id] = NumInputTasks-taskGraphId;
    TaskGraphID[Task->task_id]--;
    taskGraphId++;
    //std::cout << "Task graph_id: " << TaskGraphID[Task->task_id] << "\n"; //GGG
    long MaxSuccUrankComm = 0.0;
    for (auto &Succ : Successors(Task)) {
      MaxSuccUrankComm =
          std::max(MaxSuccUrankComm,
                   UpwardRank[&Succ] + computeAvgCommCost(*Task, Succ));
    }
    UpwardRank[Task] = computeAvgCompCost(*Task) + MaxSuccUrankComm;
  }
}

long HEFTContext::computeEST(TaskPtr Task, DeviceId Device) {
  long MaxPredEST = 0.0;

  // Iterate over predecessors
  for (const auto &Pred : Predecessors(Task)) {

    // Ignore deferred tasks for now
    if (isDeferredTask(*Task))
      continue;

    assert(Schedule.count(Pred.task_id) == 1 &&
           "Predecessor should already be scheduled!");

    DeviceId DevicePred(Schedule[Pred.task_id]);
    auto Candidate =
        AFT[&Pred] + computeCommCost(Pred, DevicePred, *Task, Device);

    if (Candidate > MaxPredEST)
      MaxPredEST = Candidate;
  }

  // Find the earliest start time if task is scheduled on device.
  EST[Task][Device] = std::max(DeviceAvail[Device], MaxPredEST);
  return EST[Task][Device];
}

long HEFTContext::computeEFT(TaskPtr Task, DeviceId Device) {
  // Calculate EST for task id, rank pair if it does not exist
  if (EST.count(Task) == 0 || EST[Task].count(Device) == 0)
    computeEST(Task, Device);

  EFT[Task][Device] = computeCompCost(*Task, Device) + EST[Task][Device];
  return EFT[Task][Device];
}

long HEFTContext::scheduleTask(TaskPtr Task, DeviceId Device) {
  // Do not change state of FAILED tasks during the reschedule
  bool IsRestartingTask = Schedule[Task->task_id].state == TaskState::FAILED;

  Schedule[Task->task_id] = {
      .state = IsRestartingTask ? TaskState::FAILED : TaskState::SCHEDULED,
      .rank = Device.Rank,
      .thread = Device.Thread,
      .translated_rank = -1,
      .task = Task,
  };

  AST[Task] = EST[Task][Device];
  AFT[Task] = EFT[Task][Device];
  return AFT[Task];
}

long HEFTContext::scheduleTaskOnBestRank(TaskPtr Task) {
  const auto &EFTs = EFT.at(Task);

  auto BestRank = std::min_element(
      EFTs.begin(), EFTs.end(),
      [](const auto &L, const auto &R) { return L.second < R.second; });

  DeviceAvail[BestRank->first] = scheduleTask(Task, BestRank->first);
  return DeviceAvail[BestRank->first];
}

void HEFTContext::forceScheduleTask(TaskPtr Task, DeviceId Device) {
  computeEFT(Task, Device);
  scheduleTaskOnBestRank(Task);
}

void HEFTContext::deferTask(TaskPtr Task) {
  // Do not change state of FAILED tasks during the reschedule
  if (Schedule[Task->task_id].state != TaskState::FAILED)
    Schedule[Task->task_id].state = TaskState::DEFERRED;
}

PriorityQueue HEFTContext::buildQueue() const {
  PROF_SCOPED(PROF_LVL_ALL, "HEFTContext / Build Queue");

  PriorityQueue Pqueue{UpwardRank.begin(), UpwardRank.end()};

  std::sort(Pqueue.begin(), Pqueue.end(),
            [](const auto &L, const auto &R) { return L.second > R.second; });

  return Pqueue;
}

std::vector<DeviceId> HEFTContext::buildDeviceList(int32_t NumRanks) const {
  std::vector<DeviceId> Devices;
  Devices.reserve(NumRanks);
  for (int32_t R = 1; R <= NumRanks; R++) //TODO review if rank 0 may have more threads
    //for (int32_t H = 0; H < NumThreads[R]; H++) //GGG solve this fast!
    for (int32_t H = 0; H < 1; H++)
      Devices.emplace_back(R, H);
  return Devices;
}

void HEFTContext::clear() {
  PROF_SCOPED(PROF_LVL_ALL, "HEFTContext / Clear");
  AST.clear();
  AFT.clear();
  DeviceAvail.clear();

  UpwardRank.clear();
  CommunicationCost.clear();
}

void HEFTContext::setTaskMap(kmp_target_task_map_t *NewTaskMap) {
  assert(NewTaskMap != nullptr && "Expected a non-null task map!");
  TaskMap = NewTaskMap;
}

long HEFTContext::computeCommCost(const kmp_target_task_t &A,
                                    DeviceId DeviceA,
                                    const kmp_target_task_t &B,
                                    DeviceId DeviceB) {
  // If the tasks are to execute on the same rank, we do not pay for the
  // communication because no data needs to be moved.
  if (DeviceA.Rank == DeviceB.Rank)
    return 0.0;

  return CommCosts[DeviceA.Rank][DeviceB.Rank] * CommunicationCost[&A][&B];
}

/// Search for all `out` dependencies of A which are also `in` dependencies of B
/// and accumulate the size of those dependencies (in bytes).
long HEFTContext::computeAvgCommCost(const kmp_target_task_t &A,
                                       const kmp_target_task_t &B) {
  long TotalBytes = 0.0;

  // Look for arguments which are common between tasks A and B and accumulate
  // their sizes in bytes.
  for (int32_t I = 0; I < A.target_data.num_args; I++) {
    for (int32_t J = 0; J < B.target_data.num_args; J++) {
      if (A.target_data.args[I] == B.target_data.args[J]) {
        TotalBytes += A.target_data.arg_sizes[I];
      }
    }
  }

  CommunicationCost[&A][&B] = TotalBytes;

  DP("[HEFT] Dependency `%d -> %d` has average cost: %lf KB\n", A.task_id,
     B.task_id, TotalBytes / 1024.0);

  return AvgCommCost * TotalBytes;
}

long HEFTContext::computeAvgCompCost(const kmp_target_task_t &Task) {
  if (isTargetDataTask(Task))
    return 1.0;

  long AvgCommCost = 0;
  for (int i = 0; i < NumInputRanks; i++) {
    AvgCommCost += CompCosts[CurrentGraph][i][TaskGraphID[Task.task_id]]; //TODO update with task_graph_id
  }
  AvgCommCost /= NumInputRanks;
  return AvgCommCost;
}

long HEFTContext::computeCompCost(const kmp_target_task_t &Task,
                                    DeviceId Device) {
  return CompCosts[CurrentGraph][Device.Rank][TaskGraphID[Task.task_id]]; //TODO update with task_graph_id
}

/// Dump internal HEFT information to a file.
void HEFTContext::dump(int32_t NumDevices) {
  PROF_SCOPED(PROF_LVL_ALL, "HEFTContext / Dump");

  if (HEFTLogPath.empty())
    return;

  FILE *fp = fopen(HEFTLogPath.c_str(), "w");

  if (fp == NULL) {
    DP("[HEFT] Failed to open log file.\n");
    return;
  }

  auto Devices = buildDeviceList(NumDevices);

  DP("[HEFT] Dumping HEFT stats to file '%s'\n", HEFTLogPath.c_str());

  // FIXME: Print the header with correct columns for EST/EFT
  fprintf(fp,
          "%-4s  %-10s  %-10s  %-10s  %-10s  %-10s  %-7s  %-7s  %-7s  %-7s"
          "%-34s  %-34s\n",
          "ID", "FNID", "Rank", "AST", "AFT", "Dev", "Root", "Exit", "Data",
          "Classic", "EST", "EFT");

  std::vector<kmp_target_task_t *> TasksV;
  TasksV.reserve(TaskMap->num_tasks);
  for (int i = 0; i < TaskMap->num_tasks; i++) {
    TasksV.push_back(&(TaskMap->tasks[i]));
  }

  std::sort(TasksV.begin(), TasksV.end(),
            [&](kmp_target_task_t *A, kmp_target_task_t *B) {
              return UpwardRank[A] > UpwardRank[B];
            });

/* //TODO return this for MOHEFT
  // Iterate over tasks
  for (auto const *task_ptr : TasksV) {
    auto &task = *task_ptr;
    uint64_t ofid = (uint64_t)task.target_data.outlined_fn_id;
    fprintf(fp,
            "%04d  0x%08lx  %10.2lf  %10.2lf  %10.2lf  %7d  %7d  %7d  %7d  %4d",
            task.task_id, ofid, UpwardRank[&task], AST[0][&task], AFT[0][&task],
            Schedule[task.task_id].rank, isRootTask(task), isExitTask(task),
            isTargetDataTask(task), isClassicalTask(task));
    for (const auto &Device : Devices)
      fprintf(fp, "  %10.2lf", EST[&task][Device]);
    for (const auto &Device : Devices)
      fprintf(fp, "  %10.2lf", EFT[&task][Device]);
    fprintf(fp, "\n");
  }
*/
  fprintf(fp, "\n");
  fprintf(fp, "Dependencies\n");
  fprintf(fp, "-------------------------------------------------------\n");

  for (const auto &A : CommunicationCost) {
    auto id_A = A.first->task_id;
    for (const auto &B : A.second) {
      auto id_B = B.first->task_id;
      fprintf(fp, "%04d -> %04d : %10.4ld\n", id_A, id_B, B.second);
    }
  }

  fprintf(fp, "\n");
  fprintf(fp, "Schedule Map\n");
  fprintf(fp, "-------------------------------------------------------\n");

  for (const auto &s : Schedule) {
    fprintf(fp, "%04d: %04d %d %d\n", s.first, s.second.task->task_id,
            s.second.rank, s.second.state);
  }

  fclose(fp);
}

} // namespace detail

// Implementation of HEFTScheduler
// =============================================================================

HEFTScheduler::DumpOptions::DumpOptions() {
  if (char *Value = std::getenv("OMPCLUSTER_HEFT_DUMP_EDGE_LABEL"))
    EdgeLabel = atoi(Value) > 0;
  if (char *Value = std::getenv("OMPCLUSTER_HEFT_DUMP_PRED"))
    Predecessors = atoi(Value) > 0;
  if (char *Value = std::getenv("OMPCLUSTER_HEFT_DUMP_FAKE_ENTRY"))
    FakeEntryNode = atoi(Value) > 0;
  if (char *Value = std::getenv("OMPCLUSTER_HEFT_DUMP_FAKE_EXIT"))
    FakeExitNode = atoi(Value) > 0;
}

/// This is the high-level implementation of the HEFT algorithm as is described
/// in the original paper.
///
/// \see https://ieeexplore.ieee.org/document/993206
void HEFTScheduler::scheduleGraph(const RTLInfoTy *Device) {
  PROF_SCOPED(PROF_LVL_ALL, "HEFTScheduler / Schedule Graph");

  using detail::DeviceId;
  auto PQueue = Ctx.buildQueue();
  auto NumDevices = ft_handler.DevicesGetActiveNumber();
  auto Devices = Ctx.buildDeviceList(NumDevices);

  DP("[SCHEDULER] Scheduling graph with HEFT algorithm:\n"
     "[SCHEDULER] --> Number of tasks: %d\n"
     "[SCHEDULER] --> Number of ranks: %d\n",
     task_map->num_tasks, NumDevices);

  while (not PQueue.empty()) {
    const auto *Task = PQueue.front().first;
    PQueue.pop_front();

    PROF_SCOPED(PROF_LVL_ALL, "HEFTScheduler / Schedule Task",
                [&]() -> std::string {
                  std::stringstream ss;
                  ss << "{";
                  ss << "\"task_id\":" << Task->task_id << ",";
                  ss << "\"remaining_tasks\":" << PQueue.size();
                  ss << "}";
                  return ss.str();
                }());

    // Do not reschedule already executed or saved tasks
    const auto Data = schedule_map[Task->task_id];
    if (Data.state > TaskState::SCHEDULED && Data.state != TaskState::FAILED)
      continue;

    // Defer the scheduling for target data tasks
    if (isTargetDataTask(Task)) {
      Ctx.deferTask(Task);
      continue;
    }

    // Classical tasks are pinned in the head node (rank=0)
    if (isClassicalTask(Task)) {
      Ctx.forceScheduleTask(Task, DeviceId::head());
      continue;
    }

    for (const auto &Device : Devices)
      Ctx.computeEFT(Task, Device);

    Ctx.scheduleTaskOnBestRank(Task);
  }

  long resultMakespan = 0;
  for (const auto &Ending : Ctx.AFT ) {
    if (Ending.second > resultMakespan)
      resultMakespan = Ending.second;
  }
  std::cout << "Makespan: " << resultMakespan/1000 << "." << resultMakespan%1000 <<"\n";

  // Schedule deferred tasks
  for (auto &Entry : schedule_map) {
    auto &Data = Entry.second;
    const auto *Task = Data.task;

    // Do not reschedule already executed or saved tasks
    if (Data.state > TaskState::SCHEDULED && Data.state != TaskState::FAILED)
      continue;

    // These are already scheduled.
    if (not Ctx.isDeferredTask(*Task))
      continue;

    DP("[HEFT] Scheduling deferred task %d: targetdata=%d root=%d exit=%d "
       "nsuccs=%d npreds=%d\n",
       Task->task_id, isTargetDataTask(Task), isRootTask(Task),
       isExitTask(Task), NumSuccessors(Task), NumPredecessors(Task));

    long MaxCost = 0.0;
    int32_t Rank = 1;

    // Find the successor with highest communication cost
    // NOTE: Exit target data tasks are temporary scheduled but the rank will
    // be overwritten later with PinTask function.
    for (const auto &Succ : Successors(Task)) {
      auto SuccRank = schedule_map[Succ.task_id].rank;

      // Save successor ranks of each scheduled data task to detect broadcast
      // opportunites
      dataTaskSuccessorRanks[Task->task_id].insert(SuccRank);

      if (Ctx.CommunicationCost[Task][&Succ] > MaxCost &&
          !isClassicalTask(Succ) && SuccRank != -1) {
        MaxCost = Ctx.CommunicationCost[Task][&Succ];
        Rank = SuccRank;
      }
    }

    // Schedule this task to the same rank as the successor chosen before
    Ctx.forceScheduleTask(Task, DeviceId(Rank));
  }

  auto Predicate = [](const auto &Entry) {
    if (Entry.second.state >= TaskState::DISPATCHED)
      return true; // skip checking for executed/saved tasks
    return Entry.second.state == TaskState::SCHEDULED &&
                   isClassicalTask(Entry.second.task)
               ? Entry.second.rank == 0
               : Entry.second.rank > 0;
  };

  // Tasks can be DISPATCHED, SCHEDULED, EXECUTED or FAILED (because of
  // reschedule)

  assert(std::all_of(schedule_map.begin(), schedule_map.end(), Predicate) &&
         "Some tasks were not properly scheduled.");

  Ctx.dump(NumDevices);
  DP("[SCHEDULER] HEFT scheduled %d tasks to %d resources.\n",
     task_map->num_tasks, NumDevices);
}

/// Reset context and recompute upward ranks for the new graph
void HEFTScheduler::acquireNewGraphPost() {
  PROF_SCOPED(PROF_LVL_ALL, "HEFTScheduler / Acquire New Graph Post");

  Ctx.clear();
  Ctx.setTaskMap(task_map);
  Ctx.CurrentGraph++;

  std::string env_var_string = "OMPCLUSTER_SCHEDULER_INPUT_DIRECTORY"; // Environment variable containing path to input files
  std::string directoryString; // Path to input files directory
  std::string readRecord; // Current record read from current file
  
  // Check if current directory path is present
  const char* str = env_var_string.c_str();
  if (char *EnvStr = std::getenv(str))
  {
    //treatInputDirectory = true;
    directoryString = EnvStr;
  }

  //std::cout << "Read FILE: OMPCLUSTER_HEFT_TASKS_GRAPH_" << std::to_string(Ctx.CurrentGraph) <<" _ \n";
  std::ifstream readFileX(directoryString+"/OMPCLUSTER_HEFT_TASKS_GRAPH_"+std::to_string(Ctx.CurrentGraph));
    getline (readFileX, readRecord);
    //std::cout << "Read record A: " << readRecord <<"\n";
    /*
    if(readRecord != "OMPCLUSTER_HEFT_TASKS_GRAPH_"+std::to_string(CurrentGraph))
    {
      treatInputDirectory = false;
      break;
    }
    */
    getline (readFileX, readRecord);
    //std::cout << "Read record B: " << readRecord <<"\n";
    if(readRecord == "OMPCLUSTER_HEFT_COMP_G_x_P_y_T_z")
    {
      //std::cout << "pos CompCosts " << NumInputRanks << "\n"; //GGG
      int taskId = 0;
      for (int rankId = 0; rankId < Ctx.NumInputRanks; rankId++)
      {
        getline (readFileX, readRecord);
        //std::cout << "Read record 8: " << readRecord <<"\n";
        taskId = 0;
        int substrStart = 0;
        int substrCur = 0;
        int substrEnd = readRecord.length();
        Ctx.NumInputTasks = 0;
        while(substrCur < substrEnd){
          if(readRecord[substrCur] == ',')
            Ctx.NumInputTasks++;
          substrCur++;
        }
        //std::cout << "Ctx.CurrentGraph: " << Ctx.CurrentGraph <<"\n";
        Ctx.NumInputTasks++;
        for(substrCur = 0; substrCur < substrEnd; substrCur++)
        {
          if(readRecord[substrCur] == ',')
          {
            Ctx.CompCosts[Ctx.CurrentGraph][rankId][taskId] = (long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))*1000);
            //std::cout << "CompCosts[" << rankId << "][" << taskId << "]: " << CompCosts[rankId][taskId] <<"\n";
            substrStart = substrCur+1;
            taskId++;
          }
        }
        Ctx.CompCosts[Ctx.CurrentGraph][rankId][taskId] = (long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))*1000);
        //std::cout << "CompCosts[" << rankId << "][" << taskId << "]: " << CompCosts[rankId][taskId] <<"\n";
      }
    }
    /*
    else
    {
      treatInputDirectory = false;
      break;
    }
    */
   
  Ctx.computeUpwardRanks();

  std::cout << "CurrentGraph: " << Ctx.CurrentGraph << "\n"; //GGG
}

void HEFTScheduler::dumpTaskGraph() {
  PROF_SCOPED(PROF_LVL_ALL, "HEFTScheduler / Dump Task Graph");

  if (graph_dump_dir_path.empty())
    return;

  if (task_map == nullptr) {
    DP("[HEFT] Task map is null, skipping dump.\n");
    return;
  }

  std::ofstream dot(graph_dump_dir_path + "_graph_" +
                    std::to_string(graph_counter) + ".dot");
  // Print header
  dot << "// Generated in HEFTScheduler\n"
      << "digraph TargetTaskGraph {\n"
      << "  labelloc=\"t\";\n"
      << "  label=\"HEFT Scheduler\";\n";

  // For each task
  for (const auto &task : Tasks(task_map)) {
    int32_t tid = task.task_id;
    int32_t trank = schedule_map[tid].rank;
    std::string tinfo = schedule_map[tid].source_location;
    int32_t state = static_cast<int32_t>(schedule_map[tid].state);

    // Print graph node
    dot << "  V" << tid << " [label=<<B>ID " << tid
        << "</B><BR /><FONT POINT-SIZE=\"10\">Rank " << trank
        << "<BR /> State: " << state;

    if (!tinfo.empty()) {
      dot << "<BR />" << tinfo;
    }

    dot << "</FONT>>";

    if (isTargetDataTask(task))
      dot << " color=gray70 fontcolor=gray50";
    if (isRootTask(task) || isExitTask(task))
      dot << " shape=octagon";

    dot << "];\n";

    // Print forward arrows using the list of successors
    for (const auto &succ : Successors(task)) {
      int32_t sid = succ.task_id;
      int32_t srank = schedule_map[sid].rank;
      const auto *color = detail::getEdgeColor(trank, srank);
      long CommCost = Ctx.CommunicationCost[&task][&succ];

      // Print graph edge
      dot << "  V" << tid << " -> V" << sid << " [color=\"" << color << "\"";
      if (Dump.EdgeLabel)
        dot << " label=<" << CommCost << ">";
      dot << "];\n";
    }

    // Print backward arrows using the list of predecessors
    if (Dump.Predecessors) {
      for (const auto &pred : Predecessors(task)) {
        const auto &pid = pred.task_id;
        dot << "  V" << pid << " -> V" << tid
            << " [style=dashed dir=back color=gray30];\n";
      }
    }
  }

  // Print a single fake entry node
  if (Dump.FakeEntryNode) {
    dot << "  Entry;\n";
    for (const auto *root : Roots(task_map))
      dot << "  Entry -> V" << root->task_id << ";\n";
  }

  // Print a single fake exit node
  if (Dump.FakeExitNode) {
    dot << "  Exit;\n";
    for (const auto &task : Tasks(task_map))
      if (isExitTask(task))
        dot << "  V" << task.task_id << " -> Exit;\n";
  }

  dot << "}\n";
}
