//===-------- moheft_green_energy.cpp - MOHEFTGreenEnergy scheduler -------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Multi-Objective Heterogeneous Earliest Finish Time scheduling algorithm.
//
//===----------------------------------------------------------------------===//

#include "moheft_green_energy.h"
#include "ft.h"
#include "profiler.h"

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

namespace detail {

// Implementation of MOHEFTGreenEnergyContext
// =============================================================================

MOHEFTGreenEnergyContext::MOHEFTGreenEnergyContext(ScheduleMap &Schedule, kmp_target_task_map_t *TaskMap)
    : NumSchedules(1), ScheduleSelectionIndex(false), SelectScheduleIndex(1),
    ScheduleSelectionTotal(false), SelectScheduleTotal(0),
    ScheduleSelectionRatio(false), SelectScheduleRatio(0),
    ScheduleSelectionRate(false), SelectScheduleRate(0), CurrentGraph(0), 
    TaskMap(TaskMap), Schedule(Schedule), ProfileInfo({}) {
  PROF_SCOPED(PROF_LVL_ALL, "MOHEFTGreenEnergyContext / Constructor");

  std::cout << "in moheft_green_energy.cpp \n"; //GGGDisplay

  if (TaskMap != nullptr)
    computeUpwardRanks();
  
  int rankId = 0; // used for indexing current rank input being treated
  int rank2Id = 0; // used for indexing current rank input being treated when two ranks being indexed
  int taskId = 0; // used for indexing current task input being treated
  int intervalId = 0; // used for indexing current interval input being treated
  bool InputComplete = false; // indicates if all needed inputs were received and treated
  bool treatInputDirectory = false; // indicates if all needed inputs read from files were received and treated //GGGNote may be merged with the field above

  NumInputDevices = 0; // Part of context, will contain the number of devices to distribute the tasks to
  NumInputRanks = 0; // Part of context, will contain the number of ranks
  NumInputIntervals = 0;

  std::string env_var_string = "OMPCLUSTER_SCHEDULER_INPUT_DIRECTORY"; // Environment variable containing path to input files
  std::string directoryString; // Path to input files directory
  std::string readRecord; // Current record read from current file
  
  // Check if current directory path is present
  const char* str = env_var_string.c_str();
  if (char *EnvStr = std::getenv(str))
  {
    treatInputDirectory = true;
    directoryString = EnvStr;
  }

  // Treats all input variables
  while (treatInputDirectory)
  {
    //Treat first input file OMPCLUSTER_MOHEFT_SEL_CRIT
    std::ifstream readFile(directoryString+"/OMPCLUSTER_MOHEFT_SEL_CRIT");
    getline (readFile, readRecord);
    //std::cout << "Read record 1: " << readRecord <<"\n"; //GGGDisplay
    if(readRecord != "OMPCLUSTER_MOHEFT_SEL_CRIT")
    {
      treatInputDirectory = false;
      break;
    }
    getline (readFile, readRecord);
    //std::cout << "Read record 2: " << readRecord <<"\n"; //GGGDisplay
    if(readRecord == "OMPCLUSTER_MOHEFT_NUM_TRADEOFF_SCHEDULES")
    {
      getline (readFile, readRecord);
      //std::cout << "Read record 3: " << readRecord <<"\n"; //GGGDisplay
      NumSchedules = std::stoi(readRecord);
    }
    else
    {
      treatInputDirectory = false;
      break;
    }
    getline (readFile, readRecord);
    //std::cout << "Read record 4: " << readRecord <<"\n";
    if(readRecord == "OMPCLUSTER_MOHEFT_CRITERIA")
    {
      while (getline (readFile, readRecord)) {
        //std::cout << "Read record 5: " << readRecord <<"\n";
        int substrStart = 0;
        int substrCur = 0;
        int substrEnd = readRecord.length();
        for(substrCur = 0; substrCur < substrEnd; substrCur++)
        {
          if(readRecord[substrCur] == ',')
          {
            std::string criteriaType = readRecord.substr(substrStart, substrCur-substrStart);
            if(criteriaType == "index")
            {
              ScheduleSelectionIndex = true;
              SelectScheduleIndex = std::stoi(readRecord.substr(substrCur+1, readRecord.length()-substrCur));
            }
            else if(criteriaType == "total")
            {
              ScheduleSelectionTotal = true;
              SelectScheduleTotal = std::stol(readRecord.substr(substrCur+1, readRecord.length()-substrCur));
            }
            else if(criteriaType == "ratio")
            {
              ScheduleSelectionRatio = true;
              SelectScheduleRatio = std::stod(readRecord.substr(substrCur+1, readRecord.length()-substrCur));
            }
            else if(criteriaType == "rate")
            {
              ScheduleSelectionRate = true;
              SelectScheduleRate = std::stod(readRecord.substr(substrCur+1, readRecord.length()-substrCur));
            }
          }
        }
      }
      if(ScheduleSelectionIndex)
      {
        if(ScheduleSelectionTotal || ScheduleSelectionRatio || ScheduleSelectionRate)
        {
          treatInputDirectory = false;
          break;
        }
      }
      else if(!(ScheduleSelectionTotal || ScheduleSelectionRatio || ScheduleSelectionRate))
      {
        treatInputDirectory = false;
        break;
      }
    }
    else
    {
      treatInputDirectory = false;
      break;
    }
    
    std::ifstream readFile1(directoryString+"/OMPCLUSTER_HEFT_RANKS");
    getline (readFile1, readRecord);
    //std::cout << "Read record 1: " << readRecord <<"\n";
    if(readRecord != "OMPCLUSTER_HEFT_RANKS")
    {
      treatInputDirectory = false;
      break;
    }
    getline (readFile1, readRecord);
    //std::cout << "Read record 2: " << readRecord <<"\n";
    if(readRecord == "OMPCLUSTER_NUM_EXEC_EVENT_HANDLERS_P_x")
    {
      //NumThreads = new LongMap; //
      getline (readFile1, readRecord);
      //std::cout << "Read record 3: " << readRecord <<"\n";
      int substrStart = 0;
      int substrCur = 0;
      int substrEnd = readRecord.length();
      for(substrCur = 0; substrCur < substrEnd; substrCur++)
      {
        if(readRecord[substrCur] == ',')
        {
          NumThreads[rankId] = std::stoi(readRecord.substr(substrStart, substrCur-substrStart));
          NumInputDevices += NumThreads[rankId++];
          substrStart = substrCur+1;
        }
      }
      NumThreads[rankId] = std::stoi(readRecord.substr(substrStart, substrCur-substrStart));
      NumInputDevices += NumThreads[rankId++];
      NumInputRanks = rankId;
    }
    else
    {
      treatInputDirectory = false;
      break;
    }

    for (int i = 0; i < NumInputRanks; i++) { //GGG
      //std::cout << "NumThreads[" << i << "]: " << NumThreads[i] << "\n"; //GGG
    } //GGG
    //std::cout << "NumInputRanks: " << NumInputRanks << "\n"; //GGG
    //std::cout << "NumInputDevices: " << NumInputDevices << "\n"; //GGG

    getline (readFile1, readRecord);
    //std::cout << "Read record 4: " << readRecord <<"\n";
    if(readRecord == "OMPCLUSTER_HEFT_COMM_P_x_P_y")
    {
      AvgCommCost = 0;
      //getline (readFile, readRecord);
      //std::cout << "Read record 5: " << readRecord <<"\n";
      rankId = 0;
      for (rankId = 0; rankId < NumInputRanks; rankId++) {
        getline (readFile1, readRecord);
        //std::cout << "Read record 6: " << readRecord <<"\n";
        rank2Id = 0;
        int substrStart = 0;
        int substrCur = 0;
        int substrEnd = readRecord.length();
        //std::cout << "GGG step 01 \n";
        for(substrCur = 0; substrCur < substrEnd; substrCur++)
        {
          //std::cout << "GGG step 02 \n";
          if(readRecord[substrCur] == ',')
          {
            //std::cout << "GGG step 03 "<<rankId<<" - "<<rank2Id<<" \n";
            CommCosts[rankId][rank2Id] = (long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))*1000);
            //std::cout << "G CommCosts["<<rankId<<"]["<<rank2Id<<"]: " << CommCosts[rankId][rank2Id] <<"\n";
            AvgCommCost += (CommCosts[rankId][rank2Id] * (NumThreads[rankId] * NumThreads[rank2Id]));
            
            //std::cout << "CommCosts[" << rankId << "][" << rank2Id << "]: " << CommCosts[rankId][rank2Id] <<"\n";
            //std::cout << "AvgCommCost tot: " << AvgCommCost <<"\n";
            substrStart = substrCur+1;
            rank2Id++;
          }
        }
        CommCosts[rankId][rank2Id] = (long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))*1000);
        AvgCommCost += (CommCosts[rankId][rank2Id] * (NumThreads[rankId] * NumThreads[rank2Id]));
        //std::cout << "CommCosts[" << rankId << "][" << rank2Id << "]: " << CommCosts[rankId][rank2Id] <<"\n";
        //std::cout << "AvgCommCost tot: " << AvgCommCost <<"\n";
      }
      AvgCommCost /= (NumInputDevices * (NumInputDevices-1)); //-1 from main thread in main rank //GUI ver como tratar rank 0 e suas threads
      //std::cout << "AvgCommCost: " << AvgCommCost << "\n"; //GGG
    }
    else
    {
      treatInputDirectory = false;
      break;
    }

    /*
    CurrentGraph = 1;
    std::ifstream readFile2(directoryString+"/OMPCLUSTER_HEFT_TASKS_GRAPH_"+std::to_string(CurrentGraph));
    getline (readFile2, readRecord);
    //std::cout << "Read record 1: " << readRecord <<"\n";
    if(readRecord != "OMPCLUSTER_HEFT_TASKS_GRAPH_"+std::to_string(CurrentGraph))
    {
      treatInputDirectory = false;
      break;
    }
    getline (readFile2, readRecord);
    //std::cout << "Read record 7: " << readRecord <<"\n";
    if(readRecord == "OMPCLUSTER_HEFT_COMP_G_x_P_y_T_z")
    {
      //std::cout << "pos CompCosts " << NumInputRanks << "\n"; //GGG
      taskId = 0;
      for (rankId = 0; rankId < NumInputRanks; rankId++)
      {
        getline (readFile2, readRecord);
        //std::cout << "Read record 8: " << readRecord <<"\n";
        taskId = 0;
        int substrStart = 0;
        int substrCur = 0;
        int substrEnd = readRecord.length();
        NumInputTasks = 0;
        while(substrCur < substrEnd){
          if(readRecord[substrCur] == ',')
            NumInputTasks++;
          substrCur++;
        }
        //std::cout << "NumInputTasks: " << NumInputTasks <<"\n";
        NumInputTasks++;
        std::cout << "CurrentGraph: " << CurrentGraph <<"\n";
        for(substrCur = 0; substrCur < substrEnd; substrCur++)
        {
          if(readRecord[substrCur] == ',')
          {
            CompCosts[CurrentGraph][rankId][taskId] = (long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))*1000);
            //std::cout << "CompCosts[" << rankId << "][" << taskId << "]: " << CompCosts[rankId][taskId] <<"\n";
            substrStart = substrCur+1;
            taskId++;
          }
        }
        CompCosts[CurrentGraph][rankId][taskId] = (long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))*1000);
        //std::cout << "CompCosts[" << rankId << "][" << taskId << "]: " << CompCosts[rankId][taskId] <<"\n";
      }
    }
    else
    {
      treatInputDirectory = false;
      break;
    }
    //TODO if InputComplete, error
    //std::cout << "NumInputRanks: " << NumInputRanks << "\n"; //GGG
    //std::cout << "NumInputTasks: " << NumInputTasks << "\n"; //GGG
    for (int i = 0; i < NumInputRanks; i++) { //GGG
      for (int j = 0; j < NumInputTasks; j++) { //GGG
        //std::cout << "CompCosts[" << i << "]["<< j <<"]: " << CompCosts[i][j] << "\n"; //GGG
      } //GGG
    } //GGG
    CurrentGraph = 0;
    */

    std::ifstream readFile3(directoryString+"/OMPCLUSTER_MOHEFT_ENER_CONS");
    getline (readFile3, readRecord);
    //std::cout << "Read record 1: " << readRecord <<"\n";
    if(readRecord != "OMPCLUSTER_MOHEFT_ENER_CONS")
    {
      treatInputDirectory = false;
      break;
    }
    getline (readFile3, readRecord);
    //std::cout << "Read record 2: " << readRecord <<"\n";
    if(readRecord == "OMPCLUSTER_MOHEFT_IDLE_CONS_P_x")
    {
      getline (readFile3, readRecord);
      //std::cout << "Read record 3: " << readRecord <<"\n";
      rankId = 0;
      int substrStart = 0;
      int substrCur = 0;
      int substrEnd = readRecord.length();
      for(substrCur = 0; substrCur < substrEnd; substrCur++)
      {
        if(readRecord[substrCur] == ',')
        {
          IdleEner[rankId++] = ((long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))))*100;
          substrStart = substrCur+1;
        }
      }
      IdleEner[rankId] = ((long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))))*100;
    }
    else
    {
      treatInputDirectory = false;
      break;
    }
    getline (readFile3, readRecord);
    //std::cout << "Read record 4: " << readRecord <<"\n";
    if(readRecord == "OMPCLUSTER_MOHEFT_EXEC_CONS_P_x")
    {
      getline (readFile3, readRecord);
      //std::cout << "Read record 5: " << readRecord <<"\n";
      rankId = 0;
      int substrStart = 0;
      int substrCur = 0;
      int substrEnd = readRecord.length();
      for(substrCur = 0; substrCur < substrEnd; substrCur++)
      {
        if(readRecord[substrCur] == ',')
        {
          ExecEner[rankId++] = ((long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))))*100;
          substrStart = substrCur+1;
        }
      }
      ExecEner[rankId] = ((long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))))*100;
    }
    else
    {
      treatInputDirectory = false;
      break;
    }

    getline (readFile3, readRecord);
    //std::cout << "Read record 5: " << readRecord <<"\n";
    if(readRecord == "OMPCLUSTER_MOHEFT_CONS_P_x_P_y")
    {
      for (rankId = 0; rankId < NumInputRanks; rankId++) {
        getline (readFile3, readRecord);
        //std::cout << "Read record 6: " << readRecord <<"\n";
        rank2Id = 0;
        int substrStart = 0;
        int substrCur = 0;
        int substrEnd = readRecord.length();
        for(substrCur = 0; substrCur < substrEnd; substrCur++)
        {
          if(readRecord[substrCur] == ',')
          {
            EnerCommCosts[rankId][rank2Id] = ((long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))))*100;
            //std::cout << "EnerCommCosts[" << rankId << "][" << rank2Id << "]: " << EnerCommCosts[rankId][rank2Id] <<"\n";
            substrStart = substrCur+1;
            rank2Id++;
          }
        }
        EnerCommCosts[rankId][rank2Id] = ((long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))))*100;
        //std::cout << "EnerCommCosts[" << rankId << "][" << rank2Id << "]: " << EnerCommCosts[rankId][rank2Id] <<"\n";
      }
    }
    else
    {
      treatInputDirectory = false;
      break;
    }

    std::ifstream readFile4(directoryString+"/OMPCLUSTER_MOHEFT_GREEN_ENER");
    //std::cout << "Read record 10: " << readRecord <<"\n";
    getline (readFile4, readRecord);
    //std::cout << "Read record 11: " << readRecord <<"\n";
    if(readRecord != "OMPCLUSTER_MOHEFT_GREEN_ENER")
    {
      treatInputDirectory = false;
      break;
    }
    getline (readFile4, readRecord);
    //std::cout << "Read record 7: " << readRecord <<"\n";
    if(readRecord == "OMPCLUSTER_MOHEFT_GREEN_P_x_I_y")
    {
      for (rankId = 0; rankId < NumInputRanks; rankId++)
      {
        getline (readFile4, readRecord);
        //std::cout << "Read record 8: " << readRecord <<"\n";
        int substrStart = 0;
        int substrCur = 0;
        int substrEnd = readRecord.length();
        NumInputIntervals = 0;
        while(substrCur < substrEnd){
          if(readRecord[substrCur] == ',')
            NumInputIntervals++;
          substrCur++;
        }
        //std::cout << "NumInputIntervals: " << NumInputIntervals <<"\n";
        NumInputIntervals++;
        //std::cout << "NumInputIntervals: " << NumInputIntervals <<"\n";
        intervalId = 0;
        for(substrCur = 0; substrCur < substrEnd; substrCur++)
        {
          if(readRecord[substrCur] == ',')
          {
            GreenEnerAvail[rankId][intervalId] = ((long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))))*100;
            //std::cout << "GreenEnerAvail[" << rankId << "][" << intervalId << "]: " << GreenEnerAvail[rankId][intervalId] <<"\n";
            substrStart = substrCur+1;
            intervalId++;
          }
        }
        GreenEnerAvail[rankId][intervalId] = ((long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))))*100;
        //std::cout << "GreenEnerAvail[" << rankId << "][" << intervalId << "]: " << GreenEnerAvail[rankId][intervalId] <<"\n";
      }
    }
    else
    {
      treatInputDirectory = false;
      break;
    }

    std::cout << "END read file\n"; //GGG
    InputComplete = true;
    break;
  }

  if (char *EnvStr = std::getenv("OMPCLUSTER_HEFT_LOG"))
    HEFTLogPath = EnvStr;

  //TODO use information from Profile file
  char *HEFTProfilePath = std::getenv("OMPCLUSTER_HEFT_USE_PROFILE");
  std::ifstream ProfileFile{HEFTProfilePath};
  std::stringstream Buffer;

  if (ProfileFile) {
    Buffer << ProfileFile.rdbuf();
    Expected<json::Value> V = json::parse(Buffer.str());
    json::Object *ProfileInfoPtr = V ? V.get().getAsObject() : nullptr;
    if (ProfileInfoPtr) {
      ProfileInfo = *ProfileInfoPtr;
    } else {
      DP("[MOHEFTGreenEnergy] JSON profile `%s` is malformed, ignoring variable.\n",
         HEFTProfilePath);
    }
  } else {
    DP("[MOHEFTGreenEnergy] JSON profile `%s` not found, ignoring variable.\n",
       HEFTProfilePath);
  }
}

void MOHEFTGreenEnergyContext::computeUpwardRanks() {
  PROF_SCOPED(PROF_LVL_ALL, "MOHEFTGreenEnergyContext / Compute Upward Ranks");
  DP("[MOHEFTGreenEnergy] Computing upward rank for all tasks.\n");
  assert(TaskMap != nullptr && "The task map should not be NULL");

  int taskGraphId = 0;
  TaskGraphID.clear();
  for (auto *Task : ReverseTopologicalOrder(TaskMap)) {
    //TODO create task_graph_id
    //std::cout << "Task task_id: " << Task->task_id << "\n"; //GGG

    if (TaskGraphID.find(Task->task_id) != TaskGraphID.end())
      std::cout << "task_id " << Task->task_id << " is repeated \n";

    TaskGraphID[Task->task_id] = NumInputTasks-taskGraphId;
    TaskGraphID[Task->task_id]--;
    taskGraphId++;
    //std::cout << "Task graph_id: " << TaskGraphID[Task->task_id] << "\n"; //GGG
    long MaxSuccUrankComm = 0.0;
    for (auto &Succ : Successors(Task)) {
      MaxSuccUrankComm =
          std::max(MaxSuccUrankComm,
                   UpwardRank[&Succ] + computeAvgCommCost(*Task, Succ));
    }
    UpwardRank[Task] = computeAvgCompCost(*Task) + MaxSuccUrankComm;
  }
}

void MOHEFTGreenEnergyContext::scheduleTaskOnRank(TaskPtr Task, DeviceId Device, int32_t Tradeoff) {

  long MaxPredEST = 0;
  if(Tradeoff == 0)
  {
    bool IsRestartingTask = Schedules[Tradeoff][Task->task_id].state == TaskState::FAILED;
    Schedule[Task->task_id] = {
        .state = IsRestartingTask ? TaskState::FAILED : TaskState::SCHEDULED,
        .rank = Device.Rank,
        .thread = Device.Thread,
        .translated_rank = -1,
        .task = Task,
    };
    // Iterate over predecessors
    for (const auto &Pred : Predecessors(Task)) {
      // Ignore deferred tasks for now
      if (isDeferredTask(Pred)) //GGGMMM
        continue;
      DeviceId DevicePred(Schedule[Pred.task_id]);
      auto Candidate = AFT[Tradeoff][&Pred] + computeCommCost(Pred, DevicePred, *Task, Device);
      if (Candidate > MaxPredEST)
        MaxPredEST = Candidate;
      //ggg TotalCommEnergy[Tradeoff] += computeEnerCommCost(Pred, DevicePred, *Task, Device); //TODO add communication energy
    }
  }
  else
  {
    // Do not change state of FAILED tasks during the reschedule
    bool IsRestartingTask = Schedules[Tradeoff][Task->task_id].state == TaskState::FAILED;
    Schedules[Tradeoff][Task->task_id] = {
        .state = IsRestartingTask ? TaskState::FAILED : TaskState::SCHEDULED,
        .rank = Device.Rank,
        .thread = Device.Thread,
        .translated_rank = -1,
        .task = Task,
    };
    // Iterate over predecessors
    for (const auto &Pred : Predecessors(Task)) {
      // Ignore deferred tasks for now
      if (isDeferredTask(Pred)) //GGGMMM
        continue;
      DeviceId DevicePred(Schedules[Tradeoff][Pred.task_id]);
      auto Candidate = AFT[Tradeoff][&Pred] + computeCommCost(Pred, DevicePred, *Task, Device);
      if (Candidate > MaxPredEST)
        MaxPredEST = Candidate;
      //ggg TotalCommEnergy[Tradeoff] += computeEnerCommCost(Pred, DevicePred, *Task, Device); //TODO add communication energy
    }
  }

  long activeTime = computeCompCost(*Task, Device); //task duration
  long prevMakespan = Makespans[Tradeoff];
  long preIdleInterval = prevMakespan-DeviceAvail[Tradeoff][Device];

  long idleInterval = MaxPredEST-DeviceAvail[Tradeoff][Device]; //how long device was idle between tasks

  //subtract prev idle consumption calculated from prev Makespan and idle start
  if(preIdleInterval > 0)
  {
    // find first interval index where device started being idle
    int intervalIndex = DeviceAvail[Tradeoff][Device] / 60000; //miliseconds to minutes
    long nextIntervalStart = ((intervalIndex+1)*60000);
    // subtract proportionally from first interval
    if(prevMakespan < nextIntervalStart)
    {
      long intervalConsumption = prevMakespan - DeviceAvail[Tradeoff][Device];
      intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
      IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] -= intervalConsumption;
      //std::cout << "IDC 1 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
    }
    else
    {
      long intervalConsumption = nextIntervalStart - DeviceAvail[Tradeoff][Device];
      intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
      IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] -= intervalConsumption;
      //std::cout << "IDC 2 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG

      intervalIndex++;
      //std::cout << "Idle 1 " << Device.Rank << " " << IdleEner[Device.Rank] << " \n"; //GGG
      // subtract proportionally from intermediate intervals, if any
      while(intervalIndex*60000 < prevMakespan)
      {
        IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] -= IdleEner[Device.Rank];
        //std::cout << "IDC 3 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
        intervalIndex++;
      }
      // subtract proportionally from last interval
      intervalIndex--;
      intervalConsumption = prevMakespan - intervalIndex*60000;
      intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
      IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] -= intervalConsumption;
      //std::cout << "IDC 4 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
    }
  }
  
  AST[Tradeoff][Task] = std::max(DeviceAvail[Tradeoff][Device], MaxPredEST);
  //add new idle consumption
  if(idleInterval > 0)
  {
    // find first interval index where device started being idle
    int intervalIndex = DeviceAvail[Tradeoff][Device] / 60000; //miliseconds to minutes
    long nextIntervalStart = ((intervalIndex+1)*60000);
    // add proportionally from first interval
    if(AST[Tradeoff][Task] < nextIntervalStart)
    {
      long intervalConsumption = AST[Tradeoff][Task] - DeviceAvail[Tradeoff][Device];
      intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
      IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += intervalConsumption;
      //std::cout << "IDC 5 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
    }
    else
    {
      long intervalConsumption = nextIntervalStart - DeviceAvail[Tradeoff][Device];
      intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
      IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += intervalConsumption;
      //std::cout << "IDC 6 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG

      intervalIndex++;
      //std::cout << "Idle 2 " << Device.Rank << " " << IdleEner[Device.Rank] << " \n"; //GGG
      // add proportionally from intermediate intervals, if any
      while(intervalIndex*60000 < AST[Tradeoff][Task])
      {
        IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += IdleEner[Device.Rank];
        //std::cout << "IDC 7 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
        intervalIndex++;
      }
      // add proportionally from last interval
      intervalIndex--;
      intervalConsumption = AST[Tradeoff][Task] - intervalIndex*60000;
      intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
      IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += intervalConsumption;
      //std::cout << "IDC 8 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
    }
  }
  
  AFT[Tradeoff][Task] = AST[Tradeoff][Task] + activeTime;
  //add new exec consumption
  if(activeTime > 0)
  {
    // find first interval index where device started executing
    int intervalIndex = AST[Tradeoff][Task] / 60000; //miliseconds to minutes
    long nextIntervalStart = ((intervalIndex+1)*60000);
    // add proportionally from first interval
    if(AFT[Tradeoff][Task] < nextIntervalStart)
    {
      //std::cout << "IDC 9A " << AST[Tradeoff][Task] << " " << AFT[Tradeoff][Task] << " \n"; //GGG
      long intervalConsumption = AFT[Tradeoff][Task] - AST[Tradeoff][Task];
      //std::cout << "IDC 9B " << intervalConsumption << " \n"; //GGG
      //std::cout << "IDC 9BB " << ExecEner[Device.Rank] << " \n"; //GGG
      intervalConsumption = intervalConsumption * (ExecEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
      //std::cout << "IDC 9C " << intervalConsumption << " \n"; //GGG
      IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += intervalConsumption;
      //std::cout << "IDC 9 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
    }
    else
    {
      long intervalConsumption = nextIntervalStart - AST[Tradeoff][Task];
      intervalConsumption = intervalConsumption * (ExecEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
      IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += intervalConsumption;
      //std::cout << "IDC 10 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG

      intervalIndex++;
      //std::cout << "Idle 3 " << Device.Rank << " " << IdleEner[Device.Rank] << " \n"; //GGG
      // add proportionally from intermediate intervals, if any
      while(intervalIndex*60000 < AFT[Tradeoff][Task])
      {
        IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += ExecEner[Device.Rank];
        //std::cout << "IDC 11 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
        intervalIndex++;
      }
      // add proportionally from last interval
      intervalIndex--;
      intervalConsumption = AFT[Tradeoff][Task] - intervalIndex*60000;
      intervalConsumption = intervalConsumption * (ExecEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
      IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += intervalConsumption;
      //std::cout << "IDC 12 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
    }
  }

  DeviceAvail[Tradeoff][Device] = AFT[Tradeoff][Task];
  //ggg add new idle consumption if not new makespan
  if(AFT[Tradeoff][Task] < prevMakespan)
  {
    // find first interval index where device started being idle
    int intervalIndex = DeviceAvail[Tradeoff][Device] / 60000; //miliseconds to minutes
    long nextIntervalStart = ((intervalIndex+1)*60000);
    // add proportionally from first interval
    if(prevMakespan < nextIntervalStart)
    {
      long intervalConsumption = prevMakespan - DeviceAvail[Tradeoff][Device];
      intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
      IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += intervalConsumption;
      //std::cout << "IDC 13 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
    }
    else
    {
      long intervalConsumption = nextIntervalStart - DeviceAvail[Tradeoff][Device];
      intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
      IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += intervalConsumption;
      //std::cout << "IDC 14 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG

      intervalIndex++;
      //std::cout << "Idle 4 " << Device.Rank << " " << IdleEner[Device.Rank] << " \n"; //GGG
      // add proportionally from intermediate intervals, if any
      while(intervalIndex*60000 < prevMakespan)
      {
        IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += IdleEner[Device.Rank];
        //std::cout << "IDC 15 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
        intervalIndex++;
      }
      // add proportionally from last interval
      intervalIndex--;
      intervalConsumption = prevMakespan - intervalIndex*60000;
      intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
      IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += intervalConsumption;
      //std::cout << "IDC 16 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
    }
  }
  else 
  {
    //std::cout << "else here \n"; //GGG
    Makespans[Tradeoff] = AFT[Tradeoff][Task];
    for (int K = 0; K < NumInputRanks; K++)
    {
      if(K != Device.Rank)
      {
        // find first interval index where device started being idle
        int intervalIndex = DeviceAvail[Tradeoff][Device] / 60000; //miliseconds to minutes
        long nextIntervalStart = ((intervalIndex+1)*60000);
        // subtract proportionally from first interval
        if(prevMakespan < nextIntervalStart)
        {
          long intervalConsumption = prevMakespan - DeviceAvail[Tradeoff][Device];
          intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
          IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] -= intervalConsumption;
        //std::cout << "IDC 17 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
        }
        else
        {
          long intervalConsumption = nextIntervalStart - DeviceAvail[Tradeoff][Device];
          intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
          IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] -= intervalConsumption;
          //std::cout << "IDC 18 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG

          intervalIndex++;
          // subtract proportionally from intermediate intervals, if any
          //std::cout << "Idle 5 " << Device.Rank << " " << IdleEner[Device.Rank] << " \n"; //GGG
          while(intervalIndex*60000 < prevMakespan)
          {
            IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] -= IdleEner[Device.Rank];
            //std::cout << "IDC 19 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
            intervalIndex++;
          }
          // subtract proportionally from last interval
          intervalIndex--;
          intervalConsumption = prevMakespan - intervalIndex*60000;
          intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
          IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] -= intervalConsumption;
          //std::cout << "IDC 20 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
        }

        // find first interval index where device started being idle
        intervalIndex = DeviceAvail[Tradeoff][Device] / 60000; //miliseconds to minutes
        nextIntervalStart = ((intervalIndex+1)*60000);
        // add proportionally from first interval
        if(Makespans[Tradeoff] < nextIntervalStart)
        {
          long intervalConsumption = Makespans[Tradeoff] - DeviceAvail[Tradeoff][Device];
          intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
          IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += intervalConsumption;
          //std::cout << "IDC 21 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
        }
        else
        {
          long intervalConsumption = nextIntervalStart - DeviceAvail[Tradeoff][Device];
          intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
          IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += intervalConsumption;
          //std::cout << "IDC 22 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG

          intervalIndex++;
          // add proportionally from intermediate intervals, if any
          //std::cout << "Idle 6 " << Device.Rank << " " << IdleEner[Device.Rank] << " \n"; //GGG
          while(intervalIndex*60000 < Makespans[Tradeoff])
          {
            IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += IdleEner[Device.Rank];
            //std::cout << "IDC 23 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
            intervalIndex++;
          }
          // add proportionally from last interval
          intervalIndex--;
          intervalConsumption = Makespans[Tradeoff] - intervalIndex*60000;
          intervalConsumption = intervalConsumption * (IdleEner[Device.Rank]/60000); // miliseconds * (minute to miliseconds);
          IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] += intervalConsumption;
          //std::cout << "IDC 24 " << IntervalDeviceConsumption[Tradeoff][Device.Rank][intervalIndex] << " \n"; //GGG
        }
      }
    }
  }

  TotalBrownConsumptions[Tradeoff] = 0;
  TotalConsumptions[Tradeoff] = 0; // GUI for experiment only
  for(int intervalIndex = 0; intervalIndex*60000 < Makespans[Tradeoff]; intervalIndex++)
  {
    for(int deviceIndex = 0; deviceIndex < NumInputDevices; deviceIndex++)
    {
      long brownConsumed = IntervalDeviceConsumption[Tradeoff][deviceIndex][intervalIndex];
      if(intervalIndex < NumInputIntervals)
      {
        brownConsumed -= GreenEnerAvail[deviceIndex][intervalIndex];
      }
      //std::cout << "IDC 25 " << IntervalDeviceConsumption[Tradeoff][deviceIndex][intervalIndex] << " \n"; //GGG
      //std::cout << "Tradeoff " << Tradeoff  << " device " << deviceIndex << " intervalIndex " << intervalIndex << " brownConsumed " << brownConsumed << "\n"; //GGG
      if(brownConsumed > 0)
      {
        TotalBrownConsumptions[Tradeoff] += brownConsumed;
      }
      TotalConsumptions[Tradeoff] += IntervalDeviceConsumption[Tradeoff][deviceIndex][intervalIndex]; // GUI for experiment only
    }
  }
  //std::cout << "TotalBrownConsumptions Tradeoff " << Tradeoff << " " << TotalBrownConsumptions[Tradeoff] << "\n"; //GGG
}

void MOHEFTGreenEnergyContext::deferTask(TaskPtr Task) {
  // Do not change state of FAILED tasks during the reschedule
  if (Schedule[Task->task_id].state != TaskState::FAILED)
    Schedule[Task->task_id].state = TaskState::DEFERRED;
}

PriorityQueue MOHEFTGreenEnergyContext::buildQueue() const {
  PROF_SCOPED(PROF_LVL_ALL, "MOHEFTGreenEnergyContext / Build Queue");

  PriorityQueue Pqueue{UpwardRank.begin(), UpwardRank.end()};

  std::sort(Pqueue.begin(), Pqueue.end(),
            [](const auto &L, const auto &R) { return L.second > R.second; });

  return Pqueue;
}

std::vector<DeviceId> MOHEFTGreenEnergyContext::buildDeviceList(int32_t NumRanks) const {
  std::vector<DeviceId> Devices;
  Devices.reserve(NumRanks);
  for (int32_t R = 1; R <= NumRanks; R++) //TODO review if rank 0 may have more threads
    //for (int32_t H = 0; H < NumThreads[R]; H++) //GGG solve this fast!
    for (int32_t H = 0; H < 1; H++)
      Devices.emplace_back(R, H);
  return Devices;
}

void MOHEFTGreenEnergyContext::clear() {
  PROF_SCOPED(PROF_LVL_ALL, "MOHEFTGreenEnergyContext / Clear");
  /*
  CommCosts.clear();
  CompCosts.clear();
  ExecEner.clear();
  IdleEner.clear();
  GreenEnerAvail.clear();
  EnerCommCosts.clear();
  EnerCompCosts.clear();
  */

  Schedules.clear();
  AST.clear();
  AFT.clear();
  DeviceAvail.clear();
  
  Makespans.clear();
  TotalBrownConsumptions.clear();
  TotalConsumptions.clear(); // GUI for experiment only
  IntervalDeviceConsumption.clear();

  UpwardRank.clear();
  CommunicationCost.clear();
}

void MOHEFTGreenEnergyContext::setTaskMap(kmp_target_task_map_t *NewTaskMap) {
  assert(NewTaskMap != nullptr && "Expected a non-null task map!");
  TaskMap = NewTaskMap;
}

long MOHEFTGreenEnergyContext::computeCommCost(const kmp_target_task_t &A,
                                    DeviceId DeviceA,
                                    const kmp_target_task_t &B,
                                    DeviceId DeviceB) {
  // If the tasks are to execute on the same rank, we do not pay for the
  // communication because no data needs to be moved.
  if (DeviceA.Rank == DeviceB.Rank)
    return 0.0;

  return CommCosts[DeviceA.Rank][DeviceB.Rank] * CommunicationCost[&A][&B];
}

long MOHEFTGreenEnergyContext::computeEnerCommCost(const kmp_target_task_t &A,
                                    DeviceId DeviceA,
                                    const kmp_target_task_t &B,
                                    DeviceId DeviceB) {
  // If the tasks are to execute on the same rank, we do not pay for the
  // communication because no data needs to be moved.
  if (DeviceA.Rank == DeviceB.Rank)
    return 0.0;

  return EnerCommCosts[DeviceA.Rank][DeviceB.Rank] * CommunicationCost[&A][&B];
}

/// Search for all `out` dependencies of A which are also `in` dependencies of B
/// and accumulate the size of those dependencies (in bytes).
long MOHEFTGreenEnergyContext::computeAvgCommCost(const kmp_target_task_t &A,
                                       const kmp_target_task_t &B) {
  long TotalBytes = 0.0;

  // Look for arguments which are common between tasks A and B and accumulate
  // their sizes in bytes.
  for (int32_t I = 0; I < A.target_data.num_args; I++) {
    for (int32_t J = 0; J < B.target_data.num_args; J++) {
      if (A.target_data.args[I] == B.target_data.args[J]) {
        TotalBytes += A.target_data.arg_sizes[I];
      }
    }
  }

  CommunicationCost[&A][&B] = TotalBytes;

  DP("[MOHEFTGreenEnergy] Dependency `%d -> %d` has average cost: %lf KB\n", A.task_id,
     B.task_id, TotalBytes / 1024.0);

  return AvgCommCost * TotalBytes;
}

long MOHEFTGreenEnergyContext::computeAvgCompCost(const kmp_target_task_t &Task) {
  if (isTargetDataTask(Task))
    return 1.0;

  long AvgCommCost = 0;
  for (int i = 0; i < NumInputRanks; i++) {
    AvgCommCost += CompCosts[CurrentGraph][i][TaskGraphID[Task.task_id]]; //TODO update with task_graph_id
  }
  AvgCommCost /= NumInputRanks;
  return AvgCommCost;
}

long MOHEFTGreenEnergyContext::computeCompCost(const kmp_target_task_t &Task,
                                    DeviceId Device) {
  return CompCosts[CurrentGraph][Device.Rank][TaskGraphID[Task.task_id]]; //TODO update with task_graph_id
}

void MOHEFTGreenEnergyContext::copySchedule(int Destination, int Origin)
{
  Schedules[Destination].clear();
  for (const auto &Task : Schedules[Origin]) {
    Schedules[Destination][Task.first] = Task.second;
  }

  AST[Destination].clear();
  for (const auto &Task : AST[Origin] ) {
    AST[Destination][Task.first] = Task.second;
  }

  AFT[Destination].clear();
  for (const auto &Task : AFT[Origin] ) {
    AFT[Destination][Task.first] = Task.second;
  }

  DeviceAvail[Destination].clear();
  for (const auto &Device : DeviceAvail[Origin] ) {
    DeviceAvail[Destination][Device.first] = Device.second;
  }
  
  Makespans[Destination] = Makespans[Origin];
  
  //std::cout << "TotalBrownConsumptions " << Destination << " " << Origin << " " << TotalBrownConsumptions[Origin] << "\n"; //GGG
  TotalBrownConsumptions[Destination] = TotalBrownConsumptions[Origin];

  TotalConsumptions[Destination] = TotalConsumptions[Origin]; // GUI for experiment only

  for (int deviceId = 0; deviceId < NumInputDevices; deviceId++) {
    for(int intervalIndex = 0; intervalIndex*60000 < Makespans[Destination]; intervalIndex++)
    {
      IntervalDeviceConsumption[Destination][deviceId][intervalIndex] = IntervalDeviceConsumption[Origin][deviceId][intervalIndex];
    }
  }
}

int gcompare1 (const void * a, const void * b)
{
  const long *pa = *(const long **)a;
  const long *pb = *(const long **)b;
  return ( pa[1] - pb[1] );
}

int gcompare2 (const void * a, const void * b)
{
  const long *pa = *(const long **)a;
  const long *pb = *(const long **)b;
  return ( pb[2] - pa[2] );
}

int gcompareBoth (const void * a, const void * b)
{
  const long *pa = *(const long **)a;
  const long *pb = *(const long **)b;
  if (pa[1] != pb[1])
    return ( pa[1] - pb[1] );
  else
    return ( pa[2] - pb[2] );
}

long** MOHEFTGreenEnergyContext::sortCrowdingDistances(long **Objectives, int CurrNumSchedules)
{
  //std::cout << "sortCrowdingDistances( 1 " << CurrNumSchedules << "\n"; //GGG
  long** dist = new long*[CurrNumSchedules];
  for (int32_t I = 0; I < CurrNumSchedules; I++)
  {
    dist[I] = new long[3];
    dist[I][0] = I;
    dist[I][1] = Objectives[I][1];
    dist[I][2] = 0;
  }

  qsort(dist,CurrNumSchedules,sizeof(dist[0]),gcompare1);
  dist[0][2] = INT_MAX;
  dist[CurrNumSchedules-1][2] = INT_MAX;
  for(int32_t I = 1; I < CurrNumSchedules-2; I++)
  {
    dist[I][2] = (Objectives[(int)(dist[I+1][0])][1] - Objectives[(int)(dist[I-1][0])][1])
                /(Objectives[(int)(dist[CurrNumSchedules-1][0])][1] - Objectives[(int)(dist[0][0])][1]);
  }

  for (int32_t I = 0; I < CurrNumSchedules; I++)
  {
    dist[I][1] = Objectives[(int)(dist[I][0])][2];
  }
  qsort(dist,CurrNumSchedules,sizeof(dist[0]),gcompare1);
  dist[0][2] = INT_MAX;
  dist[CurrNumSchedules-1][2] = INT_MAX;
  for(int32_t I = 1; I < CurrNumSchedules-2; I++)
  {
    if(dist[I][2] != INT_MAX){
      dist[I][2] += (Objectives[(int)(dist[I+1][0])][1] - Objectives[(int)(dist[I-1][0])][1])
                   /(Objectives[(int)(dist[CurrNumSchedules-1][0])][1] - Objectives[(int)(dist[0][0])][1]);
    }
  }

  qsort(dist,CurrNumSchedules,sizeof(dist[0]),gcompare2);
  return dist;
}

int MOHEFTGreenEnergyContext::removeDominated(long **Objectives, int CurrNumSchedules)
{
  qsort(Objectives,CurrNumSchedules,sizeof(Objectives[0]),gcompareBoth);
  int removeCurr = 0;
  int I = 1;
  while(I < CurrNumSchedules)
  {
    int J = 0;
    while(J < I)
    {
      if(Objectives[J][1] <= Objectives[I][1] && 
        Objectives[J][2] <= Objectives[I][2])
      {
        Objectives[I][1] = INT_MAX;
        removeCurr++;
        break;
      }
      else if(
        Objectives[J][1] != INT_MAX &&
        Objectives[I][1] <= Objectives[J][1] && 
        Objectives[I][2] <= Objectives[J][2])
      {
        Objectives[J][1] = INT_MAX;
        removeCurr++;
        break;
      }
      J++;
    }
    I++;
  }
  qsort(Objectives,CurrNumSchedules,sizeof(Objectives[0]),gcompareBoth);

  return CurrNumSchedules-removeCurr;
}

void MOHEFTGreenEnergyContext::selectFirstSchedules(long **CrowdingDistanceSet, long **Objectives, int NumSchedules)
{
  long **TempObjectives = new long*[NumSchedules];
  for (int I = 0; I < NumSchedules; I++) {
      int LocalI = (int)(CrowdingDistanceSet[I][0]);
      TempObjectives[I] = new long[4];  // GUI for experiment only long[3];
      TempObjectives[I][0] = Objectives[LocalI][0];
      TempObjectives[I][1] = Objectives[LocalI][1];
      TempObjectives[I][2] = Objectives[LocalI][2];
      TempObjectives[I][3] = Objectives[LocalI][3]; // GUI for experiment only
  }

  qsort(TempObjectives,NumSchedules,sizeof(TempObjectives[0]),gcompareBoth);

  for (int I = 0; I < NumSchedules; I++) {
      Objectives[I][0] = TempObjectives[I][0];
      Objectives[I][1] = TempObjectives[I][1];
      Objectives[I][2] = TempObjectives[I][2];
      Objectives[I][3] = TempObjectives[I][3]; // GUI for experiment only
  }

  for (int I = 0; I < NumSchedules; I++)
  {
    copySchedule(I+1, TempObjectives[I][0]);
  }
}

void MOHEFTGreenEnergyContext::selectSchedule(int ScheduleIndex)
{
  copySchedule(0, ScheduleIndex);
  for (const auto &Task : Schedules[0])
    Schedule[Task.first] = Task.second;
}

/// Dump internal MOHEFTGreenEnergy information to a file.
void MOHEFTGreenEnergyContext::dump(int32_t NumDevices) {
  PROF_SCOPED(PROF_LVL_ALL, "MOHEFTGreenEnergyContext / Dump");

  if (HEFTLogPath.empty())
    return;

  FILE *fp = fopen(HEFTLogPath.c_str(), "w");

  if (fp == NULL) {
    DP("[MOHEFTGreenEnergy] Failed to open log file.\n");
    return;
  }

  auto Devices = buildDeviceList(NumDevices);

  DP("[MOHEFTGreenEnergy] Dumping MOHEFTGreenEnergy stats to file '%s'\n", HEFTLogPath.c_str());

  // FIXME: Print the header with correct columns for EST/EFT
  fprintf(fp,
          "%-4s  %-10s  %-10s  %-10s  %-10s  %-10s  %-7s  %-7s  %-7s  %-7s"
          "%-34s  %-34s\n",
          "ID", "FNID", "Rank", "AST", "AFT", "Dev", "Root", "Exit", "Data",
          "Classic", "EST", "EFT");

  std::vector<kmp_target_task_t *> TasksV;
  TasksV.reserve(TaskMap->num_tasks);
  for (int i = 0; i < TaskMap->num_tasks; i++) {
    TasksV.push_back(&(TaskMap->tasks[i]));
  }

  std::sort(TasksV.begin(), TasksV.end(),
            [&](kmp_target_task_t *A, kmp_target_task_t *B) {
              return UpwardRank[A] > UpwardRank[B];
            });

/* //TODO return this for MOHEFT
  // Iterate over tasks
  for (auto const *task_ptr : TasksV) {
    auto &task = *task_ptr;
    uint64_t ofid = (uint64_t)task.target_data.outlined_fn_id;
    fprintf(fp,
            "%04d  0x%08lx  %10.2lf  %10.2lf  %10.2lf  %7d  %7d  %7d  %7d  %4d",
            task.task_id, ofid, UpwardRank[&task], AST[0][&task], AFT[0][&task],
            Schedule[task.task_id].rank, isRootTask(task), isExitTask(task),
            isTargetDataTask(task), isClassicalTask(task));
    for (const auto &Device : Devices)
      fprintf(fp, "  %10.2lf", EST[&task][Device]);
    for (const auto &Device : Devices)
      fprintf(fp, "  %10.2lf", EFT[&task][Device]);
    fprintf(fp, "\n");
  }
*/
  fprintf(fp, "\n");
  fprintf(fp, "Dependencies\n");
  fprintf(fp, "-------------------------------------------------------\n");

  for (const auto &A : CommunicationCost) {
    auto id_A = A.first->task_id;
    for (const auto &B : A.second) {
      auto id_B = B.first->task_id;
      fprintf(fp, "%04d -> %04d : %10.4ld\n", id_A, id_B, B.second);
    }
  }

  fprintf(fp, "\n");
  fprintf(fp, "Schedule Map\n");
  fprintf(fp, "-------------------------------------------------------\n");

  for (const auto &s : Schedule) {
    fprintf(fp, "%04d: %04d %d %d\n", s.first, s.second.task->task_id,
            s.second.rank, s.second.state);
  }

  fclose(fp);
}

} // namespace detail

// Implementation of MOHEFTGreenEnergyScheduler
// =============================================================================

MOHEFTGreenEnergyScheduler::DumpOptions::DumpOptions() {
  if (char *Value = std::getenv("OMPCLUSTER_HEFT_DUMP_EDGE_LABEL"))
    EdgeLabel = atoi(Value) > 0;
  if (char *Value = std::getenv("OMPCLUSTER_HEFT_DUMP_PRED"))
    Predecessors = atoi(Value) > 0;
  if (char *Value = std::getenv("OMPCLUSTER_HEFT_DUMP_FAKE_ENTRY"))
    FakeEntryNode = atoi(Value) > 0;
  if (char *Value = std::getenv("OMPCLUSTER_HEFT_DUMP_FAKE_EXIT"))
    FakeExitNode = atoi(Value) > 0;
}

/// This is the high-level implementation of the HEFT algorithm as is described
/// in the original paper.
///
/// \see https://ieeexplore.ieee.org/document/993206
void MOHEFTGreenEnergyScheduler::scheduleGraph(const RTLInfoTy *Device) {
  PROF_SCOPED(PROF_LVL_ALL, "MOHEFTGreenEnergyScheduler / Schedule Graph");

  using detail::DeviceId;
  auto PQueue = Ctx.buildQueue();
  auto NumDevices = ft_handler.DevicesGetActiveNumber();
  auto Devices = Ctx.buildDeviceList(NumDevices);

  auto Objectives = Ctx.Objectives;
  //auto Makespans = Ctx.Makespans;
  //auto TotalBrownConsumptions = Ctx.TotalBrownConsumptions;
  auto IntervalDeviceConsumption = Ctx.IntervalDeviceConsumption;
  auto NumSchedules = Ctx.NumSchedules;
  auto NumInputIntervals = Ctx.NumInputIntervals;
  auto ScheduleSelectionIndex = Ctx.ScheduleSelectionIndex;
  auto ScheduleSelectionTotal = Ctx.ScheduleSelectionTotal;
  auto ScheduleSelectionRatio = Ctx.ScheduleSelectionRatio;
  auto ScheduleSelectionRate = Ctx.ScheduleSelectionRate;
  auto SelectScheduleIndex = Ctx.SelectScheduleIndex;
  auto SelectScheduleTotal = Ctx.SelectScheduleTotal;
  auto SelectScheduleRatio = Ctx.SelectScheduleRatio;
  auto SelectScheduleRate = Ctx.SelectScheduleRate;
  int CurrNumSchedules = 1;

  Objectives = new long*[NumSchedules*NumDevices]; 
  for (int i = 0; i < NumSchedules*NumDevices; i++) {
      Objectives[i] = new long[4]; // GUI for experiment only new long[3];
  }

  DP("[SCHEDULER] Scheduling graph with MOHEFTGreenEnergy algorithm:\n"
     "[SCHEDULER] --> Number of tasks: %d\n"
     "[SCHEDULER] --> Number of ranks: %d\n",
     task_map->num_tasks, NumDevices);

  while (not PQueue.empty()) {
    const auto *Task = PQueue.front().first;
    PQueue.pop_front();
    /*
    for(int i = 1; i <= CurrNumSchedules; i++) {//GGG
      for (const auto &Task : Ctx.Schedules[i]) { //GGG
        std::cout << "Schedules[" << i << "]: " << Task.first << " " << Task.second.task->task_id << "," << Task.second.rank << "\n"; //GGG
      } //GGG
    }//GGG
    */
    PROF_SCOPED(PROF_LVL_ALL, "MOHEFTGreenEnergyScheduler / Schedule Task",
                [&]() -> std::string {
                  std::stringstream ss;
                  ss << "{";
                  ss << "\"task_id\":" << Task->task_id << ",";
                  ss << "\"remaining_tasks\":" << PQueue.size();
                  ss << "}";
                  return ss.str();
                }());

    // Do not reschedule already executed or saved tasks
    const auto Data = schedule_map[Task->task_id];
    if (Data.state > TaskState::SCHEDULED && Data.state != TaskState::FAILED)
      continue;

    // Defer the scheduling for target data tasks
    if (isTargetDataTask(Task)) {
      Ctx.deferTask(Task);
      continue;
    }

    // Classical tasks are pinned in the head node (rank=0)
    if (isClassicalTask(Task)) {
      Ctx.scheduleTaskOnRank(Task, DeviceId::head(), 0);
      continue;
    }

    //GGG here it will change

    // Iterate over predecessors
    for (const auto &Pred : Predecessors(Task)) {
      // Ignore deferred tasks for now
      if (Ctx.isDeferredTask(Pred)) //GGGMMM
        continue;
      assert(Ctx.Schedules[1].count(Pred.task_id) == 1 &&
            "Predecessor should already be scheduled!");
    }

    //std::cout << "NumSchedules: " << NumSchedules << "\n"; //GGG
    int TradeoffsCount = 0;
    //std::cout << "TradeoffsCount: " << TradeoffsCount << "\n"; //GGG
    for (int I = 0; I < NumDevices; I++)
    {
      const auto &Device = Devices[I]; //TODO considering that Device 1 starts at rank 0 //TODO consider multiple threads

      for (int J = 1; J <= CurrNumSchedules; J++)
      {
        // below: 1 (index 0 that is final schedule) + CurrNumSchedules + CurrNumSchedules*I + J (current schedule for current device) - 1 (J starts at 1)
        // then remove +1 and -1 \/
        int CurrentScheduleIndex = NumSchedules + CurrNumSchedules*I + J;
        Ctx.copySchedule(CurrentScheduleIndex,J);
        //std::cout << "Objective disp pre sched " << CurrentScheduleIndex << " " << Ctx.Makespans[CurrentScheduleIndex] << " " << Ctx.TotalBrownConsumptions[CurrentScheduleIndex] << "\n"; //GGG
        Ctx.scheduleTaskOnRank(Task, Device, CurrentScheduleIndex);
        //std::cout << "Objective disp " << CurrentScheduleIndex << " " << Ctx.Makespans[CurrentScheduleIndex] << " " << Ctx.TotalBrownConsumptions[CurrentScheduleIndex] << "\n"; //GGG
        Objectives[TradeoffsCount][0] = CurrentScheduleIndex;
        Objectives[TradeoffsCount][1] = Ctx.Makespans[CurrentScheduleIndex];
        Objectives[TradeoffsCount][2] = Ctx.TotalBrownConsumptions[CurrentScheduleIndex];
        Objectives[TradeoffsCount][3] = Ctx.TotalConsumptions[CurrentScheduleIndex]; // GUI for experiment only
        //std::cout << "Objectives[" << TradeoffsCount << "]: " << Objectives[TradeoffsCount][1]/1000 << "." << Objectives[TradeoffsCount][1]%1000 << " " << Objectives[TradeoffsCount][2]/100<< "." << Objectives[TradeoffsCount][2]%100 << "\n"; // GUI
        TradeoffsCount++;
        //std::cout << "TradeoffsCount: " << TradeoffsCount << "\n"; //GGG
      }
    }
    
    //std::cout << "TradeoffsCount: " << TradeoffsCount << "\n"; //GGG
    for (int i = 0; i < TradeoffsCount; i++) { //GGG
      for (int j = 0; j < 3; j++) { //GGG
        //std::cout << "Pre sort Objectives[" << i << "]["<< j <<"]: " << Objectives[i][j] << "\n"; //GGG
      } //GGG
    } //GGG
    
    TradeoffsCount = Ctx.removeDominated(Objectives,TradeoffsCount);

    //std::cout << "After removal TradeoffsCount: " << TradeoffsCount << "\n"; //GGG
    for (int i = 0; i < TradeoffsCount; i++) { //GGG
      for (int j = 0; j < 3; j++) { //GGG
        //std::cout << "Pos sort and removal Objectives[" << i << "]["<< j <<"]: " << Objectives[i][j] << "\n"; //GGG
      } //GGG
    } //GGG
    
    //std::cout << "Pre sortCrowdingDistances " << TradeoffsCount << "\n"; //GGG
    long** CrowdingDistanceSet = Ctx.sortCrowdingDistances(Objectives,TradeoffsCount);
    //std::cout << "Pos sortCrowdingDistances \n"; //GGG

    CurrNumSchedules = std::min(TradeoffsCount,NumSchedules);
    //std::cout << "CurrNumSchedules: " << CurrNumSchedules << "\n"; //GGG

    for (int32_t I = 0; I < CurrNumSchedules; I++) //GGG
    {
      //std::cout << "CrowdingDistanceSet[" << I << "]: " << CrowdingDistanceSet[I][0] << "," << CrowdingDistanceSet[I][1] << "," << CrowdingDistanceSet[I][2] << "\n"; //GGG
    } //GGG
    //Ctx.selectFirstSchedules(Objectives,CurrNumSchedules); // copySchedule(1,2)
    Ctx.selectFirstSchedules(CrowdingDistanceSet,Objectives,CurrNumSchedules); // copySchedule(1,2)
    //std::cout << "selectFirstSchedules done\n"; //GGG
    /*
    for(int i = 1; i <= CurrNumSchedules; i++) {//GGG
      for (const auto &Task : Ctx.Schedules[i]) { //GGG
        std::cout << "aft Schedules[" << i << "]: " << Task.first << " " << Task.second.task->task_id << "," << Task.second.rank << "\n"; //GGG
      } //GGG
    }//GGG
    */
  }

  for(int i = 0; i < CurrNumSchedules; i++) {//GGG
    std::cout << "Objectives[" << i << "]: " << Objectives[i][1]/1000 << "." << Objectives[i][1]%1000 << " " << Objectives[i][2]/100<< "." << Objectives[i][2]%100 << " " << Objectives[i][3]/100<< "." << Objectives[i][3]%100 << "\n"; // GUI
    for (const auto &Task : Ctx.Schedules[i+1]) { //GGG
      //std::cout << "final Schedules[" << i << "]: " << Task.first << " " << Task.second.task->task_id << "," << Task.second.rank << "\n"; //GGG
    } //GGG
  }//GGG

  //std::cout << "CurrNumSchedules: " << CurrNumSchedules << "\n"; //GGG
  
  if(ScheduleSelectionIndex)
  {
    //std::cout << "ScheduleSelectionIndex\n"; //GGG
    Ctx.selectSchedule(std::min(SelectScheduleIndex,CurrNumSchedules));
  }
  else 
  {
    int selected = 0;
    if(ScheduleSelectionTotal)
    {
      while(selected < CurrNumSchedules)
      {
        if(Objectives[selected][1] > (Objectives[0][1]+SelectScheduleTotal))
          break;
        selected++;
      }
      selected--;
    }
    if(ScheduleSelectionRatio)
    {
      int i = 0;
      while(i < CurrNumSchedules)
      {
        if(Objectives[i][1] > (Objectives[0][1]+(Objectives[0][1]*SelectScheduleRatio/100)))
          break;
        i++;
      }
      i--;
      if (i < selected)
        selected = i;
    }
    if(ScheduleSelectionRate)
    {
      int i = 1;
      while(i < CurrNumSchedules)
      {
        if((Objectives[i][1] - Objectives[0][1])/(Objectives[0][2] - Objectives[i][2]) < SelectScheduleRate)
          break;
        i++;
      }
      i--;
      if (i < selected)
        selected = i;
    }
    //std::cout << "i: " << i << " , " << Objectives[i][0] <<"\n"; //GGG
    Ctx.selectSchedule(selected+1);
  }

  // Schedule deferred tasks
  for (auto &Entry : schedule_map) {
    auto &Data = Entry.second;
    const auto *Task = Data.task;

    // Do not reschedule already executed or saved tasks
    if (Data.state > TaskState::SCHEDULED && Data.state != TaskState::FAILED)
      continue;

    // These are already scheduled.
    if (not Ctx.isDeferredTask(*Task))
      continue;

    DP("[MOHEFTGreenEnergy] Scheduling deferred task %d: targetdata=%d root=%d exit=%d "
       "nsuccs=%d npreds=%d\n",
       Task->task_id, isTargetDataTask(Task), isRootTask(Task),
       isExitTask(Task), NumSuccessors(Task), NumPredecessors(Task));

    long MaxCost = 0.0;
    int32_t Rank = 1;

    // Find the successor with highest communication cost
    // NOTE: Exit target data tasks are temporary scheduled but the rank will
    // be overwritten later with PinTask function.
    for (const auto &Succ : Successors(Task)) {
      auto SuccRank = schedule_map[Succ.task_id].rank;

      // Save successor ranks of each scheduled data task to detect broadcast
      // opportunites
      dataTaskSuccessorRanks[Task->task_id].insert(SuccRank);

      if (Ctx.CommunicationCost[Task][&Succ] > MaxCost &&
          !isClassicalTask(Succ) && SuccRank != -1) {
        MaxCost = Ctx.CommunicationCost[Task][&Succ];
        Rank = SuccRank;
      }
    }

    // Schedule this task to the same rank as the successor chosen before
    Ctx.scheduleTaskOnRank(Task, DeviceId(Rank), 0);
  }

  auto Predicate = [](const auto &Entry) {
    if (Entry.second.state >= TaskState::DISPATCHED)
      return true; // skip checking for executed/saved tasks
    return Entry.second.state == TaskState::SCHEDULED &&
                   isClassicalTask(Entry.second.task)
               ? Entry.second.rank == 0
               : Entry.second.rank > 0;
  };

  // Tasks can be DISPATCHED, SCHEDULED, EXECUTED or FAILED (because of
  // reschedule)

  assert(std::all_of(schedule_map.begin(), schedule_map.end(), Predicate) &&
         "Some tasks were not properly scheduled.");

  Ctx.dump(NumDevices);
  DP("[SCHEDULER] MOHEFTGreenEnergy scheduled %d tasks to %d resources.\n",
     task_map->num_tasks, NumDevices);
}

/// Reset context and recompute upward ranks for the new graph
void MOHEFTGreenEnergyScheduler::acquireNewGraphPost() {
  PROF_SCOPED(PROF_LVL_ALL, "MOHEFTGreenEnergyScheduler / Acquire New Graph Post");

  Ctx.clear();
  Ctx.setTaskMap(task_map);
  Ctx.CurrentGraph++;

  std::string env_var_string = "OMPCLUSTER_SCHEDULER_INPUT_DIRECTORY"; // Environment variable containing path to input files
  std::string directoryString; // Path to input files directory
  std::string readRecord; // Current record read from current file
  
  // Check if current directory path is present
  const char* str = env_var_string.c_str();
  if (char *EnvStr = std::getenv(str))
  {
    //treatInputDirectory = true;
    directoryString = EnvStr;
  }

  //std::cout << "Read FILE: OMPCLUSTER_HEFT_TASKS_GRAPH_" << std::to_string(Ctx.CurrentGraph) <<" _ \n";
  std::ifstream readFileX(directoryString+"/OMPCLUSTER_HEFT_TASKS_GRAPH_"+std::to_string(Ctx.CurrentGraph));
    getline (readFileX, readRecord);
    //std::cout << "Read record A: " << readRecord <<"\n";
    /*
    if(readRecord != "OMPCLUSTER_HEFT_TASKS_GRAPH_"+std::to_string(CurrentGraph))
    {
      treatInputDirectory = false;
      break;
    }
    */
    getline (readFileX, readRecord);
    //std::cout << "Read record B: " << readRecord <<"\n";
    if(readRecord == "OMPCLUSTER_HEFT_COMP_G_x_P_y_T_z")
    {
      //std::cout << "pos CompCosts " << NumInputRanks << "\n"; //GGG
      int taskId = 0;
      for (int rankId = 0; rankId < Ctx.NumInputRanks; rankId++)
      {
        getline (readFileX, readRecord);
        //std::cout << "Read record 8: " << readRecord <<"\n";
        taskId = 0;
        int substrStart = 0;
        int substrCur = 0;
        int substrEnd = readRecord.length();
        Ctx.NumInputTasks = 0;
        while(substrCur < substrEnd){
          if(readRecord[substrCur] == ',')
            Ctx.NumInputTasks++;
          substrCur++;
        }
        //std::cout << "Ctx.CurrentGraph: " << Ctx.CurrentGraph <<"\n";
        Ctx.NumInputTasks++;
        for(substrCur = 0; substrCur < substrEnd; substrCur++)
        {
          if(readRecord[substrCur] == ',')
          {
            Ctx.CompCosts[Ctx.CurrentGraph][rankId][taskId] = (long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))*1000);
            //std::cout << "CompCosts[" << rankId << "][" << taskId << "]: " << CompCosts[rankId][taskId] <<"\n";
            substrStart = substrCur+1;
            taskId++;
          }
        }
        Ctx.CompCosts[Ctx.CurrentGraph][rankId][taskId] = (long)(std::stod(readRecord.substr(substrStart, substrCur-substrStart))*1000);
        //std::cout << "CompCosts[" << rankId << "][" << taskId << "]: " << CompCosts[rankId][taskId] <<"\n";
      }
    }
    /*
    else
    {
      treatInputDirectory = false;
      break;
    }
    */
   
  Ctx.computeUpwardRanks();

  std::cout << "CurrentGraph: " << Ctx.CurrentGraph << "\n"; //GGG
}

void MOHEFTGreenEnergyScheduler::dumpTaskGraph() {
  PROF_SCOPED(PROF_LVL_ALL, "MOHEFTGreenEnergyScheduler / Dump Task Graph");

  if (graph_dump_dir_path.empty())
    return;

  if (task_map == nullptr) {
    DP("[MOHEFTGreenEnergy] Task map is null, skipping dump.\n");
    return;
  }

  std::ofstream dot(graph_dump_dir_path + "_graph_" +
                    std::to_string(graph_counter) + ".dot");
  // Print header
  dot << "// Generated in MOHEFTGreenEnergyScheduler\n"
      << "digraph TargetTaskGraph {\n"
      << "  labelloc=\"t\";\n"
      << "  label=\"MOHEFTGreenEnergy Scheduler\";\n";

  // For each task
  for (const auto &task : Tasks(task_map)) {
    int32_t tid = task.task_id;
    int32_t trank = schedule_map[tid].rank;
    std::string tinfo = schedule_map[tid].source_location;
    int32_t state = static_cast<int32_t>(schedule_map[tid].state);

    // Print graph node
    dot << "  V" << tid << " [label=<<B>ID " << tid
        << "</B><BR /><FONT POINT-SIZE=\"10\">Rank " << trank
        << "<BR /> State: " << state;

    if (!tinfo.empty()) {
      dot << "<BR />" << tinfo;
    }

    dot << "</FONT>>";

    if (isTargetDataTask(task))
      dot << " color=gray70 fontcolor=gray50";
    if (isRootTask(task) || isExitTask(task))
      dot << " shape=octagon";

    dot << "];\n";

    // Print forward arrows using the list of successors
    for (const auto &succ : Successors(task)) {
      int32_t sid = succ.task_id;
      int32_t srank = schedule_map[sid].rank;
      const auto *color = detail::getEdgeColor(trank, srank);
      long CommCost = Ctx.CommunicationCost[&task][&succ];

      // Print graph edge
      dot << "  V" << tid << " -> V" << sid << " [color=\"" << color << "\"";
      if (Dump.EdgeLabel)
        dot << " label=<" << CommCost << ">";
      dot << "];\n";
    }

    // Print backward arrows using the list of predecessors
    if (Dump.Predecessors) {
      for (const auto &pred : Predecessors(task)) {
        const auto &pid = pred.task_id;
        dot << "  V" << pid << " -> V" << tid
            << " [style=dashed dir=back color=gray30];\n";
      }
    }
  }

  // Print a single fake entry node
  if (Dump.FakeEntryNode) {
    dot << "  Entry;\n";
    for (const auto *root : Roots(task_map))
      dot << "  Entry -> V" << root->task_id << ";\n";
  }

  // Print a single fake exit node
  if (Dump.FakeExitNode) {
    dot << "  Exit;\n";
    for (const auto &task : Tasks(task_map))
      if (isExitTask(task))
        dot << "  V" << task.task_id << " -> Exit;\n";
  }

  dot << "}\n";
}
