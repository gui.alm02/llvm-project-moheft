//===----------- weighted_roundrobin.cpp - Target-Task scheduler ----------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Implementation of the weighted round-robin scheduler.
//
//===----------------------------------------------------------------------===//

#include "weighted_roundrobin.h"

#include "ft.h"

#include <algorithm>

#include <iostream>
#include <cstring>
#include <string>

void WeightedRoundRobinScheduler::preScheduleTask(int32_t task_id,
                                          const RTLInfoTy *device) {

  std::cout << "in weighted_roundrobin.cpp \n";

  assert(schedule_map.count(task_id) == 1);
  auto &schedule_data = schedule_map[task_id];
  auto &task = schedule_data.task;

  schedule_data.state = TaskState::SCHEDULED;
  // Target data task are all mapped to the same device because we noticed
  // better performance this way (we cannot predict where the data is actually
  // going to be used with round-robin strategy).
  if(isTargetDataTask(task))
  {
    std::cout << "isTargetDataTask \n";
    schedule_data.rank = 1;
  }
  else 
  {
    std::cout << "task_device_current: " << std::to_string(task_device_current) << " task_device_counter: " << std::to_string(task_device_counter) << "\n";

    if(task_device_counter < task_device_weight)
    {
        task_device_counter++;
    }
    else
    {
        task_device_current++;
        if(task_device_current > ft_handler.DevicesGetActiveNumber())
        {
            task_device_current = 1;
        }  
        std::string env_var_string = "OMPCLUSTER_WEIGHTED_ROUNDROBIN_" + std::to_string(task_device_current);
        const char* str = env_var_string.c_str();
        char *EnvStr = std::getenv(str);
        task_device_weight = std::stoi(EnvStr);
        std::cout << "task_device_current: " << std::to_string(task_device_current) << " task_device_weight: " << std::to_string(task_device_weight) << "\n";
        task_device_counter = 1;
    }
    schedule_data.rank = task_device_current;
  }
}
