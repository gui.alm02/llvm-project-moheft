# -*- Python -*- vim: set ft=python ts=4 sw=4 expandtab tw=79:
# Configuration file for the 'lit' test runner.

import os
import lit.formats

# Tell pylint that we know config and lit_config exist somewhere.
if 'PYLINT_IMPORT' in os.environ:
    config = object()
    lit_config = object()

def append_dynamic_library_path(name, value, sep):
    if name in config.environment:
        config.environment[name] = value + sep + config.environment[name]
    else:
        config.environment[name] = value

# name: The name of this test suite.
config.name = 'libomptarget-mpi'

# suffixes: A list of file extensions to treat as test files.
config.suffixes = ['.c', '.cpp', '.cc']

# test_source_root: The root path where tests are located.
config.test_source_root = os.path.dirname(__file__)

# test_exec_root: The root object directory where output is placed
config.test_exec_root = config.libomptarget_obj_root

# test format
config.test_format = lit.formats.ShTest()

# compiler flags
config.test_flags = " -I " + config.test_source_root + \
    " -I " + config.mpi_src + \
    " -I " + config.include_dir + \
    " -L " + config.library_dir;

# add FT wrapper compiler options
config.test_flags += " -Wl,--wrap=MPI_Wait" + \
    " -Wl,--wrap=MPI_Test" + \
    " -Wl,--wrap=MPI_Barrier" + \
    " -Wl,--wrap=MPI_Comm_free" + \
    " -Wl,--wrap=MPI_Mprobe" + \
    " -Wl,--wrap=MPI_Send" + \
    " -Wl,--wrap=MPI_Recv";

# Disable UCX warning
config.environment['UCX_LOG_LEVEL'] = 'error'

# MPI support libraries
config.library_flags = " -l omptarget-mpi-support" + \
    " -l pthread -l dl -l elf -l ffi"

if config.libomptarget_profiler:
    config.library_flags += " -l omptarget-support"

# Check if veloc is available
if config.using_veloc == "true":
    config.veloc_flags = "-l veloc-client"
else:
    config.veloc_flags = ""

if config.omp_host_rtl_directory:
    config.test_flags = config.test_flags + " -L " + \
        config.omp_host_rtl_directory

config.test_flags = config.test_flags + " " + config.test_extra_flags

# Allow REQUIRES / UNSUPPORTED / XFAIL to work
config.target_triple = [ ]
for feature in config.test_compiler_features:
    config.available_features.add(feature)

if config.libomptarget_debug:
  config.available_features.add('libomptarget-debug')

# Setup environment to find dynamic library at runtime
if config.operating_system == 'Windows':
    append_dynamic_library_path('PATH', config.libomp_dir, ":")
    append_dynamic_library_path('PATH', config.library_dir, ";")
    append_dynamic_library_path('PATH', config.omp_host_rtl_directory, ";")
elif config.operating_system == 'Darwin':
    append_dynamic_library_path('DYLD_LIBRARY_PATH', config.libomp_dir, ":")
    append_dynamic_library_path('DYLD_LIBRARY_PATH', config.library_dir, ":")
    append_dynamic_library_path('DYLD_LIBRARY_PATH', \
        config.omp_host_rtl_directory, ";")
    config.test_flags += " -Wl,-rpath," + config.libomp_dir
    config.test_flags += " -Wl,-rpath," + config.library_dir
    config.test_flags += " -Wl,-rpath," + config.omp_host_rtl_directory
else: # Unices
    append_dynamic_library_path('LD_LIBRARY_PATH', config.libomp_dir, ":")
    append_dynamic_library_path('LD_LIBRARY_PATH', config.library_dir, ":")
    append_dynamic_library_path('LD_LIBRARY_PATH', \
        config.omp_host_rtl_directory, ":")

# substitutions

# Fault tolerance module testing
# Only test if there is a recovery flag
if config.recovery_flag != "":
    if config.using_veloc == "true":
        config.substitutions.append(("%libomptarget-compilexx-run-check-ftmpi-veloc",
            "export OMPCLUSTER_CP_USEVELOC=1 && " +
            "export OMPCLUSTER_CP_TESTCFG=" + config.test_source_root + "/ft/veloc.cfg && " +
            "export VELOC_BIN=" + config.veloc_bin + " && " +
            "%libomptarget-compilexx-run-check-ftmpi"))
    else:
        config.substitutions.append(("%libomptarget-compilexx-run-check-ftmpi-veloc",
            "echo ignored-command: not using veloc"))

    config.substitutions.append(("%libomptarget-compilexx-run-check-ftmpi",
        "%ftmpi-compilexx-run | " + config.libomptarget_filecheck + " %s"))

    config.substitutions.append(("%ftmpi-compilexx-run",
        "%ftmpi-compilexx && %ftmpi-run"))

    config.substitutions.append(("%ftmpi-compilexx",
        "%mpicxx %flags %openmp_flags %s -o %t %library_flags %veloc_flags"))

    config.substitutions.append(("%ftmpi-run",
        "mpirun -np 3 %recovery-flag %t"))

    config.substitutions.append(("%recovery-flag",
        "" + config.recovery_flag))
else:
    config.substitutions.append(("%libomptarget-compilexx-run-check-ftmpi",
        "echo ignored-command: used MPI library does not support recovery flags"))

# Event system testing
config.substitutions.append(("%libomptarget-compilempicxx-mpirun-and-check",
    "%libomptarget-compilempicxx && "
    "%libomptarget-mpirun | "
    "%libomptarget-check"))

config.substitutions.append(("%libomptarget-compilempicxx",
    "%mpicxx %flags %openmp_flags %s -o %t %library_flags %veloc_flags"))

config.substitutions.append(("%libomptarget-mpirun",
    "mpirun -np 3 %t"))

config.substitutions.append(("%libomptarget-check",
    f"{config.libomptarget_filecheck} %s"))

# General substitutions
config.substitutions.append(("%mpicxx",
    "export OMPI_CXX=%clangxx && export MPICH_CXX=%clangxx && mpic++"))

config.substitutions.append(("%veloc_flags",
    config.veloc_flags))

config.substitutions.append(("%library_flags",
    config.library_flags))

config.substitutions.append(("%clangxx",
    config.test_cxx_compiler))

config.substitutions.append(("%flags",
    config.test_flags))

config.substitutions.append(("%openmp_flags", config.test_openmp_flags))
