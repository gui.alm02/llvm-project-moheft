//===--- mpi_common.cpp - Common MPI constants and functions ------------*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file contains the implementation of MPI common library functions
//
//===----------------------------------------------------------------------===//

#include "mpi_common.h"

#include <cassert>
#include <cstdlib>

#define MPICH_SKIP_MPICXX
#include <mpi.h>

#include <string>

/// Check if MPI is initialized. If not, initialize with required thread support
int WMPI_Init_thread(int *argc, char ***argv, int required) {
  int is_mpi_initialized = 0;
  int provided = MPI_THREAD_SINGLE;

  MPI_Initialized(&is_mpi_initialized);

  // If not initialized, initialize with required level, else, get the provided
  if (!is_mpi_initialized)
    MPI_Init_thread(argc, argv, required, &provided);
  else
    MPI_Query_thread(&provided);

  // Exits if provided level is lower than required
  if (provided < required) {
    std::string level_names[4] = {"SINGLE", "FUNNELED", "SERIALIZED",
                                  "MULTIPLE"};
    OMPC_DP("Used MPI implementation provides MPI_THREAD_%s support. "
            "MPI_THREAD_%s required",
            level_names[provided].c_str(), level_names[required].c_str());
    exit(1);
  }

#ifdef OMPTARGET_DEBUG
  // Set comm world for debugging
  DebugData &db_data = DebugData::get();
  assert(db_data.world_rank == -1);
  MPI_Comm_rank(MPI_COMM_WORLD, &db_data.world_rank);
#endif // OMPTARGET_DEBUG

  return MPI_SUCCESS;
}

// Debugging
#ifdef OMPTARGET_DEBUG
DebugData::DebugData() : debug_level(-1), world_rank(-1) {}

DebugData &DebugData::get() {
  static DebugData data;

  if (data.debug_level < 0) {
    if (char *envStr = std::getenv("OMPCLUSTER_DEBUG")) {
      data.debug_level = std::stoi(envStr);
    } else {
      data.debug_level = 0;
    }
    assert(data.debug_level >= 0);
  }

  return data;
}
#endif // OMPTARGET_DEBUG
