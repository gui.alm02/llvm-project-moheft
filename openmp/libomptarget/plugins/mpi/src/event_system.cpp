//===------ event_system.cpp - Concurrent MPI communicaiton -----*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file contains the implementation of the MPI Event System used by the MPI
// target runtime for concurrent communication.
//
//===----------------------------------------------------------------------===//

#include "event_system.h"

#include <cassert>
#include <cmath>
#include <cstdio>
#include <ffi.h>

#include <algorithm>
#include <chrono>
#include <limits>
#include <sstream>

#include "omptarget.h"

#include "coroutines.h"
#include "ft.h"
#include "mpi_common.h"
#include "mpi_manager.h"
#include "profiler.h"

// Debug macros
#define DPES(msg_literal, ...)                                                 \
  OMPC_DP("[Event System] -> " msg_literal, ##__VA_ARGS__)

// TODO: Better visualization of error. Allows for formatted strings to be
// printed. Check if `assertm` should be replaced by this.
#define CHECK_DP(expr, msg, ...)                                               \
  if (!(expr)) {                                                               \
    DPES(msg, ##__VA_ARGS__);                                                  \
    std::exit(EXIT_FAILURE);                                                   \
  }

#define assertm(expr, msg) assert(((void)msg, expr));

// Helper coroutine macros
#define EVENT_BEGIN() CO_BEGIN()
#define EVENT_PAUSE() CO_YIELD(false)
#define EVENT_PAUSE_FOR_REQUESTS()                                             \
  while (!checkPendingRequests()) {                                            \
    EVENT_PAUSE();                                                             \
  }
#define EVENT_END() CO_RETURN(true)

// Declaration of needed kmp.h functions and structures
// KEEP IN SYNC WITH ITS ORIGINAL DECLARATIONS
// =============================================================================
extern "C" {

int32_t __kmpc_global_thread_num(void *) __attribute__((weak));

int32_t __kmpc_omp_taskyield(void *loc_ref, int32_t gtid, int end_part)
    __attribute__((weak));

int32_t __kmpc_target_task_current_task_id() __attribute__((weak));
}

static ft::FaultTolerance *ft_handler = nullptr;

namespace event_system {

// Customizable parameters of the event system
// =============================================================================
// Here we declare some configuration variables with their respective default
// values. Every single one of them can be tuned by an environment variable with
// the following name pattern: OMPCLUSTER_VAR_NAME.
namespace config {
// Maximum buffer size to use during data transfer.
static int64_t mpi_fragment_size = 100e6;
// Number of execute event handlers to spawn.
static int num_exec_event_handlers = 1;
// Number of data event handlers to spawn.
static int num_data_event_handlers = 1;
// Polling rate period (us) used by event handlers.
static int event_polling_rate = 1;
// Maximum array size that is packed
static int64_t packing_threshold = 0;
// Number of communicators to be spawned and distributed for the events. Allows
// for parallel use of network resources.
static int64_t num_event_comm = 10;
} // namespace config

// Helper functions
// =============================================================================
const char *toString(EventType type) {
  switch (type) {
  case EventType::DEBUG:
    return "Debug";
  case EventType::ALLOC:
    return "Alloc";
  case EventType::DELETE:
    return "Delete";
  case EventType::RETRIEVE:
    return "Retrieve";
  case EventType::SUBMIT:
    return "Submit";
  case EventType::PACKED_RETRIEVE:
    return "PackedRetrieve";
  case EventType::PACKED_SUBMIT:
    return "PackedSubmit";
  case EventType::PACKED_EXCHANGE:
    return "PackedExchange";
  case EventType::PACKED_BCAST:
    return "PackedBcast";
  case EventType::PACKED_DYNBCAST:
    return "PackedDynBcast";
  case EventType::BCAST:
    return "Broadcast";
  case EventType::DYNBCAST:
    return "DynBcast";
  case EventType::EXCHANGE:
    return "Exchange";
  case EventType::EXECUTE:
    return "Execute";
  case EventType::SYNC:
    return "Sync";
  case EventType::REGISTERCPPTRS:
    return "RegisterCpPtrs";
  case EventType::CHECKPOINT:
    return "Checkpoint";
  case EventType::RECOVERY:
    return "Recovery";
  case EventType::EXIT:
    return "Exit";
  }

  assertm(false, "Every enum value must be checked on the switch above.");
  return nullptr;
}

const char *toString(EventLocation location) {
  switch (location) {
  case EventLocation::DEST:
    return "Destination";
  case EventLocation::ORIG:
    return "Origin";
  }

  assertm(false, "Every enum value must be checked on the switch above.");
  return nullptr;
}

const char *toString(EventState state) {
  switch (state) {
  case EventState::CREATED:
    return "Created";
  case EventState::EXECUTING:
    return "Executing";
  case EventState::WAITING:
    return "Waiting";
  case EventState::FAILED:
    return "Failed";
  case EventState::FINISHED:
    return "Finished";
  }

  assertm(false, "Every enum value must be checked on the switch above.");
  return nullptr;
}

// TODO avoid string and char* toString functions
std::string toString(const std::vector<int> &dest_ranks) {
  std::stringstream ss;
  for (int i = 0; i < dest_ranks.size(); i++) {
    ss << dest_ranks[i];
    if (i < dest_ranks.size() - 1) {
      ss << ",";
    }
  }

  return ss.str();
}

// Base Event implementation
// =============================================================================
BaseEvent::BaseEvent(EventLocation event_location, EventType event_type,
                     int mpi_tag, MPI_Comm target_comm, int orig_rank,
                     std::vector<int> dest_ranks, bool is_packable)
    : event_location(event_location), event_type(event_type), mpi_tag(mpi_tag),
      is_packable(is_packable), target_comm(target_comm), orig_rank(orig_rank),
      dest_ranks(dest_ranks) {
  assertm(mpi_tag >= static_cast<int>(ControlTags::FIRST_EVENT),
          "Event MPI tag must not have a Control Tag value");
  assertm(mpi_tag <= EventSystem::TAG_MAX_VALUE,
          "Event MPI tag must be smaller than the maximum value alllowed");
}

std::string BaseEvent::getEventDescription() {
  std::stringstream details;
  details << "{";
  details << "\"mpi_tag\":" << mpi_tag << ",";
  details << "\"Origin\":" << orig_rank << ",";
  details << "\"Destinations\": [" << toString(dest_ranks) << "],";
  details << "\"Location\":\"" << toString(event_location) << "\"";
  details << "}";
  return details.str();
}

void BaseEvent::notifyNewEvent() {
  DPES("Start event of type %s at its %s, sent from rank %d to %s %s\n",
       toString(event_type), toString(event_location), orig_rank,
       dest_ranks.size() == 1 ? "rank" : "ranks", toString(dest_ranks).c_str());

  if (checkFailure())
    return;

  if (event_location == EventLocation::ORIG) {
    // Sends event request.
    event_request_info[0] = static_cast<uint32_t>(event_type);
    event_request_info[1] = static_cast<uint32_t>(mpi_tag);

    // Notify each destination rank that this event is starting.
    for (int dest_rank : dest_ranks) {
      if (orig_rank != dest_rank)
        MPI_Isend(event_request_info, 2, MPI_UINT32_T, dest_rank,
                  static_cast<int>(ControlTags::EVENT_REQUEST),
                  EventSystem::gate_thread_comm, getNextRequest(dest_rank));
    }
  }
}

bool BaseEvent::checkFailure() {
  // Check if the other side of communication failed. If so, immediately return
  if (ft_handler) {
    int other_process = (event_location == EventLocation::ORIG)
                            ? dest_ranks.front()
                            : orig_rank;
    int this_process = (event_location == EventLocation::ORIG)
                           ? orig_rank
                           : dest_ranks.front();
    if (ft_handler->getProcessState(other_process) == ft::ProcessState::DEAD) {
      DPES("[Rank %d] - Other side communication rank failed (Rank %d).\n",
           this_process, other_process);
      event_state = EventState::FAILED;
      return true;
    }
  }
  return false;
}

bool BaseEvent::runCoroutine() {
  if (event_location == EventLocation::ORIG) {
    return runOrigin();
  } else {
    return runDestination();
  }
}

bool BaseEvent::checkPendingRequests() {
  int requests_completed = false;

  if (checkFailure())
    return true;

  MPI_Testall(pending_requests.size(), pending_requests.data(),
              &requests_completed, MPI_STATUSES_IGNORE);
  return requests_completed;
}

bool BaseEvent::isDone() const {
  return (event_state == EventState::FINISHED) ||
         (event_state == EventState::FAILED);
}

void BaseEvent::progress() {
  // Immediately return if the event is already failed
  if (checkFailure()) {
    return;
  }

  // Immediately return if the event is already finished
  if (isDone()) {
    return;
  }

  // The following code block uses progress_guard to ensure only one thread is
  // executing the progress function at a time, returning immediately for all
  // the other threads (e.g. multiple threads waiting on the same event).
  //
  // If one thread is already advancing the event execution at a node, there is
  // no need for other threads to execute the progress function. Returning
  // immediately frees other threads to execute other events/procedures and
  // allows the events run* coroutines to be implemented in a not thead-safe
  // manner.
  bool expected_progress_guard = false;
  if (!progress_guard.compare_exchange_weak(expected_progress_guard, true)) {
    return;
  }

  // Advance the event local execution depending on its state.
  switch (event_state) {
  case EventState::CREATED:
    notifyNewEvent();
    event_state = EventState::EXECUTING;
    {
      PROF_SCOPED(PROF_LVL_USER, std::string(toString(event_type)) + " / Begin",
                  getEventDescription());
    }
    [[fallthrough]];

  case EventState::EXECUTING:
    if (!runCoroutine()) {
      break;
    }
    event_state = EventState::WAITING;
    [[fallthrough]];

  case EventState::WAITING:
    if (!checkPendingRequests()) {
      break;
    }
    if (event_state != EventState::FAILED)
      event_state = EventState::FINISHED;
    {
      PROF_SCOPED(PROF_LVL_USER,
                  std::string(toString(event_type)) + " / End",
                  getEventDescription());
    }
    [[fallthrough]];

  // This is handled on MPI manager
  case EventState::FAILED:
    [[fallthrough]];

  case EventState::FINISHED:
    break;
  }

  // Allow other threads to call progress again.
  progress_guard = false;
}

void BaseEvent::wait() {
  PROF_SCOPED(PROF_LVL_ALL, std::string(toString(event_type)) + " / Wait");

  const bool is_inside_target_task = __kmpc_target_task_current_task_id() == -1;

  // Advance the event progress until it is completed.
  while (!isDone()) {
    progress();

    if (is_inside_target_task) {
      taskingWait();
    } else {
      blockingWait();
    }
  }
}

void BaseEvent::taskingWait() {
  int omp_global_tid = __kmpc_global_thread_num(nullptr);
  assertm(omp_global_tid >= 0,
          "Tasking wait must be called inside an OpenMP Task context");

  __kmpc_omp_taskyield(nullptr, omp_global_tid, 0 /* default value */);
}

void BaseEvent::blockingWait() {
  std::this_thread::sleep_for(
      std::chrono::microseconds(config::event_polling_rate));
}

EventState BaseEvent::getEventState() const { return event_state; }

MPI_Request *BaseEvent::getNextRequest(int dest_proc) {
  pending_requests.emplace_back(MPI_REQUEST_NULL);
  pending_requests_ranks.emplace_back(dest_proc);
  return &pending_requests.back();
}

// Debug Event implementation
// =============================================================================
DebugEvent::DebugEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                       std::vector<int> dest_ranks, std::string debug_msg)
    : BaseEvent(EventLocation::ORIG, EventType::DEBUG, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is_packable*/),
      debug_msg(debug_msg) {
  assertm((int)debug_msg.size() < config::mpi_fragment_size,
          "Debug message should not exceed the maximum buffer size allowed for "
          "MPI messages");
}

bool DebugEvent::runOrigin() {
  assert(event_location == EventLocation::ORIG);
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");

  EVENT_BEGIN();

  // Add one to the debug message length to account for the '/0'.
  MPI_Isend(debug_msg.c_str(), debug_msg.length() + 1, MPI_CHAR, dest_ranks[0],
            mpi_tag, target_comm, getNextRequest(dest_ranks[0]));

  EVENT_END();
}

DebugEvent::DebugEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                       std::vector<int> dest_ranks)
    : BaseEvent(EventLocation::DEST, EventType::DEBUG, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is_packable*/) {}

bool DebugEvent::runDestination() {
  assert(event_location == EventLocation::DEST);
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");

  MPI_Message message;
  MPI_Status status;
  int message_length = 0;

  EVENT_BEGIN();

  do {
    MPI_Improbe(orig_rank, mpi_tag, target_comm, &has_received, &message,
                &status);
    if (!has_received) {
      EVENT_PAUSE();
    }
  } while (!has_received);

  MPI_Get_count(&status, MPI_CHAR, &message_length);
  debug_msg.resize(message_length);

  MPI_Imrecv(&debug_msg[0], message_length, MPI_CHAR, &message,
             getNextRequest(dest_ranks[0]));

  EVENT_PAUSE_FOR_REQUESTS();

  printf("[Event System] -> Debug event\n"
         " - Origin: %d\n"
         " - Destination: %d\n"
         " - Event tag: %d\n"
         " - Communicator: %p\n"
         " - Message: %s\n",
         orig_rank, dest_ranks[0], mpi_tag, target_comm, debug_msg.c_str());

  EVENT_END();
}

// Alloc Event implementation
// =============================================================================
AllocEvent::AllocEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                       std::vector<int> dest_ranks, int64_t size,
                       uintptr_t *allocated_address)
    : BaseEvent(EventLocation::ORIG, EventType::ALLOC, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is_packable*/),
      size(size), allocated_address_ptr(allocated_address) {
  assertm(size >= 0, "AllocEvent must receive a size >= 0");
  assertm(allocated_address != nullptr,
          "AllocEvent must receive a valid pointer as allocated_address");
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool AllocEvent::runOrigin() {
  assert(event_location == EventLocation::ORIG);

  EVENT_BEGIN();

  MPI_Isend(&size, 1, MPI_INT64_T, dest_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  MPI_Irecv(allocated_address_ptr, sizeof(uintptr_t), MPI_BYTE, dest_rank,
            mpi_tag, target_comm, getNextRequest(dest_rank));

  EVENT_END();
}

AllocEvent::AllocEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                       std::vector<int> dest_ranks)
    : BaseEvent(EventLocation::DEST, EventType::ALLOC, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is_packable*/) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool AllocEvent::runDestination() {
  assert(event_location == EventLocation::DEST);

  void *allocated_ptr = nullptr;

  EVENT_BEGIN();

  MPI_Irecv(&size, 1, MPI_INT64_T, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  allocated_ptr = malloc(size);
  assertm(allocated_ptr != nullptr,
          "Failed to allocated memory on remote process");

  allocated_address = reinterpret_cast<uintptr_t>(allocated_ptr);
  DPES("Allocation of %ld byte done at " DPxMOD "!\n", size,
       DPxPTR(allocated_address));

  MPI_Isend(&allocated_address, sizeof(uintptr_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  EVENT_END();
}

// Delete Event implementation
// =============================================================================
DeleteEvent::DeleteEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                         std::vector<int> dest_ranks, uintptr_t target_address)
    : BaseEvent(EventLocation::ORIG, EventType::DELETE, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is_packable*/),
      target_address(target_address) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool DeleteEvent::runOrigin() {
  assert(event_location == EventLocation::ORIG);

  EVENT_BEGIN();

  MPI_Isend(&target_address, sizeof(uintptr_t), MPI_BYTE, dest_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  // Event completion notification
  MPI_Irecv(nullptr, 0, MPI_BYTE, dest_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

DeleteEvent::DeleteEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                         std::vector<int> dest_ranks)
    : BaseEvent(EventLocation::DEST, EventType::DELETE, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is_packable*/) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool DeleteEvent::runDestination() {
  assert(event_location == EventLocation::DEST);

  EVENT_BEGIN();

  MPI_Irecv(&target_address, sizeof(uintptr_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  free(reinterpret_cast<void *>(target_address));

  DPES("" DPxMOD " freed!\n", DPxPTR(target_address));

  // Event completion notification
  MPI_Isend(nullptr, 0, MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

// Retrieve Event implementation
// =============================================================================
RetrieveEvent::RetrieveEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                             std::vector<int> dest_ranks, void *orig_ptr,
                             const void *dest_ptr, int64_t size)
    : BaseEvent(EventLocation::ORIG, EventType::RETRIEVE, mpi_tag, target_comm,
                orig_rank, dest_ranks, true /*is_packable*/),
      orig_ptr(orig_ptr), dest_ptr(dest_ptr), size(size) {
  assertm(size >= 0, "RetrieveEvent must receive a size >= 0");
  assertm(orig_ptr != nullptr,
          "RetrieveEvent must receive a valid pointer as orig_ptr");
  assertm(dest_ptr != nullptr,
          "RetrieveEvent must receive a valid pointer as dest_ptr");
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool RetrieveEvent::runOrigin() {
  assert(event_location == EventLocation::ORIG);

  char *host_byte_ptr = nullptr;
  int64_t bytes_to_receive = 0;

  EVENT_BEGIN();

  MPI_Isend(&dest_ptr, sizeof(uintptr_t), MPI_BYTE, dest_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  MPI_Isend(&size, sizeof(int64_t), MPI_BYTE, dest_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  // Operates over many fragments of the original buffer of at most
  // mpi_fragment_size bytes.
  host_byte_ptr = reinterpret_cast<char *>(orig_ptr);
  bytes_to_receive = size;
  while (bytes_to_receive > 0) {
    MPI_Irecv(
        &host_byte_ptr[size - bytes_to_receive],
        static_cast<int>(std::min(bytes_to_receive, config::mpi_fragment_size)),
        MPI_BYTE, dest_rank, mpi_tag, target_comm, getNextRequest(dest_rank));
    bytes_to_receive -= config::mpi_fragment_size;
  }

  EVENT_END();
}

RetrieveEvent::RetrieveEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                             std::vector<int> dest_ranks)
    : BaseEvent(EventLocation::DEST, EventType::RETRIEVE, mpi_tag, target_comm,
                orig_rank, dest_ranks, true /*is_packable*/) {}

bool RetrieveEvent::runDestination() {
  assert(event_location == EventLocation::DEST);

  const char *target_byte_ptr = nullptr;
  int64_t bytes_to_send = 0;

  EVENT_BEGIN();

  MPI_Irecv(&dest_ptr, sizeof(uintptr_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  MPI_Irecv(&size, sizeof(int64_t), MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  DPES("Sending %ld byte stored at " DPxMOD "!\n", size, DPxPTR(dest_ptr));

  // Operates over many fragments of the original buffer of at most
  // mpi_fragment_size bytes.
  target_byte_ptr = reinterpret_cast<const char *>(dest_ptr);
  bytes_to_send = size;
  while (bytes_to_send > 0) {
    MPI_Isend(
        &target_byte_ptr[size - bytes_to_send],
        static_cast<int>(std::min(bytes_to_send, config::mpi_fragment_size)),
        MPI_BYTE, orig_rank, mpi_tag, target_comm, getNextRequest(dest_rank));
    bytes_to_send -= config::mpi_fragment_size;
  }

  DPES("Data sent\n");

  EVENT_END();
}

// Submit Event implementation
// =============================================================================
SubmitEvent::SubmitEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                         std::vector<int> dest_ranks, const void *orig_ptr,
                         void *dest_ptr, int64_t size)
    : BaseEvent(EventLocation::ORIG, EventType::SUBMIT, mpi_tag, target_comm,
                orig_rank, dest_ranks, true /*is_packable*/),
      orig_ptr(orig_ptr), dest_ptr(dest_ptr), size(size) {
  assertm(size >= 0, "SubmitEvent must receive a size >= 0");
  assertm(orig_ptr != nullptr,
          "SubmitEvent must receive a valid pointer as orig_ptr");
  assertm(dest_ptr != nullptr,
          "SubmitEvent must receive a valid pointer as dest_ptr");
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool SubmitEvent::runOrigin() {
  assert(event_location == EventLocation::ORIG);

  const char *host_byte_ptr;
  int64_t bytes_to_send;

  EVENT_BEGIN();

  MPI_Isend(&dest_ptr, sizeof(uintptr_t), MPI_BYTE, dest_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  MPI_Isend(&size, sizeof(int64_t), MPI_BYTE, dest_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  // Operates over many fragments of the original buffer of at most
  // mpi_fragment_size bytes.
  host_byte_ptr = reinterpret_cast<const char *>(orig_ptr);
  bytes_to_send = size;
  while (bytes_to_send > 0) {
    MPI_Isend(
        &host_byte_ptr[size - bytes_to_send],
        static_cast<int>(std::min(bytes_to_send, config::mpi_fragment_size)),
        MPI_BYTE, dest_rank, mpi_tag, target_comm, getNextRequest(dest_rank));
    bytes_to_send -= config::mpi_fragment_size;
  }

  // Event completion notification
  MPI_Irecv(nullptr, 0, MPI_BYTE, dest_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

SubmitEvent::SubmitEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                         std::vector<int> dest_ranks)
    : BaseEvent(EventLocation::DEST, EventType::SUBMIT, mpi_tag, target_comm,
                orig_rank, dest_ranks, true /*is_packable*/) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool SubmitEvent::runDestination() {
  assert(event_location == EventLocation::DEST);

  char *target_byte_ptr = nullptr;
  int64_t bytes_to_receive = 0;

  EVENT_BEGIN();

  MPI_Irecv(&dest_ptr, sizeof(uintptr_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  MPI_Irecv(&size, sizeof(int64_t), MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  DPES("Receiving %ld byte to store at " DPxMOD "!\n", size, DPxPTR(dest_ptr));

  // Operates over many fragments of the original buffer of at most
  // mpi_fragment_size bytes.
  target_byte_ptr = reinterpret_cast<char *>(dest_ptr);
  bytes_to_receive = size;
  while (bytes_to_receive > 0) {
    MPI_Irecv(
        &target_byte_ptr[size - bytes_to_receive],
        static_cast<int>(std::min(bytes_to_receive, config::mpi_fragment_size)),
        MPI_BYTE, orig_rank, mpi_tag, target_comm, getNextRequest(dest_rank));
    bytes_to_receive -= config::mpi_fragment_size;
  }

  EVENT_PAUSE_FOR_REQUESTS();

  DPES("Data received\n");

  // Event completion notification
  MPI_Isend(nullptr, 0, MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

// Exchange Event implementation
// =============================================================================
ExchangeEvent::ExchangeEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                             std::vector<int> dest_ranks, int data_src_rank,
                             int data_dst_rank, const void *src_ptr,
                             void *dst_ptr, int64_t size)
    : BaseEvent(EventLocation::ORIG, EventType::EXCHANGE, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is_packable*/),
      data_dst_rank(data_dst_rank), data_src_rank(data_src_rank),
      src_ptr(src_ptr), dst_ptr(dst_ptr), size(size) {
  assertm(size >= 0, "ExchangeEvent must receive a size >= 0");
  assertm(src_ptr != nullptr,
          "ExchangeEvent must receive a valid pointer as src_ptr");
  assertm(dst_ptr != nullptr,
          "ExchangeEvent must receive a valid pointer as dst_ptr");
  assertm(dest_ranks.size() == 2,
          "Exchange event must have exactly two destination ranks");
}

bool ExchangeEvent::runOrigin() {
  assert(event_location == EventLocation::ORIG);
  EVENT_BEGIN();
  // Send metadata to data source and data destination ranks
  for (int dest_rank : dest_ranks) {
    DPES("Forwarding %ld bytes from rank %d (" DPxMOD ") to rank %d (" DPxMOD
         ")\n",
         size, data_src_rank, DPxPTR(src_ptr), data_dst_rank, DPxPTR(dst_ptr));

    MPI_Isend(&data_dst_rank, sizeof(int), MPI_BYTE, dest_rank, mpi_tag,
              target_comm, getNextRequest(dest_rank));

    MPI_Isend(&data_src_rank, sizeof(int), MPI_BYTE, dest_rank, mpi_tag,
              target_comm, getNextRequest(dest_rank));

    MPI_Isend(&src_ptr, sizeof(uintptr_t), MPI_BYTE, dest_rank, mpi_tag,
              target_comm, getNextRequest(dest_rank));

    MPI_Isend(&dst_ptr, sizeof(uintptr_t), MPI_BYTE, dest_rank, mpi_tag,
              target_comm, getNextRequest(dest_rank));

    MPI_Isend(&size, sizeof(int64_t), MPI_BYTE, dest_rank, mpi_tag, target_comm,
              getNextRequest(dest_rank));
  }

  EVENT_PAUSE_FOR_REQUESTS();
  // Event completion notifications
  for (int dest_rank : dest_ranks) {
    MPI_Irecv(nullptr, 0, MPI_BYTE, dest_rank, mpi_tag, target_comm,
              getNextRequest(dest_rank));
  }

  EVENT_END();
}

ExchangeEvent::ExchangeEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                             std::vector<int> dest_ranks)
    : BaseEvent(EventLocation::DEST, EventType::EXCHANGE, mpi_tag, target_comm,
                orig_rank, dest_ranks, true /*is_packable*/),
      data_dst_rank(-1), src_ptr(nullptr), dst_ptr(nullptr), size(0) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool ExchangeEvent::runDestination() {
  assert(event_location == EventLocation::DEST);
  const int test_size = 10;

  const char *host_byte_ptr;
  int64_t bytes_to_send = 0;
  char *target_byte_ptr = nullptr;
  int64_t bytes_to_receive = 0;

  EVENT_BEGIN();
  MPI_Irecv(&data_dst_rank, sizeof(int), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  MPI_Irecv(&data_src_rank, sizeof(int), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  MPI_Irecv(&src_ptr, sizeof(uintptr_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  MPI_Irecv(&dst_ptr, sizeof(uintptr_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  MPI_Irecv(&size, sizeof(int64_t), MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();
  assertm(data_src_rank != orig_rank,
          "ExchangeEvent must not receive data from the origin rank");
  assertm(data_dst_rank != orig_rank,
          "ExchangeEvent must not receive data from the origin rank");
  assertm(src_ptr != nullptr,
          "ExchangeEvent must receive a valid pointer as src_ptr");
  assertm(dst_ptr != nullptr,
          "ExchangeEvent must receive a valid pointer as dst_ptr");
  assertm(size >= 0, "ExchangeEvent must receive a size >= 0");

  DPES("Forwarding %ld bytes from rank %d (" DPxMOD ") to rank %d (" DPxMOD
       ")\n",
       size, data_src_rank, DPxPTR(src_ptr), data_dst_rank, DPxPTR(dst_ptr));

  if (dest_rank == data_src_rank) {
    // TODO: remove this test
    value_test.resize(test_size);
    value_test[0] = 20;
    MPI_Isend(value_test.data(), sizeof(int) * test_size, MPI_BYTE,
              data_dst_rank, mpi_tag, target_comm, getNextRequest(dest_rank));
    // Operates over many fragments of the original buffer of at most
    // mpi_fragment_size bytes.
    host_byte_ptr = reinterpret_cast<const char *>(src_ptr);
    bytes_to_send = size;
    while (bytes_to_send > 0) {
      MPI_Isend(
          &host_byte_ptr[size - bytes_to_send],
          static_cast<int>(std::min(bytes_to_send, config::mpi_fragment_size)),
          MPI_BYTE, data_dst_rank, mpi_tag, target_comm,
          getNextRequest(dest_rank));
      bytes_to_send -= config::mpi_fragment_size;
    }
  }

  if (dest_rank == data_dst_rank) {
    value_test.resize(test_size);
    MPI_Irecv(value_test.data(), sizeof(int) * test_size, MPI_BYTE,
              data_src_rank, mpi_tag, target_comm, getNextRequest(dest_rank));
    // Operates over many fragments of the original buffer of at most
    // mpi_fragment_size bytes.
    target_byte_ptr = reinterpret_cast<char *>(dst_ptr);
    bytes_to_receive = size;
    while (bytes_to_receive > 0) {
      MPI_Irecv(&target_byte_ptr[size - bytes_to_receive],
                static_cast<int>(
                    std::min(bytes_to_receive, config::mpi_fragment_size)),
                MPI_BYTE, data_src_rank, mpi_tag, target_comm,
                getNextRequest(dest_rank));
      bytes_to_receive -= config::mpi_fragment_size;
    }
  }

  EVENT_PAUSE_FOR_REQUESTS();
  // Event completion notification
  MPI_Isend(nullptr, 0, MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

// PackedEvent implementation
// =============================================================================
MPI_Datatype PackedEvent::MPI_PACKED_SUBMIT_METADATA_T;
MPI_Datatype PackedEvent::MPI_PACKED_RETRIEVE_METADATA_T;
MPI_Datatype PackedEvent::MPI_PACKED_EXCHANGE_METADATA_T;
MPI_Datatype PackedEvent::MPI_PACKED_BCAST_METADATA_T;

bool PackedEvent::isPackingEnabled() {
  const char *env_str = std::getenv("OMPCLUSTER_ENABLE_PACKING");
  constexpr const char *default_strategy = "0";
  std::string selected_mode = env_str != nullptr ? env_str : default_strategy;

  return (selected_mode == "1");
}

void PackedEvent::initMPICustomDatatype() {
  if (isPackingEnabled()) {
    // Commits MPI custom datatypes
    // Submit metadata MPI datatype
    {
      struct metadata_t {
        int64_t size;
        void *dest_ptr;
      };
      int count = 2;

      MPI_Datatype tmp_type;
      int array_of_blocklengths[] = {1, 1};
      MPI_Aint array_of_displacements[] = {offsetof(metadata_t, size),
                                           offsetof(metadata_t, dest_ptr)};
      MPI_Datatype array_of_types[] = {MPI_INT64_T, MPI_INT64_T};
      MPI_Type_create_struct(count, array_of_blocklengths,
                             array_of_displacements, array_of_types, &tmp_type);

      // Resized is needed to use properly send this as an array or std::vector
      MPI_Aint lb, extent;
      MPI_Type_get_extent(tmp_type, &lb, &extent);
      MPI_Type_create_resized(tmp_type, lb, extent,
                              &MPI_PACKED_SUBMIT_METADATA_T);
      MPI_Type_commit(&MPI_PACKED_SUBMIT_METADATA_T);
    }

    // Retrieve metadata MPI datatype
    MPI_PACKED_RETRIEVE_METADATA_T = MPI_PACKED_SUBMIT_METADATA_T;

    // Broadcast metadata MPI datatype
    MPI_PACKED_BCAST_METADATA_T = MPI_PACKED_SUBMIT_METADATA_T;

    // Exchange metadata MPI datatype
    {
      struct metadata_t {
        int64_t size;
        const void *src_ptr;
        void *dst_ptr;
        int dst_rank;
      };

      int count = 4;
      MPI_Datatype tmp_type;
      int array_of_blocklengths[] = {1, 1, 1, 1};
      MPI_Aint array_of_displacements[] = {
          offsetof(metadata_t, size), offsetof(metadata_t, src_ptr),
          offsetof(metadata_t, dst_ptr), offsetof(metadata_t, dst_rank)};
      MPI_Datatype array_of_types[] = {MPI_INT64_T, MPI_INT64_T, MPI_INT64_T,
                                       MPI_INT};
      MPI_Type_create_struct(count, array_of_blocklengths,
                             array_of_displacements, array_of_types, &tmp_type);
      MPI_Aint lb, extent;
      MPI_Type_get_extent(tmp_type, &lb, &extent);
      MPI_Type_create_resized(tmp_type, lb, extent,
                              &MPI_PACKED_EXCHANGE_METADATA_T);
      MPI_Type_commit(&MPI_PACKED_EXCHANGE_METADATA_T);
    }
  }
}

void PackedEvent::freeMPICustomDatatype() {
  if (isPackingEnabled()) {
    MPI_Type_free(&MPI_PACKED_SUBMIT_METADATA_T);
    MPI_Type_free(&MPI_PACKED_EXCHANGE_METADATA_T);
  }
}

void PackedEvent::packEvent(EventPtr event) { packed_events.push_back(event); }

EventType PackedEvent::packedType() {
  // Safe to check front since every PackedEvent has at least one event packed.
  return packed_events.front()->event_type;
}

// Packed Broadcast Event implementation
// =============================================================================
PackedBcastEvent::PackedBcastEvent(int mpi_tag, MPI_Comm target_comm,
                                   int orig_rank, std::vector<int> dest_ranks,
                                   EventPtr event)
    : PackedEvent(EventLocation::ORIG, EventType::PACKED_BCAST, mpi_tag,
                  target_comm, orig_rank, dest_ranks, false /*is_packable*/) {

  int world_size = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  assertm(
      dest_ranks.size() == world_size - 1,
      "BcastEvent must have all processes as destination, except the root.");

  metadata_buffers.resize(world_size);
  packed_buffers.resize(world_size);
  packEvent(event);
}

bool PackedBcastEvent::runOrigin() {
  int64_t packed_buffer_size = 0;
  int position = 0;
  char *host_byte_ptr = nullptr;
  int64_t bytes_to_send = 0;
  int64_t metadata_buffer_size = 0;

  EVENT_BEGIN();
  assertm(packed_events.size() >= 1,
          "Must have at least one event in packed list");
  for (auto base_ev : packed_events) {
    auto ev = std::static_pointer_cast<BcastEvent>(base_ev);
    metadata_buffer_size +=
        sizeof(int64_t) + sizeof(uintptr_t); // Metadata info for event

    if (ev->size <= config::packing_threshold) {
      packed_buffer_size += ev->size;
    }
  }

  // Pack and send metadata to each destination rank
  for (auto dest_rank : dest_ranks) {
    metadata_buffers[dest_rank].resize(metadata_buffer_size);
    position = 0;

    for (auto base_ev : packed_events) {
      auto ev = std::static_pointer_cast<BcastEvent>(base_ev);
      MPI_Pack(&ev->size, 1, MPI_INT64_T, metadata_buffers[dest_rank].data(),
               metadata_buffer_size, &position, ev->target_comm);

      MPI_Pack(&ev->tgt_ptrs[dest_rank], sizeof(uintptr_t), MPI_BYTE,
               metadata_buffers[dest_rank].data(), metadata_buffer_size,
               &position, ev->target_comm);
    }
    assertm(position == metadata_buffer_size,
            "Metadata buffer size does not match position");

    MPI_Isend(&metadata_buffer_size, sizeof(int64_t), MPI_BYTE, dest_rank,
              mpi_tag, target_comm, getNextRequest(dest_rank));
    MPI_Isend(metadata_buffers[dest_rank].data(), metadata_buffer_size,
              MPI_BYTE, dest_rank, mpi_tag, target_comm,
              getNextRequest(dest_rank));
  }

  // Pack buffers that are small enough to be packed
  packed_buffers.resize(packed_buffer_size);
  position = 0;
  for (auto base_ev : packed_events) {
    auto ev = std::static_pointer_cast<BcastEvent>(base_ev);
    if (ev->size <= config::packing_threshold) {
      MPI_Pack(ev->host_ptr, ev->size, MPI_BYTE, packed_buffers.data(),
               packed_buffer_size, &position, ev->target_comm);
    }
  }
  assertm(position == packed_buffer_size,
          "Packed buffer size does not match position");

  EVENT_PAUSE_FOR_REQUESTS();

  // Broadcast packed buffers to each destination rank
  if (packed_buffer_size > 0) {
    host_byte_ptr = packed_buffers.data();
    bytes_to_send = packed_buffer_size;
    while (bytes_to_send > 0) {
      MPI_Ibcast(
          &host_byte_ptr[packed_buffer_size - bytes_to_send],
          static_cast<int>(std::min(bytes_to_send, config::mpi_fragment_size)),
          MPI_PACKED, 0, EventSystem::collective_comm,
          getNextRequest(ft::FT_MPI_COLLECTIVE));
      bytes_to_send -= config::mpi_fragment_size;
    }
  }

  EVENT_PAUSE_FOR_REQUESTS();

  // Broadcast not packed buffers to each destination rank
  for (auto base_ev : packed_events) {
    auto ev = std::static_pointer_cast<BcastEvent>(base_ev);
    if (ev->size > config::packing_threshold) {
      host_byte_ptr =
          const_cast<char *>(reinterpret_cast<const char *>(ev->host_ptr));
      bytes_to_send = ev->size;
      while (bytes_to_send > 0) {
        MPI_Ibcast(&host_byte_ptr[ev->size - bytes_to_send],
                   static_cast<int>(
                       std::min(bytes_to_send, config::mpi_fragment_size)),
                   MPI_BYTE, 0, EventSystem::collective_comm,
                   getNextRequest(ft::FT_MPI_COLLECTIVE));
        bytes_to_send -= config::mpi_fragment_size;
      }
    }
  }

  EVENT_PAUSE_FOR_REQUESTS();

  // Event completion notification
  for (auto dest_rank : dest_ranks) {
    MPI_Irecv(nullptr, 0, MPI_BYTE, dest_rank, mpi_tag, target_comm,
              getNextRequest(dest_rank));
  }

  EVENT_END();
}

PackedBcastEvent::PackedBcastEvent(int mpi_tag, MPI_Comm target_comm,
                                   int orig_rank, std::vector<int> dest_ranks)
    : PackedEvent(EventLocation::DEST, EventType::PACKED_BCAST, mpi_tag,
                  target_comm, orig_rank, dest_ranks, false /*is_packable*/) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool PackedBcastEvent::runDestination() {
  int64_t packed_buffer_size = 0;
  char *target_byte_ptr = nullptr;
  int64_t bytes_to_receive = 0;
  int position = 0;
  int n_events = 0;

  EVENT_BEGIN();
  // Get metadata buffer size
  MPI_Irecv(&metadata_buffer_size, sizeof(int64_t), MPI_BYTE, orig_rank,
            mpi_tag, target_comm, getNextRequest(orig_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  assertm(metadata_buffer_size > 0,
          "Metadata buffer size must be greater than zero");
  rcv_metadata_buffer.resize(metadata_buffer_size);

  MPI_Irecv(rcv_metadata_buffer.data(), metadata_buffer_size, MPI_BYTE,
            orig_rank, mpi_tag, target_comm, getNextRequest(orig_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  // Unpack metadata buffer
  position = 0;
  n_events = rcv_metadata_buffer.size() / (sizeof(int64_t) + sizeof(uintptr_t));
  buffer_sizes.reserve(n_events);
  buffer_ptrs.reserve(n_events);

  while (position < rcv_metadata_buffer.size()) {
    int buffer_size;
    char *tgt_ptr;
    MPI_Unpack(rcv_metadata_buffer.data(), rcv_metadata_buffer.size(),
               &position, &buffer_size, 1, MPI_INT64_T, target_comm);
    MPI_Unpack(rcv_metadata_buffer.data(), rcv_metadata_buffer.size(),
               &position, &tgt_ptr, sizeof(uintptr_t), MPI_BYTE, target_comm);

    buffer_sizes.push_back(buffer_size);
    buffer_ptrs.push_back(tgt_ptr);
  }
  assertm(position == rcv_metadata_buffer.size(),
          "Metadata buffer size does not match position");

  // Calculate packed buffer size
  packed_buffer_size = 0;
  for (auto buffer_size : buffer_sizes) {
    assertm(buffer_size > 0, "Invalid buffer size");
    if (buffer_size <= config::packing_threshold) {
      packed_buffer_size += buffer_size;
    }
  }

  // Receive packed buffers via broadcast
  rcv_packed_buffer.resize(packed_buffer_size);
  target_byte_ptr = reinterpret_cast<char *>(rcv_packed_buffer.data());
  bytes_to_receive = packed_buffer_size;

  EVENT_PAUSE_FOR_REQUESTS();

  while (bytes_to_receive > 0) {
    MPI_Ibcast(
        &target_byte_ptr[rcv_packed_buffer.size() - bytes_to_receive],
        static_cast<int>(std::min(bytes_to_receive, config::mpi_fragment_size)),
        MPI_PACKED, 0, EventSystem::collective_comm,
        getNextRequest(ft::FT_MPI_COLLECTIVE));
    bytes_to_receive -= config::mpi_fragment_size;
  }

  // Receive not packed buffers via broadcast
  for (int i = 0; i < buffer_ptrs.size(); i++) {
    if (buffer_sizes[i] > config::packing_threshold) {
      bytes_to_receive = buffer_sizes[i];
      target_byte_ptr = reinterpret_cast<char *>(buffer_ptrs[i]);
      while (bytes_to_receive > 0) {
        MPI_Ibcast(&target_byte_ptr[buffer_sizes[i] - bytes_to_receive],
                   static_cast<int>(
                       std::min(bytes_to_receive, config::mpi_fragment_size)),
                   MPI_BYTE, 0, EventSystem::collective_comm,
                   getNextRequest(ft::FT_MPI_COLLECTIVE));
        bytes_to_receive -= config::mpi_fragment_size;
      }
    }
  }

  EVENT_PAUSE_FOR_REQUESTS();

  unpackEvents();

  // Event completion notification
  MPI_Isend(nullptr, 0, MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(orig_rank));

  EVENT_END();
}

void PackedBcastEvent::unpackEvents() {}

// Packed DynBcast Event implementation
// =============================================================================
PackedDynBcastEvent::PackedDynBcastEvent(
    int mpi_tag, MPI_Comm target_comm, int orig_rank,
    std::vector<int> dest_ranks, std::shared_ptr<event_system::BaseEvent> event)
    : PackedEvent(EventLocation::ORIG, EventType::PACKED_DYNBCAST, mpi_tag,
                  target_comm, orig_rank, dest_ranks, false /*is_packable */) {

  packEvent(event);
}

void PackedDynBcastEvent::updateSendList() {
  // Get fwd_list from packed event
  auto dyn_bcast =
      std::static_pointer_cast<DynBcastEvent>(packed_events.front());
  dyn_bcast->scheduleBcast();
  fwd_list = dyn_bcast->fwd_list;

  // Get dest_ranks from DynBcastEvent scheduling
  dest_ranks = DynBcastEvent::getSendList(fwd_list, orig_rank);
}

bool PackedDynBcastEvent::runOrigin() {
  int64_t packed_buffer_size = 0;
  int64_t metadata_buffer_size = 0;
  char *host_byte_ptr = nullptr;
  int64_t bytes_to_send = 0;
  int position = 0;

  EVENT_BEGIN();

  // Pack metadata into serialized buffer
  if (metadata_buffer.size() == 0) {
    // Serialize metadata
    assertm(packed_events.size() >= 1, "No events to pack");
    position = 0;

    // Calculate metadata buffer size
    auto ev = std::static_pointer_cast<DynBcastEvent>(packed_events.front());
    metadata_buffer_size = 0;
    // size to pack fwd_list
    metadata_buffer_size += sizeof(int) + fwd_list.size() * sizeof(int);
    metadata_buffer_size +=
        sizeof(int) +                                  // number of events
        sizeof(int) +                                  // tgt_ptrs size
        packed_events.size() *                         // for each event
            (sizeof(int64_t) +                         // buffer size
             sizeof(uintptr_t) * (ev->tgt_ptrs.size()) // list of tgt ptrs
            );

    metadata_buffer.resize(metadata_buffer_size);

    // Pack propagation tree
    int fwd_list_size = fwd_list.size();
    assertm(fwd_list_size > 0, "Propagation tree is empty");
    MPI_Pack(&fwd_list_size, 1, MPI_INT, metadata_buffer.data(),
             metadata_buffer_size, &position, target_comm);
    MPI_Pack(fwd_list.data(), fwd_list.size(), MPI_INT, metadata_buffer.data(),
             metadata_buffer_size, &position, target_comm);

    // Pack events metadata
    int n_packed_events = packed_events.size();

    // Aux metadata array
    buffer_sizes.reserve(n_packed_events);
    curr_rank_ptrs.reserve(n_packed_events);

    MPI_Pack(&n_packed_events, 1, MPI_INT, metadata_buffer.data(),
             metadata_buffer_size, &position, target_comm);
    int tgt_ptrs_size = ev->tgt_ptrs.size();
    MPI_Pack(&tgt_ptrs_size, 1, MPI_INT, metadata_buffer.data(),
             metadata_buffer_size, &position, target_comm);
    packed_buffer_size = 0;
    for (auto base_ev : packed_events) {
      auto ev = std::static_pointer_cast<DynBcastEvent>(base_ev);

      // Check if we have the hst ptr in tgt_ptrs list
      for (auto ptr : ev->tgt_ptrs) {
        assertm(ptr != nullptr, "Invalid target pointer");
      }

      // Pack buffer size and buffer pointer on each target, including the host
      buffer_sizes.push_back(ev->size);
      curr_rank_ptrs.push_back(ev->tgt_ptrs[orig_rank]);
      MPI_Pack(&ev->size, 1, MPI_INT64_T, metadata_buffer.data(),
               metadata_buffer_size, &position, target_comm);
      MPI_Pack(ev->tgt_ptrs.data(), ev->tgt_ptrs.size() * sizeof(uintptr_t),
               MPI_BYTE, metadata_buffer.data(), metadata_buffer_size,
               &position, target_comm);

      // Check if buffer is packable
      if (ev->size <= config::packing_threshold) {
        packed_buffer_size += ev->size;
      }
    }
    assertm(position == metadata_buffer_size,
            "Metadata buffer size does not match position");

    // Pack buffers that are small enough to be packed
    if (packed_buffer_size > 0) {
      packed_buffers.resize(packed_buffer_size);
      position = 0;
      for (auto base_ev : packed_events) {
        auto ev = std::static_pointer_cast<DynBcastEvent>(base_ev);
        if (ev->size <= config::packing_threshold) {
          MPI_Pack(ev->host_ptr, ev->size, MPI_BYTE, packed_buffers.data(),
                   packed_buffer_size, &position, target_comm);
        }
      }
      assertm(position == packed_buffer_size,
              "Packed buffer size does not match position");
      DPES("Packed buffer size: %ld\n", packed_buffer_size);
    }
  }

  // Assert metadata buffer and packed buffers are ready
  packed_buffer_size = packed_buffers.size();
  assertm(metadata_buffer.size() > 0, "Metadata buffer not ready");
  assertm(fwd_list.size() > 0, "Fwd list not ready");
  assertm(dest_ranks == DynBcastEvent::getSendList(fwd_list, orig_rank),
          "Dest_ranks is not correct");
  assertm(packed_buffer_size >= 0, "Packed buffer size is negative");

  // Send metadata array
  metadata_buffer_size = metadata_buffer.size();
  for (auto dest_rank : dest_ranks) {
    MPI_Isend(&metadata_buffer_size, sizeof(int64_t), MPI_BYTE, dest_rank,
              mpi_tag, target_comm, getNextRequest(dest_rank));
    MPI_Isend(&packed_buffer_size, sizeof(int64_t), MPI_BYTE, dest_rank,
              mpi_tag, target_comm, getNextRequest(dest_rank));

    host_byte_ptr = const_cast<char *>(
        reinterpret_cast<const char *>(metadata_buffer.data()));
    bytes_to_send = metadata_buffer_size;
    while (bytes_to_send > 0) {
      MPI_Isend(
          &host_byte_ptr[metadata_buffer_size - bytes_to_send],
          static_cast<int>(std::min(bytes_to_send, config::mpi_fragment_size)),
          MPI_BYTE, dest_rank, mpi_tag, target_comm, getNextRequest(dest_rank));
      bytes_to_send -= config::mpi_fragment_size;
    }
  }

  // Send packed buffer
  DPES("Packed buffer size: %ld\n", packed_buffer_size);
  if (packed_buffers.size() > 0) {
    DPES("Sending packed buffer of size %ld\n", packed_buffers.size());
    for (auto dest_rank : dest_ranks) {
      host_byte_ptr = const_cast<char *>(
          reinterpret_cast<const char *>(packed_buffers.data()));
      bytes_to_send = packed_buffers.size();
      while (bytes_to_send > 0) {
        MPI_Isend(&host_byte_ptr[packed_buffers.size() - bytes_to_send],
                  static_cast<int>(
                      std::min(bytes_to_send, config::mpi_fragment_size)),
                  MPI_BYTE, dest_rank, mpi_tag, target_comm,
                  getNextRequest(dest_rank));
        bytes_to_send -= config::mpi_fragment_size;
      }
    }
  }

  // Send non-packed buffers
  assertm(buffer_sizes.size() == curr_rank_ptrs.size(),
          "Invalid number of buffers metadata");
  for (int i = 0; i < buffer_sizes.size(); i++) {
    int size = buffer_sizes[i];
    if (size > config::packing_threshold) {
      for (auto dest_rank : dest_ranks) {
        host_byte_ptr = const_cast<char *>(
            reinterpret_cast<const char *>(curr_rank_ptrs[i]));
        bytes_to_send = size;
        while (bytes_to_send > 0) {
          MPI_Isend(&host_byte_ptr[size - bytes_to_send],
                    static_cast<int>(
                        std::min(bytes_to_send, config::mpi_fragment_size)),
                    MPI_BYTE, dest_rank, mpi_tag, target_comm,
                    getNextRequest(dest_rank));
          bytes_to_send -= config::mpi_fragment_size;
        }
      }
    }
  }

  // Event completion notification
  for (auto dest_rank : dest_ranks) {
    MPI_Irecv(nullptr, 0, MPI_BYTE, dest_rank, mpi_tag, target_comm,
              getNextRequest(dest_rank));
  }

  EVENT_END();
}

PackedDynBcastEvent::PackedDynBcastEvent(int mpi_tag, MPI_Comm target_comm,
                                         int orig_rank,
                                         std::vector<int> dest_ranks)
    : PackedEvent(EventLocation::DEST, EventType::PACKED_DYNBCAST, mpi_tag,
                  target_comm, orig_rank, dest_ranks, false /*is_packable */) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool PackedDynBcastEvent::runDestination() {
  EventPtr remote_dynbcast;
  char *target_byte_ptr = nullptr;
  int64_t bytes_to_receive = 0;

  EVENT_BEGIN();
  // Get metadata buffer size
  MPI_Irecv(&metadata_buffer_size, sizeof(int64_t), MPI_BYTE, orig_rank,
            mpi_tag, target_comm, getNextRequest(orig_rank));
  MPI_Irecv(&packed_buffer_size, sizeof(int64_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(orig_rank));
  EVENT_PAUSE_FOR_REQUESTS();

  assertm(packed_buffer_size >= 0, "Packed buffer size is negative");
  assertm(metadata_buffer_size > 0, "Metadata buffer size is 0");
  DPES("Metadata buffer size: %ld\n", metadata_buffer_size);
  DPES("Packed buffer size: %ld\n", packed_buffer_size);

  rcv_metadata_buffer.resize(metadata_buffer_size);
  rcv_packed_buffer.resize(packed_buffer_size);

  // Receive metadata buffer
  target_byte_ptr = const_cast<char *>(
      reinterpret_cast<const char *>(rcv_metadata_buffer.data()));
  bytes_to_receive = metadata_buffer_size;
  while (bytes_to_receive > 0) {
    MPI_Irecv(
        &target_byte_ptr[metadata_buffer_size - bytes_to_receive],
        static_cast<int>(std::min(bytes_to_receive, config::mpi_fragment_size)),
        MPI_BYTE, orig_rank, mpi_tag, target_comm, getNextRequest(dest_rank));
    bytes_to_receive -= config::mpi_fragment_size;
  }

  // Receive packed buffers
  if (packed_buffer_size > 0) {
    assertm(rcv_packed_buffer.size() == packed_buffer_size,
            "Packed buffer size does not match ");
    target_byte_ptr = const_cast<char *>(
        reinterpret_cast<const char *>(rcv_packed_buffer.data()));
    bytes_to_receive = packed_buffer_size;
    while (bytes_to_receive > 0) {
      MPI_Irecv(&target_byte_ptr[packed_buffer_size - bytes_to_receive],
                static_cast<int>(
                    std::min(bytes_to_receive, config::mpi_fragment_size)),
                MPI_BYTE, orig_rank, mpi_tag, target_comm,
                getNextRequest(dest_rank));
      bytes_to_receive -= config::mpi_fragment_size;
    }
    DPES("Received packed buffer of size %ld\n", packed_buffer_size);
  }

  // Unpack fwd_list
  EVENT_PAUSE_FOR_REQUESTS();
  int fwd_list_size;
  position = 0;
  MPI_Unpack(rcv_metadata_buffer.data(), metadata_buffer_size, &position,
             &fwd_list_size, 1, MPI_INT, target_comm);
  assertm(fwd_list_size > 0, "Fwd list size is 0");
  fwd_list.resize(fwd_list_size);
  DPES("Fwd list size: %d\n", fwd_list_size);
  MPI_Unpack(rcv_metadata_buffer.data(), metadata_buffer_size, &position,
             fwd_list.data(), fwd_list_size, MPI_INT, target_comm);

  // Unpack remaining metadata
  int n_packed_events;
  MPI_Unpack(rcv_metadata_buffer.data(), metadata_buffer_size, &position,
             &n_packed_events, 1, MPI_INT, target_comm);
  DPES("Number of packed events: %d\n", n_packed_events);
  assertm(n_packed_events > 0, "Number of packed events is 0");

  int tgt_ptrs_size;
  MPI_Unpack(rcv_metadata_buffer.data(), metadata_buffer_size, &position,
             &tgt_ptrs_size, 1, MPI_INT, target_comm);
  DPES("Target pointers size: %d\n", tgt_ptrs_size);
  assertm(tgt_ptrs_size > 0, "Target pointers size is 0");

  // Unpack events metadata
  tgt_ptrs.resize(tgt_ptrs_size);
  buffer_sizes.reserve(n_packed_events);
  assertm(n_packed_events > 0, "Number of packed events is 0");
  curr_rank_ptrs.reserve(n_packed_events);

  packed_buffer_size = 0;
  while (position < metadata_buffer_size) {
    int64_t size;
    MPI_Unpack(rcv_metadata_buffer.data(), metadata_buffer_size, &position,
               &size, 1, MPI_INT64_T, target_comm);

    MPI_Unpack(rcv_metadata_buffer.data(), metadata_buffer_size, &position,
               tgt_ptrs.data(), tgt_ptrs_size * sizeof(uintptr_t), MPI_BYTE,
               target_comm);
    curr_rank_ptrs.push_back(tgt_ptrs[dest_rank]);

    buffer_sizes.push_back(size);

    if (size <= config::packing_threshold) {
      packed_buffer_size += size;
    }
  }
  assertm(position == metadata_buffer_size,
          "Metadata buffer not fully unpacked");
  assertm(buffer_sizes.size() == curr_rank_ptrs.size(),
          "Number of packed events is incorrect");

  // Receive non-packed buffers
  for (int i = 0; i < buffer_sizes.size(); i++) {
    int size = buffer_sizes[i];
    if (size > config::packing_threshold) {
      target_byte_ptr =
          const_cast<char *>(reinterpret_cast<const char *>(curr_rank_ptrs[i]));
      bytes_to_receive = size;
      while (bytes_to_receive > 0) {
        MPI_Irecv(&target_byte_ptr[size - bytes_to_receive],
                  static_cast<int>(
                      std::min(bytes_to_receive, config::mpi_fragment_size)),
                  MPI_BYTE, orig_rank, mpi_tag, target_comm,
                  getNextRequest(dest_rank));
        bytes_to_receive -= config::mpi_fragment_size;
      }
    }
  }

  EVENT_PAUSE_FOR_REQUESTS();
  // Forward events
  assertm(fwd_list.size() > 0, "Fwd list size is 0");
  fwd_ranks = DynBcastEvent::getSendList(fwd_list, dest_rank);

  if (fwd_ranks.size() > 0) {
    remote_dynbcast = std::make_shared<PackedDynBcastEvent>(
        mpi_tag, target_comm, dest_rank, fwd_ranks, nullptr);

    auto ev = std::static_pointer_cast<PackedDynBcastEvent>(remote_dynbcast);
    ev->metadata_buffer_size = rcv_metadata_buffer.size();
    ev->metadata_buffer = rcv_metadata_buffer;
    ev->packed_buffers = rcv_packed_buffer;
    ev->fwd_list = fwd_list;
    ev->tgt_ptrs = tgt_ptrs;
    ev->curr_rank_ptrs = curr_rank_ptrs;
    ev->buffer_sizes = buffer_sizes;

    remote_dynbcast->progress();
    forwarded_bcasts.push_back(ev);
  }
  for (auto ev : forwarded_bcasts) {
    ev->wait();
  };

  // Unpack buffers
  position = 0;
  for (int i = 0; i < buffer_sizes.size(); i++) {
    int size = buffer_sizes[i];
    if (size <= config::packing_threshold) {
      MPI_Unpack(rcv_packed_buffer.data(), rcv_packed_buffer.size(), &position,
                 curr_rank_ptrs[i], size, MPI_BYTE, target_comm);
    }
  }
  assertm(position == rcv_packed_buffer.size(),
          "Packed buffer not fully unpacked");

  // Event completion notification
  MPI_Isend(nullptr, 0, MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(orig_rank));
  EVENT_END();
}

// Packed Submit Event implementation
// =============================================================================
PackedSubmitEvent::PackedSubmitEvent(int mpi_tag, MPI_Comm target_comm,
                                     int orig_rank, std::vector<int> dest_ranks,
                                     EventPtr event)
    : PackedEvent(EventLocation::ORIG, EventType::PACKED_SUBMIT, mpi_tag,
                  target_comm, orig_rank, dest_ranks, false /*is_packable*/) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
  packEvent(event);
}

bool PackedSubmitEvent::runOrigin() {
  int64_t packed_buffer_size = 0;
  int position = 0;
  std::shared_ptr<SubmitEvent> event;
  char *host_byte_ptr = nullptr;
  int64_t bytes_to_send = 0;

  EVENT_BEGIN();

  DPES("Packing %ld events\n", packed_events.size());

  // Build metadata array
  packed_events_metadata.reserve(packed_events.size());
  packed_buffer_size = 0;

  for (auto base_event : packed_events) {
    event = std::static_pointer_cast<SubmitEvent>(base_event);
    packed_events_metadata.push_back({event->size, event->dest_ptr});

    // Calculates packed buffer size
    if (event->size <= config::packing_threshold) {
      packed_buffer_size += event->size;
    }
  }

  // Send metadata array
  MPI_Isend(&packed_events_metadata[0], packed_events_metadata.size(),
            MPI_PACKED_SUBMIT_METADATA_T, dest_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  // Send packed buffer
  DPES("Packed buffer size: %ld bytes\n", packed_buffer_size);
  if (packed_buffer_size > 0) {
    // Pack the data on a contiguous buffer
    packed_buffer.resize(packed_buffer_size);
    position = 0;
    for (auto base_event : packed_events) {
      event = std::static_pointer_cast<SubmitEvent>(base_event);
      if (event->size <= config::packing_threshold) {
        MPI_Pack(event->orig_ptr, event->size, MPI_BYTE, &packed_buffer[0],
                 packed_buffer.size(), &position, event->target_comm);
      }
    }
    assertm(position == packed_buffer.size(),
            "Buffer was not correctly written");

    // Send packed data
    host_byte_ptr = reinterpret_cast<char *>(&packed_buffer[0]);
    bytes_to_send = packed_buffer.size();
    while (bytes_to_send > 0) {
      MPI_Isend(&host_byte_ptr[packed_buffer.size() - bytes_to_send],
                std::min(bytes_to_send, config::mpi_fragment_size), MPI_PACKED,
                dest_rank, mpi_tag, target_comm, getNextRequest(dest_rank));
      bytes_to_send -= config::mpi_fragment_size;
    }
  }

  // Loop through non-packed data and send it using separated messages
  for (auto base_event : packed_events) {
    event = std::static_pointer_cast<SubmitEvent>(base_event);

    if (event->size > config::packing_threshold) {
      DPES("Packing: sending bigger-than-treshold buffer on a separated "
           "message\n");
      host_byte_ptr =
          const_cast<char *>(reinterpret_cast<const char *>(event->orig_ptr));
      bytes_to_send = event->size;
      while (bytes_to_send > 0) {
        MPI_Isend(&host_byte_ptr[event->size - bytes_to_send],
                  static_cast<int>(
                      std::min(bytes_to_send, config::mpi_fragment_size)),
                  MPI_BYTE, dest_rank, mpi_tag, target_comm,
                  getNextRequest(dest_rank));
        bytes_to_send -= config::mpi_fragment_size;
      }
    }
  }

  // Event completion notification
  MPI_Irecv(nullptr, 0, MPI_BYTE, dest_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

void PackedSubmitEvent::unpackEvents() {
  if (event_location == EventLocation::DEST) {
    // Unpack received data
    int position = 0;
    for (auto item : packed_events_metadata) {
      if (item.size <= config::packing_threshold) {
        MPI_Unpack(&packed_buffer[0], packed_buffer.size(), &position,
                   (void *)item.dest_ptr, item.size, MPI_BYTE, target_comm);
      }
    }

    // Event completion notification
    MPI_Send(nullptr, 0, MPI_BYTE, orig_rank, mpi_tag, target_comm);
  }
}

PackedSubmitEvent::PackedSubmitEvent(int mpi_tag, MPI_Comm target_comm,
                                     int orig_rank, std::vector<int> dest_ranks)
    : PackedEvent(EventLocation::DEST, EventType::PACKED_SUBMIT, mpi_tag,
                  target_comm, orig_rank, dest_ranks, false /*is_packable*/) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool PackedSubmitEvent::runDestination() {
  int rcvd_size = 0;
  MPI_Status status;
  int64_t packed_buffer_size = 0;
  char *target_byte_ptr = nullptr;
  int64_t bytes_to_receive = 0;

  EVENT_BEGIN();

  // Probe incoming metadata array size
  do {
    MPI_Iprobe(orig_rank, mpi_tag, target_comm, &has_received, &status);

    if (has_received) {
      break;
    }

    EVENT_PAUSE();
  } while (!has_received);

  MPI_Get_count(&status, MPI_PACKED_SUBMIT_METADATA_T, &rcvd_size);
  DPES("Packing event probed %d items\n", rcvd_size);

  // Allocate metada array
  packed_events_metadata.resize(rcvd_size);

  MPI_Irecv(&packed_events_metadata[0], rcvd_size, MPI_PACKED_SUBMIT_METADATA_T,
            orig_rank, mpi_tag, target_comm, getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  // Get packed buffer size
  packed_buffer_size = 0;
  for (auto item : packed_events_metadata) {
    if (item.size <= config::packing_threshold) {
      packed_buffer_size += item.size;
    }
  }

  DPES("Packing event expects packed buffer of %ld bytes\n",
       packed_buffer_size);

  // Get packed data on buffer
  assertm(packed_buffer_size >= 0, "Buffer size cannot be a negative number.");
  packed_buffer.resize(packed_buffer_size);

  target_byte_ptr = reinterpret_cast<char *>(&packed_buffer[0]);
  bytes_to_receive = packed_buffer.size();
  while (bytes_to_receive > 0) {
    MPI_Irecv(&target_byte_ptr[packed_buffer.size() - bytes_to_receive],
              std::min(bytes_to_receive, config::mpi_fragment_size), MPI_PACKED,
              orig_rank, mpi_tag, target_comm, getNextRequest(orig_rank));
    bytes_to_receive -= config::mpi_fragment_size;
  }

  DPES("Packing event received data buffer\n");

  // Loop through non-packed data and receive it
  for (auto item : packed_events_metadata) {
    if (item.size > config::packing_threshold) {
      target_byte_ptr =
          const_cast<char *>(reinterpret_cast<const char *>(item.dest_ptr));
      bytes_to_receive = item.size;
      while (bytes_to_receive > 0) {
        MPI_Irecv(&target_byte_ptr[item.size - bytes_to_receive],
                  static_cast<int>(
                      std::min(bytes_to_receive, config::mpi_fragment_size)),
                  MPI_BYTE, orig_rank, mpi_tag, target_comm,
                  getNextRequest(orig_rank));
        bytes_to_receive -= config::mpi_fragment_size;
      }
    }
  }

  EVENT_PAUSE_FOR_REQUESTS();

  unpackEvents();

  EVENT_END();
}

// Packed Retrieve Event implementation
// =============================================================================
PackedRetrieveEvent::PackedRetrieveEvent(int mpi_tag, MPI_Comm target_comm,
                                         int orig_rank,
                                         std::vector<int> dest_ranks,
                                         EventPtr event)
    : PackedEvent(EventLocation::ORIG, EventType::PACKED_RETRIEVE, mpi_tag,
                  target_comm, orig_rank, dest_ranks, false /*is_packable*/) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
  packEvent(event);
}

bool PackedRetrieveEvent::runOrigin() {
  int64_t packed_buffer_size = 0;
  std::shared_ptr<RetrieveEvent> event;
  char *host_byte_ptr = nullptr;
  int64_t bytes_to_receive = 0;

  EVENT_BEGIN();

  DPES("Packing %ld events\n", packed_events.size());

  // Build metadata array
  packed_events_metadata.reserve(packed_events.size());
  packed_buffer_size = 0;

  for (auto base_event : packed_events) {
    event = std::static_pointer_cast<RetrieveEvent>(base_event);
    packed_events_metadata.push_back({event->size, event->dest_ptr});

    // Calculate packed buffer size
    if (event->size <= config::packing_threshold) {
      packed_buffer_size += event->size;
    }
  }

  // Send metadata array
  MPI_Isend(&packed_events_metadata[0], packed_events_metadata.size(),
            MPI_PACKED_RETRIEVE_METADATA_T, dest_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  DPES("Packed buffer size: %ld bytes\n", packed_buffer_size);

  // Get packed data buffer
  assertm(packed_buffer_size >= 0, "Buffer size cannot be a negative number.");
  packed_buffer.resize(packed_buffer_size);

  host_byte_ptr = reinterpret_cast<char *>(&packed_buffer[0]);
  bytes_to_receive = packed_buffer.size();
  while (bytes_to_receive > 0) {
    MPI_Irecv(
        &host_byte_ptr[packed_buffer.size() - bytes_to_receive],
        static_cast<int>(std::min(bytes_to_receive, config::mpi_fragment_size)),
        MPI_PACKED, dest_rank, mpi_tag, target_comm, getNextRequest(dest_rank));
    bytes_to_receive -= config::mpi_fragment_size;
  }

  DPES("Packing event received data buffer\n");

  // loop through non-packed data and receive it
  for (auto base_event : packed_events) {
    event = std::static_pointer_cast<RetrieveEvent>(base_event);
    if (event->size > config::packing_threshold) {
      host_byte_ptr = reinterpret_cast<char *>(event->orig_ptr);
      assertm(event->size > 0, "Event buffer size must be greater than zero");
      bytes_to_receive = event->size;
      while (bytes_to_receive > 0) {
        MPI_Irecv(&host_byte_ptr[event->size - bytes_to_receive],
                  static_cast<int>(
                      std::min(bytes_to_receive, config::mpi_fragment_size)),
                  MPI_BYTE, dest_rank, mpi_tag, target_comm,
                  getNextRequest(dest_rank));
        bytes_to_receive -= config::mpi_fragment_size;
      }
      DPES("Packing event received non-packed buffer\n");
    }
  }

  EVENT_PAUSE_FOR_REQUESTS();

  unpackEvents();

  EVENT_END();
}

// receive the data after the meta-data has been unpacked
void PackedRetrieveEvent::unpackEvents() {
  // only post-process unpacking in ORIG location
  if (event_location == EventLocation::ORIG) {
    //  Unpack received data
    for (auto base_event : packed_events) {
      auto event = std::static_pointer_cast<RetrieveEvent>(base_event);
      if (event->size <= config::packing_threshold) {
        int position = 0;
        MPI_Unpack(&packed_buffer[0], packed_buffer.size(), &position,
                   (void *)event->orig_ptr, event->size, MPI_BYTE, target_comm);
      }
    }

    // Event completion notification
    MPI_Recv(nullptr, 0, MPI_BYTE, dest_rank, mpi_tag, target_comm,
             MPI_STATUS_IGNORE);
  }
  if (event_location == EventLocation::DEST) {
    // Event completion notification
    MPI_Send(nullptr, 0, MPI_BYTE, orig_rank, mpi_tag, target_comm);
  }
}

PackedRetrieveEvent::PackedRetrieveEvent(int mpi_tag, MPI_Comm target_comm,
                                         int orig_rank,
                                         std::vector<int> dest_ranks)
    : PackedEvent(EventLocation::DEST, EventType::PACKED_RETRIEVE, mpi_tag,
                  target_comm, orig_rank, dest_ranks, false /*is_packable*/) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool PackedRetrieveEvent::runDestination() {
  int rcvd_size = 0;
  MPI_Status status;
  int64_t packed_buffer_size = 0;
  char *target_byte_ptr = nullptr;
  int64_t bytes_to_send = 0;
  int position = 0;

  EVENT_BEGIN();

  // Probe incoming metadata array size
  do {
    MPI_Iprobe(orig_rank, mpi_tag, target_comm, &has_received, &status);

    if (has_received) {
      break;
    }

    EVENT_PAUSE();
  } while (!has_received);

  MPI_Get_count(&status, MPI_PACKED_RETRIEVE_METADATA_T, &rcvd_size);
  DPES("Packing event probed %d items\n", rcvd_size);

  // Allocate metada array
  packed_events_metadata.resize(rcvd_size);

  MPI_Irecv(&packed_events_metadata[0], rcvd_size,
            MPI_PACKED_RETRIEVE_METADATA_T, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  // Calculate packed buffer size
  packed_buffer_size = 0;
  for (auto item : packed_events_metadata) {
    if (item.size <= config::packing_threshold) {
      packed_buffer_size += item.size;
    }
  }

  DPES("Packing event expects metadata buffer of %ld bytes\n",
       packed_buffer_size);

  if (packed_buffer_size > 0) {
    // Pack the data on a contiguous buffer
    packed_buffer.resize(packed_buffer_size);
    position = 0;

    for (auto send_info : packed_events_metadata) {
      if (send_info.size <= config::packing_threshold) {
        MPI_Pack(send_info.dest_ptr, send_info.size, MPI_BYTE,
                 &packed_buffer[0], packed_buffer.size(), &position,
                 target_comm);
      }
    }
    assertm(position == packed_buffer.size(),
            "Buffer was not correctly written");

    // Send packed data
    target_byte_ptr = reinterpret_cast<char *>(&packed_buffer[0]);
    bytes_to_send = packed_buffer.size();
    while (bytes_to_send > 0) {
      MPI_Isend(
          &target_byte_ptr[packed_buffer.size() - bytes_to_send],
          static_cast<int>(std::min(bytes_to_send, config::mpi_fragment_size)),
          MPI_PACKED, orig_rank, mpi_tag, target_comm,
          getNextRequest(orig_rank));
      bytes_to_send -= config::mpi_fragment_size;
    }

    DPES("Packing event sent packed buffer\n");
  }

  // Loop through non-packed data and send it using separeted messages
  for (auto send_info : packed_events_metadata) {
    if (send_info.size > config::packing_threshold) {
      DPES("Packing event sent non-packed buffer\n");

      target_byte_ptr = const_cast<char *>(
          reinterpret_cast<const char *>(send_info.dest_ptr));
      bytes_to_send = send_info.size;
      while (bytes_to_send > 0) {
        MPI_Isend(&target_byte_ptr[send_info.size - bytes_to_send],
                  static_cast<int>(
                      std::min(bytes_to_send, config::mpi_fragment_size)),
                  MPI_BYTE, orig_rank, mpi_tag, target_comm,
                  getNextRequest(orig_rank));
        bytes_to_send -= config::mpi_fragment_size;
      }
    }
  }

  EVENT_PAUSE_FOR_REQUESTS();

  unpackEvents();

  EVENT_END();
}

// Packed Exchange Event implementation
// Temporarily disabled. See issue #XX
// =============================================================================
PackedExchangeEvent::PackedExchangeEvent(int mpi_tag, MPI_Comm target_comm,
                                         int orig_rank,
                                         std::vector<int> dest_ranks,
                                         EventPtr event)
    : PackedEvent(EventLocation::ORIG, EventType::PACKED_EXCHANGE, mpi_tag,
                  target_comm, orig_rank, dest_ranks, false /*is_packable*/) {
  assertm(dest_ranks.size() == 2,
          "Exchange event must have exactly two destination ranks");
  packEvent(event);
}

bool PackedExchangeEvent::runOrigin() {
  int64_t metadata_buffer_size = 0;
  int position = 0;
  auto front_ev =
      std::static_pointer_cast<ExchangeEvent>(packed_events.front());

  EVENT_BEGIN();
  assertm(packed_events.size() >= 1,
          "Must have at least one event in packed list");
  metadata_buffer_size = sizeof(int) + // number of events
                         sizeof(int) + // data_dst_rank
                         sizeof(int);  // data_src_rank

  metadata_buffer_size +=
      packed_events.size() * (sizeof(uintptr_t) + // data_src_ptr
                              sizeof(uintptr_t) + // data_dst_ptr
                              sizeof(int64_t)     // buffer size
                             );

  // Allocate metadata buffer
  metadata_buffer.resize(metadata_buffer_size);
  position = 0;
  num_events = packed_events.size();

  MPI_Pack(&num_events, 1, MPI_INT, metadata_buffer.data(),
           metadata_buffer.size(), &position, target_comm);
  MPI_Pack(&front_ev->data_dst_rank, 1, MPI_INT, metadata_buffer.data(),
           metadata_buffer.size(), &position, target_comm);
  MPI_Pack(&front_ev->data_src_rank, 1, MPI_INT, metadata_buffer.data(),
           metadata_buffer.size(), &position, target_comm);

  for (auto base_ev : packed_events) {
    auto ev = std::static_pointer_cast<ExchangeEvent>(base_ev);

    MPI_Pack(&ev->src_ptr, sizeof(uintptr_t), MPI_BYTE, metadata_buffer.data(),
             metadata_buffer.size(), &position, target_comm);
    MPI_Pack(&ev->dst_ptr, sizeof(uintptr_t), MPI_BYTE, metadata_buffer.data(),
             metadata_buffer.size(), &position, target_comm);
    MPI_Pack(&ev->size, 1, MPI_INT64_T, metadata_buffer.data(),
             metadata_buffer.size(), &position, target_comm);
  }
  assertm(position == metadata_buffer.size(),
          "Metadata buffer size does not match position");

  for (int dest_rank : dest_ranks) {
    MPI_Isend(&metadata_buffer_size, 1, MPI_INT64_T, dest_rank, mpi_tag,
              target_comm, getNextRequest(dest_rank));
    MPI_Isend(metadata_buffer.data(), metadata_buffer.size(), MPI_PACKED,
              dest_rank, mpi_tag, target_comm, getNextRequest(dest_rank));
  }

  // Event completion notifications
  for (int dest_rank : dest_ranks) {
    MPI_Irecv(nullptr, 0, MPI_BYTE, dest_rank, mpi_tag, target_comm,
              getNextRequest(dest_rank));
  }

  EVENT_END();
}

PackedExchangeEvent::PackedExchangeEvent(int mpi_tag, MPI_Comm target_comm,
                                         int orig_rank,
                                         std::vector<int> dest_ranks)
    : PackedEvent(EventLocation::DEST, EventType::PACKED_EXCHANGE, mpi_tag,
                  target_comm, orig_rank, dest_ranks, false /*is_packable*/) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool PackedExchangeEvent::runDestination() {
  int64_t metadata_buffer_size = 0;
  int position = 0;
  const char *host_byte_ptr;
  int64_t bytes_to_send = 0;
  char *target_byte_ptr = nullptr;
  int64_t bytes_to_receive = 0;
  int64_t curr_buffer_size = 0;
  char *curr_buffer_src_ptr = nullptr;
  char *curr_buffer_dst_ptr = nullptr;

  EVENT_BEGIN();

  MPI_Irecv(&metadata_buffer_size, 1, MPI_INT64_T, orig_rank, mpi_tag,
            target_comm, getNextRequest(orig_rank));
  EVENT_PAUSE_FOR_REQUESTS();

  metadata_buffer.resize(metadata_buffer_size);
  MPI_Irecv(metadata_buffer.data(), metadata_buffer.size(), MPI_PACKED,
            orig_rank, mpi_tag, target_comm, getNextRequest(orig_rank));

  EVENT_PAUSE_FOR_REQUESTS();
  // Unpack metadata
  position = 0;
  MPI_Unpack(metadata_buffer.data(), metadata_buffer.size(), &position,
             &num_events, 1, MPI_INT, target_comm);
  MPI_Unpack(metadata_buffer.data(), metadata_buffer.size(), &position,
             &data_dst_rank, 1, MPI_INT, target_comm);
  MPI_Unpack(metadata_buffer.data(), metadata_buffer.size(), &position,
             &data_src_rank, 1, MPI_INT, target_comm);

  while (position != metadata_buffer.size()) {
    MPI_Unpack(metadata_buffer.data(), metadata_buffer.size(), &position,
               &curr_buffer_src_ptr, sizeof(uintptr_t), MPI_BYTE, target_comm);
    MPI_Unpack(metadata_buffer.data(), metadata_buffer.size(), &position,
               &curr_buffer_dst_ptr, sizeof(uintptr_t), MPI_BYTE, target_comm);
    MPI_Unpack(metadata_buffer.data(), metadata_buffer.size(), &position,
               &curr_buffer_size, 1, MPI_INT64_T, target_comm);

    if (dest_rank == data_src_rank) {
      // Send data to destination rank
      host_byte_ptr = reinterpret_cast<const char *>(curr_buffer_src_ptr);
      bytes_to_send = curr_buffer_size;
      while (bytes_to_send > 0) {
        MPI_Isend(&host_byte_ptr[curr_buffer_size - bytes_to_send],
                  static_cast<int>(
                      std::min(bytes_to_send, config::mpi_fragment_size)),
                  MPI_BYTE, data_dst_rank, mpi_tag, target_comm,
                  getNextRequest(dest_rank));
        bytes_to_send -= config::mpi_fragment_size;
      }

    } else if (dest_rank == data_dst_rank) {
      // Receive data from source rank
      target_byte_ptr = reinterpret_cast<char *>(curr_buffer_dst_ptr);
      bytes_to_receive = curr_buffer_size;
      while (bytes_to_receive > 0) {
        MPI_Irecv(&target_byte_ptr[curr_buffer_size - bytes_to_receive],
                  static_cast<int>(
                      std::min(bytes_to_receive, config::mpi_fragment_size)),
                  MPI_BYTE, data_src_rank, mpi_tag, target_comm,
                  getNextRequest(dest_rank));
        bytes_to_receive -= config::mpi_fragment_size;
      }
    }
  }
  assertm(position == metadata_buffer.size(),
          "Metadata buffer size does not match position");

  // Event completion notification
  MPI_Isend(nullptr, 0, MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

// Execute Event implementation
// =============================================================================
ExecuteEvent::ExecuteEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                           std::vector<int> dest_ranks, int32_t arg_num,
                           void **arg_ptrs_p, uint32_t target_entry_idx)
    : BaseEvent(EventLocation::ORIG, EventType::EXECUTE, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is_packable*/),
      arg_num(arg_num), arg_ptrs(arg_num, nullptr),
      target_entry_idx(target_entry_idx) {
  assertm(arg_num >= 0, "ExecuteEvent must receive an arg_num >= 0");
  assertm(arg_num == 0 || arg_ptrs_p != nullptr,
          "ExecuteEvent must receive a valid arg_ptrs_p when arg_num > 0");
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();

  std::copy_n(arg_ptrs_p, arg_num, arg_ptrs.begin());
}

bool ExecuteEvent::runOrigin() {
  assert(event_location == EventLocation::ORIG);

  EVENT_BEGIN();

  MPI_Isend(&arg_num, sizeof(int32_t), MPI_BYTE, dest_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  MPI_Isend(arg_ptrs.data(), arg_num * sizeof(uintptr_t), MPI_BYTE, dest_rank,
            mpi_tag, target_comm, getNextRequest(dest_rank));

  MPI_Isend(&target_entry_idx, sizeof(uint32_t), MPI_BYTE, dest_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  // Event completion notification
  MPI_Irecv(nullptr, 0, MPI_BYTE, dest_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

ExecuteEvent::ExecuteEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                           std::vector<int> dest_ranks)
    : BaseEvent(EventLocation::DEST, EventType::EXECUTE, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is_packable*/) {
  assertm(EventSystem::device_info != nullptr,
          "ExecuteEvent must receive a valid pointer as device_info");
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool ExecuteEvent::runDestination() {
  assert(event_location == EventLocation::DEST);

  __tgt_target_table *TargetTable = nullptr;
  __tgt_offload_entry *begin = nullptr;
  __tgt_offload_entry *end = nullptr;
  __tgt_offload_entry *cur = nullptr;
  void *tgt_entry_ptr = nullptr;
  ffi_cif cif;
  std::vector<ffi_type *> args_types;
  std::vector<void *> args;
  ffi_status status [[maybe_unused]];
  void (*entry)(void) = nullptr;

  EVENT_BEGIN();

  MPI_Irecv(&arg_num, sizeof(int32_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  arg_ptrs.resize(arg_num, nullptr);
  MPI_Irecv(arg_ptrs.data(), arg_num * sizeof(uintptr_t), MPI_BYTE, orig_rank,
            mpi_tag, target_comm, getNextRequest(dest_rank));

  MPI_Irecv(&target_entry_idx, sizeof(uint32_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  // Gets the translation table (which contains all the good info).
  TargetTable = EventSystem::device_info->getOffloadEntriesTableOnDevice();
  // Iterates over all the host table entries to see if we can locate the
  // host_ptr.
  begin = TargetTable->EntriesBegin;
  end = TargetTable->EntriesEnd;
  cur = begin;

  tgt_entry_ptr = NULL;

  DPES("Look for the entry %d\n", target_entry_idx);
  // Iterates over all the table entries to see if we can locate the entry.
  for (uint32_t i = 0; cur < end; ++cur, ++i) {
    if (i == target_entry_idx) {
      // We got a match, now fill the HostPtrToTableMap so that we may avoid
      // this search next time.
      DPES("Entry found\n");
      tgt_entry_ptr = cur->addr;
      break;
    }
  }

  assertm(cur != end, "Could not find the right entry");

  // All args are references.
  args_types.resize(arg_num, &ffi_type_pointer);
  args.resize(arg_num);

  for (int32_t i = 0; i < arg_num; ++i) {
    args[i] = &arg_ptrs[i];
  }

  status = ffi_prep_cif(&cif, FFI_DEFAULT_ABI, arg_num, &ffi_type_void,
                        &args_types[0]);

  assertm(status == FFI_OK, "Unable to prepare target launch!");

  DPES("Running kernel called %s...\n", cur->name);
  DPES("Running entry point at " DPxMOD "...\n", DPxPTR(tgt_entry_ptr));

  *((void **)&entry) = tgt_entry_ptr;
  {
    PROF_SCOPED(PROF_LVL_USER,
                std::string(toString(event_type)) + " / Target Execution",
                [&]() -> std::string {
                  std::stringstream details;
                  details << "{";
                  details << "\"mpi_tag\":" << mpi_tag << ",";
                  details << "\"target_region_index\":" << target_entry_idx;
                  details << "}";
                  return details.str();
                }());
    ffi_call(&cif, entry, NULL, &args[0]);
  }

  DPES("Execution DONE...\n");

  // Event completion notification
  MPI_Isend(nullptr, 0, MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

// Bcast Event implementation
// =============================================================================
void BcastEvent::addTgtPtr(int device_rank, void *tgt_ptr) {
  tgt_ptrs[device_rank] = tgt_ptr;
}

BcastEvent::BcastEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                       std::vector<int> dest_ranks, const void *host_ptr,
                       void *dest_ptr, int64_t size)
    : BaseEvent(EventLocation::ORIG, EventType::BCAST, mpi_tag, target_comm,
                orig_rank, dest_ranks, true /*is_packable*/),
      host_ptr(host_ptr), dest_ptr(dest_ptr), size(size) {
  assertm(size >= 0, "BcastEvent must receive a size >= 0");
  assertm(host_ptr != nullptr,
          "BcastEvent must receive a valid pointer as host_ptr");
  assertm(dest_ptr != nullptr,
          "BcastEvent must receive a valid pointer as dest_ptr");
  dest_rank = dest_ranks.front();

  int world_size = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  tgt_ptrs.resize(world_size);

  assertm(
      dest_ranks.size() == world_size - 1,
      "BcastEvent must have all processes as destination, except the root.");
}

bool BcastEvent::runOrigin() {
  assert(event_location == EventLocation::ORIG);
  char *host_byte_ptr = nullptr;
  int64_t bytes_to_send = 0;

  EVENT_BEGIN();

  for (auto dest_rank : dest_ranks) {
    MPI_Isend(&tgt_ptrs[dest_rank], sizeof(uintptr_t), MPI_BYTE, dest_rank,
              mpi_tag, target_comm, getNextRequest(dest_rank));

    MPI_Isend(&size, sizeof(int64_t), MPI_BYTE, dest_rank, mpi_tag, target_comm,
              getNextRequest(dest_rank));
  }

  // Operates over many fragments of the original buffer of at most
  // mpi_fragment_size bytes.
  host_byte_ptr = const_cast<char *>(reinterpret_cast<const char *>(host_ptr));
  bytes_to_send = size;
  while (bytes_to_send > 0) {
    MPI_Ibcast(
        &host_byte_ptr[size - bytes_to_send],
        static_cast<int>(std::min(bytes_to_send, config::mpi_fragment_size)),
        MPI_BYTE, orig_rank, EventSystem::collective_comm,
        getNextRequest(ft::FT_MPI_COLLECTIVE));
    bytes_to_send -= config::mpi_fragment_size;
  }

  // Prevent error from mixing with completing requests
  EVENT_PAUSE_FOR_REQUESTS();

  for (auto dest_rank : dest_ranks) {
    MPI_Irecv(nullptr, 0, MPI_BYTE, dest_rank, mpi_tag, target_comm,
              getNextRequest(dest_rank));
  }

  DPES("Host broadcasted buffer with %" PRId64 " bytes\n", size);

  EVENT_END();
}

BcastEvent::BcastEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                       std::vector<int> dest_ranks)
    : BaseEvent(EventLocation::DEST, EventType::BCAST, mpi_tag, target_comm,
                orig_rank, dest_ranks, true /*is_packable*/) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool BcastEvent::runDestination() {
  assert(event_location == EventLocation::DEST);

  char *target_byte_ptr;
  int64_t bytes_to_receive;

  EVENT_BEGIN();

  MPI_Irecv(&dest_ptr, sizeof(uintptr_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  MPI_Irecv(&size, sizeof(int64_t), MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  DPES("Receiving %ld byte to store at " DPxMOD "!\n", size, DPxPTR(dest_ptr));

  // Operates over many fragments of the original buffer of at most
  // mpi_fragment_size bytes.
  target_byte_ptr = reinterpret_cast<char *>(dest_ptr);
  bytes_to_receive = size;
  while (bytes_to_receive > 0) {
    // MPI_Ibcast must be used to match the MPI_Ibcast call used on the origin
    // side of this event.
    MPI_Ibcast(
        &target_byte_ptr[size - bytes_to_receive],
        static_cast<int>(std::min(bytes_to_receive, config::mpi_fragment_size)),
        MPI_BYTE, orig_rank, EventSystem::collective_comm,
        getNextRequest(ft::FT_MPI_COLLECTIVE));

    bytes_to_receive -= config::mpi_fragment_size;
  }

  EVENT_PAUSE_FOR_REQUESTS();

  DPES("Data received from broadcast\n");

  // Event completion notification
  MPI_Isend(nullptr, 0, MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

void DynBcastEvent::addTgtPtr(int device_rank, void *tgt_ptr) {
  assertm(tgt_ptr, "Cannot register nullptr");
  assertm(device_rank != orig_rank, "Cannot register current orig_rank");
  assertm(device_rank < tgt_ptrs.size(), "Got device_rank out of range");
  assertm(!tgt_ptrs[device_rank], "Cannot overwrite registered pointer");

  tgt_ptrs[device_rank] = tgt_ptr;
}

bool DynBcastEvent::isBcastData(void *test_ptr, int test_size) {
  if (test_ptr == host_ptr && test_size == size) {
    return true;
  }
  return false;
}

void DynBcastEvent::scheduleBcast() {
  assertm(scheduling_ready == false && fwd_list.size() == 0,
          "Cannot change bcast schedule");

  // tgt_ptrs has a position for each rank's buffer
  int world_size = tgt_ptrs.size();

  // Need two times the world size: a slot for each process plus a separator.
  fwd_list.resize(world_size * 2);

  // Check filled entries in tgt_ptrs. In the case of a sub-broadcast, only a
  // subset of the ranks will be used
  std::vector<int> used_ranks;
  used_ranks.reserve(tgt_ptrs.size());
  for (int i = 0; i < tgt_ptrs.size(); i++) {
    // Keep the number of the ranks that are participating in this broadcast
    if (tgt_ptrs[i] != nullptr) {
      used_ranks.push_back(i);
    }
  }

  assertm(used_ranks[0] == orig_rank,
          "Implementation assumes that orig_rank during scheduling is the "
          "broadcast root and is the first item in the used_ranks list");

  // To schedule a binary tree broadcast, each rank must forward the buffer to
  // two other ranks. fwd_list represents this as the rank numbers separated
  // by a marker. E.g., if the marker is -1, fwd_list = [1,2,-1,3,4,-1] means
  // that rank 0 will forward the buffer to 1 and 2 and rank 1 will forward the
  // buffer to 3 and 4.
  const int children_per_node = 2; // Binary tree
  int sched_rank_counter = 1; // Starts at 1 since we do not need to forward the
                              // data to the broadcast root
  int i = 0;
  for (int rank = 0; rank < world_size; rank++) {
    // Check if current rank is participating in this broadcast
    bool rank_in_used_ranks = std::find(used_ranks.begin(), used_ranks.end(),
                                        rank) != used_ranks.end();

    // Each rank in used_ranks vector must forward the data to two other ranks
    if (rank_in_used_ranks) {
      int n_children = 0;
      while (sched_rank_counter < used_ranks.size() &&
             n_children < children_per_node) {
        fwd_list[i++] = used_ranks[sched_rank_counter];
        sched_rank_counter++;
        n_children++;
      }
    }
    fwd_list[i++] = slice_marker;
  }

  // Fill the remainder of fwd_list with markers
  while (i < fwd_list.size()) {
    fwd_list[i++] = slice_marker;
  }

  scheduling_ready = true;
  printBcastSchedule();
}

void DynBcastEvent::printBcastSchedule() {
  assertm(fwd_list.size() != 0, "Cannot print non-existent bcast schedule");

  int i = 0;
  int sched_rank = 0;

  DPES("[DynBcast] From rank %d to:\n", sched_rank++);
  while (i < fwd_list.size()) {
    if (fwd_list[i] == slice_marker) {
      DPES("[DynBcast] From rank %d to:\n", sched_rank++);
    } else {
      DPES("[DynBcast]          -> %d\n", fwd_list[i]);
    }
    i++;
  }
}

const int DynBcastEvent::slice_marker;
std::vector<int> DynBcastEvent::getSendList(std::vector<int> fwd_list,
                                            int from_rank) {
  assertm(fwd_list.capacity() > 0, "Cannot get list before scheduling");
  std::vector<int>::iterator slice_begin = fwd_list.begin();
  std::vector<int>::iterator slice_end = fwd_list.end();

  int occurrence = 0;

  while (occurrence++ != from_rank) {
    slice_begin = std::find(slice_begin, fwd_list.end(), slice_marker) + 1;
  }

  // skip the slice marker
  if (*slice_begin == slice_marker) {
    slice_begin++;
  }

  if (slice_begin != fwd_list.end()) {
    slice_end = std::find(slice_begin, fwd_list.end(), slice_marker);
  }

  std::vector<int> vec(slice_begin, slice_end);

  assertm(!(std::find(vec.begin(), vec.end(), from_rank) != vec.end()),
          "Rank cannot forward data to itself");

  return vec;
}

DynBcastEvent::DynBcastEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                             std::vector<int> dest_ranks, void *host_ptr,
                             void *dest_ptr, int64_t size)
    : BaseEvent(EventLocation::ORIG, EventType::DYNBCAST, mpi_tag, target_comm,
                orig_rank, dest_ranks, true /* is packable*/),
      host_ptr(host_ptr), dest_ptr(dest_ptr), size(size) {
  assertm(size >= 0, "BcastEvent must receive a size >= 0");
  assertm(host_ptr != nullptr,
          "BcastEvent must receive a valid pointer as host_ptr");
  assertm(dest_ptr != nullptr,
          "BcastEvent must receive a valid pointer as dest_ptr");
  assertm(dest_ranks.size() >= 1,
          "Event must have at least one destination rank");
  dest_rank = dest_ranks.front();

  // Get world size
  int world_size = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // Allocate space to hold every device pointer
  tgt_ptrs.resize(world_size);
  dest_ranks.reserve(world_size);

  // Register host pointer to list of addresses
  tgt_ptrs[orig_rank] = (void *)host_ptr;
}

bool DynBcastEvent::runOrigin() {
  assert(event_location == EventLocation::ORIG);

  int64_t fwd_list_size = 0;
  char *host_byte_ptr = nullptr;
  int64_t bytes_to_send = 0;
  EventPtr remote_dynbcast;

  EVENT_BEGIN();

  for (auto dest_rank : dest_ranks) {
    MPI_Isend(&size, sizeof(int64_t), MPI_BYTE, dest_rank, mpi_tag, target_comm,
              getNextRequest(dest_rank));

    MPI_Isend(&tgt_ptrs[0], tgt_ptrs.size() * sizeof(uintptr_t), MPI_BYTE,
              dest_rank, mpi_tag, target_comm, getNextRequest(dest_rank));

    fwd_list_size = fwd_list.size();
    assertm(fwd_list_size != 0, "Cannot use uninitialized fwd_list");
    MPI_Isend(&fwd_list_size, sizeof(int64_t), MPI_BYTE, dest_rank, mpi_tag,
              target_comm, getNextRequest(dest_rank));

    MPI_Isend(&fwd_list[0], fwd_list.size() * sizeof(int), MPI_BYTE, dest_rank,
              mpi_tag, target_comm, getNextRequest(dest_rank));

    // Operates over many fragments of the original buffer of at most
    // mpi_fragment_size bytes.
    host_byte_ptr =
        const_cast<char *>(reinterpret_cast<const char *>(host_ptr));
    bytes_to_send = size;
    while (bytes_to_send > 0) {
      MPI_Isend(
          &host_byte_ptr[size - bytes_to_send],
          static_cast<int>(std::min(bytes_to_send, config::mpi_fragment_size)),
          MPI_BYTE, dest_rank, mpi_tag, target_comm, getNextRequest(dest_rank));
      bytes_to_send -= config::mpi_fragment_size;
    }
  }

  // If root, wait for completion
  if (orig_rank == 0 /*bcast root*/) {
    for (auto rank : fwd_list) {
      MPI_Irecv(nullptr, 0, MPI_BYTE, rank, mpi_tag, target_comm,
                getNextRequest(rank));
    }
  }

  EVENT_END();
}

DynBcastEvent::DynBcastEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                             std::vector<int> dest_ranks)
    : BaseEvent(EventLocation::DEST, EventType::DYNBCAST, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is packable*/),
      host_ptr(nullptr), dest_ptr(nullptr), size(0) {

  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();

  int world_size = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  tgt_ptrs.resize(world_size);
}

bool DynBcastEvent::runDestination() {
  assert(event_location == EventLocation::DEST);

  char *target_byte_ptr = nullptr;
  int64_t bytes_to_receive = 0;
  EventPtr remote_dynbcast;
  std::shared_ptr<event_system::DynBcastEvent> ev;
  std::vector<int> fwd_ranks;

  EVENT_BEGIN();
  MPI_Irecv(&size, sizeof(int64_t), MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  MPI_Irecv(&tgt_ptrs[0], tgt_ptrs.size() * sizeof(uintptr_t), MPI_BYTE,
            orig_rank, mpi_tag, target_comm, getNextRequest(dest_rank));

  MPI_Irecv(&fwd_list_size, sizeof(int64_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  // Wait for parameters to be received.
  EVENT_PAUSE_FOR_REQUESTS();

  dest_ptr = tgt_ptrs[dest_rank];

  fwd_list.resize(fwd_list_size);

  MPI_Irecv(&fwd_list[0], fwd_list.size() * sizeof(int), MPI_BYTE, orig_rank,
            mpi_tag, target_comm, getNextRequest(dest_rank));

  // Wait for forwarding list to be received.
  EVENT_PAUSE_FOR_REQUESTS();

  printBcastSchedule();

  DPES("Receiving %ld byte to store at " DPxMOD "!\n", size, DPxPTR(dest_ptr));

  // Operates over many fragments of the original buffer of at most
  // mpi_fragment_size bytes.
  target_byte_ptr = reinterpret_cast<char *>(dest_ptr);
  bytes_to_receive = size;
  while (bytes_to_receive > 0) {
    MPI_Irecv(
        &target_byte_ptr[size - bytes_to_receive],
        static_cast<int>(std::min(bytes_to_receive, config::mpi_fragment_size)),
        MPI_BYTE, orig_rank, mpi_tag, target_comm, getNextRequest(dest_rank));
    bytes_to_receive -= config::mpi_fragment_size;
  }

  // Wait for buffer to be received.
  EVENT_PAUSE_FOR_REQUESTS();

  // Forward the broadcast to destination ranks in forward list, if any
  fwd_ranks = getSendList(fwd_list, dest_rank);

  if (fwd_ranks.size() > 0) {
    remote_dynbcast = std::make_shared<DynBcastEvent>(
        mpi_tag, target_comm, dest_rank, fwd_ranks, dest_ptr,
        tgt_ptrs[0] /*refactor this out*/, size);

    // Copy tgt pointer list to new DynBcastEvent
    ev = std::static_pointer_cast<event_system::DynBcastEvent>(remote_dynbcast);
    // Deep-copy std::vector
    ev->fwd_list = fwd_list;
    ev->tgt_ptrs = tgt_ptrs;

    remote_dynbcast->progress();
    forwarded_bcasts.push_back(ev);
  }

  // Send completion notification to broadcast root
  MPI_Isend(nullptr, 0, MPI_BYTE, 0 /* bcast root*/, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  DPES("Data received from broadcast\n");

  EVENT_END();
}

// RegisterCPPtrs Event implementation
// =============================================================================
RegisterCPPtrs::RegisterCPPtrs(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                               std::vector<int> dest_ranks,
                               uintptr_t *ptrs_to_register, int64_t *ptrs_sizes,
                               int32_t num_ptrs)
    : BaseEvent(EventLocation::ORIG, EventType::REGISTERCPPTRS, mpi_tag,
                target_comm, orig_rank, dest_ranks, false /*is_packable*/),
      ptrs(num_ptrs), sizes(num_ptrs) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();

  num = num_ptrs;
  std::copy_n(ptrs_to_register, num, ptrs.begin());
  std::copy_n(ptrs_sizes, num, sizes.begin());
}

bool RegisterCPPtrs::runOrigin() {
  assert(event_location == EventLocation::ORIG);

  EVENT_BEGIN();

  if (dest_rank == orig_rank) { // If saving on the host
    PROF_SCOPED(PROF_LVL_ALL,
                std::string(toString(event_type)) + " / Registering Buffers",
                [&]() -> std::string {
                  std::stringstream details;
                  details << "{";
                  details << "\"Number of ptrs\":" << num;
                  details << "}";
                  return details.str();
                }());
    if (ft_handler != nullptr) {
      int last_id;
      ft_handler->cpRegisterPointers(ptrs, sizes, sizeof(char), &last_id);
      assert(last_id != -1);
    }
  } else { // If in a worker node
    // First send the number of pointers to save
    MPI_Isend(&num, sizeof(int32_t), MPI_BYTE, dest_rank, mpi_tag, target_comm,
              getNextRequest(dest_rank));
    // Send the buffer pointers and buffer sizes
    MPI_Isend(ptrs.data(), num * sizeof(uintptr_t), MPI_BYTE, dest_rank,
              mpi_tag, target_comm, getNextRequest(dest_rank));
    MPI_Isend(sizes.data(), num * sizeof(int64_t), MPI_BYTE, dest_rank, mpi_tag,
              target_comm, getNextRequest(dest_rank));

    // Event completion notification
    MPI_Irecv(nullptr, 0, MPI_BYTE, dest_rank, mpi_tag, target_comm,
              getNextRequest(dest_rank));
  }

  EVENT_END();
}

RegisterCPPtrs::RegisterCPPtrs(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                               std::vector<int> dest_ranks)
    : BaseEvent(EventLocation::DEST, EventType::REGISTERCPPTRS, mpi_tag,
                target_comm, orig_rank, dest_ranks, false /*is_packable*/) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool RegisterCPPtrs::runDestination() {
  assert(event_location == EventLocation::DEST);

  EVENT_BEGIN();

  MPI_Irecv(&num, sizeof(int32_t), MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  ptrs.resize(num);
  sizes.resize(num);

  MPI_Irecv(ptrs.data(), num * sizeof(uintptr_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));
  MPI_Irecv(sizes.data(), num * sizeof(int64_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  {
    PROF_SCOPED(PROF_LVL_ALL,
                std::string(toString(event_type)) + " / Registering Buffers",
                [&]() -> std::string {
                  std::stringstream details;
                  details << "{";
                  details << "\"Number of ptrs\":" << num;
                  details << "}";
                  return details.str();
                }());
    if (ft_handler != nullptr) {
      int last_id;
      ft_handler->cpRegisterPointers(ptrs, sizes, sizeof(char), &last_id);
      assert(last_id != -1);
    }
  }
  // Event completion notification
  MPI_Isend(nullptr, 0, MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

// Checkpointing Event implementation
// =============================================================================
CheckpointEvent::CheckpointEvent(int mpi_tag, MPI_Comm target_comm,
                                 int orig_rank, std::vector<int> dest_ranks,
                                 int32_t *cp_version)
    : BaseEvent(EventLocation::ORIG, EventType::CHECKPOINT, mpi_tag,
                target_comm, orig_rank, dest_ranks, false /*is_packable*/) {

  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
  version = cp_version;
}

bool CheckpointEvent::runOrigin() {
  assert(event_location == EventLocation::ORIG);

  EVENT_BEGIN();

  if (orig_rank == dest_rank) { // host side of checkpointing
    PROF_SCOPED(PROF_LVL_ALL,
                std::string(toString(event_type)) + " / Saving Checkpoint");
    if (ft_handler != nullptr)
      *version = ft_handler->cpSaveCheckpoint(*version);
  } else {
    if (ft_handler->getProcessState(dest_rank) == ft::ProcessState::ALIVE) {
      MPI_Isend(version, sizeof(int32_t), MPI_BYTE, dest_rank, mpi_tag,
                target_comm, getNextRequest(dest_rank));
      MPI_Irecv(version, sizeof(int32_t), MPI_BYTE, dest_rank, mpi_tag,
                target_comm, getNextRequest(dest_rank));
    }
  }

  EVENT_END();
}

CheckpointEvent::CheckpointEvent(int mpi_tag, MPI_Comm target_comm,
                                 int orig_rank, std::vector<int> dest_ranks)
    : BaseEvent(EventLocation::DEST, EventType::CHECKPOINT, mpi_tag,
                target_comm, orig_rank, dest_ranks, false /*is_packable*/) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool CheckpointEvent::runDestination() {
  assert(event_location == EventLocation::DEST);

  EVENT_BEGIN();

  MPI_Irecv(&returned_version, sizeof(int32_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  {
    PROF_SCOPED(PROF_LVL_ALL,
                std::string(toString(event_type)) + " / Saving Checkpoint");
    if (ft_handler != nullptr)
      returned_version = ft_handler->cpSaveCheckpoint(returned_version);
  }

  MPI_Isend(&returned_version, sizeof(int32_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));

  EVENT_END();
}

// Recovery Event implementation
// =============================================================================
RecoveryEvent::RecoveryEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                             std::vector<int> dest_ranks, uintptr_t tgt_ptr,
                             int64_t size, int32_t id, int32_t rank,
                             int32_t ver)
    : BaseEvent(EventLocation::ORIG, EventType::RECOVERY, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is_packable*/),
      buffer_ptr(tgt_ptr), buffer_size(size), buffer_id(id), cp_rank(rank),
      cp_ver(ver) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool RecoveryEvent::runOrigin() {
  assert(event_location == EventLocation::ORIG);

  EVENT_BEGIN();

  // Load on the host side
  if (dest_rank == orig_rank) {
    PROF_SCOPED(PROF_LVL_ALL,
                std::string(toString(event_type)) + " / Loading Buffer",
                [&]() -> std::string {
                  std::stringstream details;
                  details << "{";
                  details << "\"buffer_id\":" << buffer_id << ",";
                  details << "\"buffer_size\":" << buffer_size << ",";
                  details << "\"rank_saved\":" << cp_rank << ",";
                  details << "\"cp_version\":" << cp_ver;
                  details << "}";
                  return details.str();
                }());
    if (ft_handler != nullptr) {
      ft_handler->cpLoadStart();
      ft_handler->cpLoadMem(buffer_id, cp_rank, cp_ver, buffer_size,
                            sizeof(char), reinterpret_cast<void *>(buffer_ptr));
      ft_handler->cpLoadEnd();
    }
  } else {
    // Send address to load the buffer
    MPI_Isend(&buffer_ptr, sizeof(uintptr_t), MPI_BYTE, dest_rank, mpi_tag,
              target_comm, getNextRequest(dest_rank));

    // Send the size of the buffer in bytes
    MPI_Isend(&buffer_size, sizeof(int64_t), MPI_BYTE, dest_rank, mpi_tag,
              target_comm, getNextRequest(dest_rank));

    // Send the buffer info
    buffer_info = std::vector<int>({buffer_id, cp_rank, cp_ver});
    MPI_Isend(buffer_info.data(), 3 * sizeof(int), MPI_BYTE, dest_rank, mpi_tag,
              target_comm, getNextRequest(dest_rank));

    // Event completion notification
    MPI_Irecv(nullptr, 0, MPI_BYTE, dest_rank, mpi_tag, target_comm,
              getNextRequest(dest_rank));
  }

  EVENT_END();
}

RecoveryEvent::RecoveryEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                             std::vector<int> dest_ranks)
    : BaseEvent(EventLocation::DEST, EventType::RECOVERY, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is_packable*/) {}

bool RecoveryEvent::runDestination() {
  assert(event_location == EventLocation::DEST);

  EVENT_BEGIN();

  MPI_Irecv(&buffer_ptr, sizeof(uintptr_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));
  MPI_Irecv(&buffer_size, sizeof(uint64_t), MPI_BYTE, orig_rank, mpi_tag,
            target_comm, getNextRequest(dest_rank));
  buffer_info.resize(3);
  MPI_Irecv(buffer_info.data(), 3 * sizeof(int32_t), MPI_BYTE, orig_rank,
            mpi_tag, target_comm, getNextRequest(dest_rank));

  EVENT_PAUSE_FOR_REQUESTS();

  {
    PROF_SCOPED(PROF_LVL_ALL,
                std::string(toString(event_type)) + " / Loading Buffer",
                [&]() -> std::string {
                  std::stringstream details;
                  details << "{";
                  details << "\"buffer_id\":" << buffer_info[0] << ",";
                  details << "\"buffer_size\":" << buffer_size << ",";
                  details << "\"rank_saved\":" << buffer_info[1] << ",";
                  details << "\"cp_version\":" << buffer_info[2];
                  details << "}";
                  return details.str();
                }());
    if (ft_handler != nullptr) {
      ft_handler->cpLoadStart();
      ft_handler->cpLoadMem(buffer_info[0], buffer_info[1], buffer_info[2],
                            buffer_size, sizeof(char),
                            reinterpret_cast<void *>(buffer_ptr));
      ft_handler->cpLoadEnd();
    }
  }

  // Event completion notification
  MPI_Isend(nullptr, 0, MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

// Sync Event implementation
// =============================================================================
SyncEvent::SyncEvent(EventPtr &target_event)
    : BaseEvent(EventLocation::DEST, EventType::SYNC,
                EventSystem::TAG_MAX_VALUE, 0, 0, {0}, false /*is_packable*/),
      target_event(target_event) {}

bool SyncEvent::runOrigin() { return true; }

bool SyncEvent::runDestination() {
  EVENT_BEGIN();

  while (!target_event->isDone()) {
    EVENT_PAUSE();
  }

  EVENT_END();
}

// Exit Event implementation
// =============================================================================
ExitEvent::ExitEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                     std::vector<int> dest_ranks)
    : BaseEvent(EventLocation::ORIG, EventType::EXIT, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is_packable*/) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool ExitEvent::runOrigin() {
  assert(event_location == EventLocation::ORIG);

  EVENT_BEGIN();

  // Event completion notification
  MPI_Irecv(nullptr, 0, MPI_BYTE, dest_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

ExitEvent::ExitEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                     std::vector<int> dest_ranks,
                     std::atomic<EventSystemState> *es_state)
    : BaseEvent(EventLocation::DEST, EventType::EXIT, mpi_tag, target_comm,
                orig_rank, dest_ranks, false /*is_packable*/),
      es_state(es_state) {
  assertm(dest_ranks.size() == 1,
          "Point-to-point event must have one destination rank only");
  dest_rank = dest_ranks.front();
}

bool ExitEvent::runDestination() {
  assert(event_location == EventLocation::DEST);

  EventSystemState old_state;

  EVENT_BEGIN();

  old_state = es_state->exchange(EventSystemState::EXITED);
  assertm(old_state != EventSystemState::EXITED,
          "Exit event received multiple times");

  // Event completion notification
  MPI_Isend(nullptr, 0, MPI_BYTE, orig_rank, mpi_tag, target_comm,
            getNextRequest(dest_rank));

  EVENT_END();
}

// Event Queue implementation
// =============================================================================
EventQueue::EventQueue() : base_queue(), base_queue_mutex(), can_pop_cv() {}

size_t EventQueue::size() {
  std::lock_guard<std::mutex> lock(base_queue_mutex);
  return base_queue.size();
}

void EventQueue::push(EventPtr event) {
  {
    std::unique_lock<std::mutex> lock(base_queue_mutex);
    base_queue.push(event);
  }

  // Notifies a thread possibly blocked by an empty queue.
  can_pop_cv.notify_one();
}

EventPtr EventQueue::pop() {
  EventPtr target_event = nullptr;

  {
    std::unique_lock<std::mutex> lock(base_queue_mutex);

    // Waits for at least one item to be pushed.
    while (base_queue.empty()) {
      const bool has_new_event = can_pop_cv.wait_for(
          lock, std::chrono::microseconds(config::event_polling_rate),
          [&] { return !base_queue.empty(); });

      if (!has_new_event) {
        return nullptr;
      }
    }

    assertm(!base_queue.empty(), "Queue was empty on pop operation.");

    target_event = base_queue.front();
    base_queue.pop();
  }

  return target_event;
}

// Event System implementation
// =============================================================================
// Event System statics.
MPI_Comm EventSystem::gate_thread_comm = MPI_COMM_NULL;
MPI_Comm EventSystem::collective_comm = MPI_COMM_NULL;
int32_t EventSystem::TAG_MAX_VALUE = 0;
RTLDeviceInfoTy *EventSystem::device_info = nullptr;

EventSystem::EventSystem(const MPI_Comm &base_comm,
                         RTLDeviceInfoTy *device_info_p)
    : event_comm_pool(), world_size(0), local_rank(-1), event_counter(0),
      es_state(EventSystemState::CREATED) {
  // Initialize event system profiler
  PROF_BEGIN(PROF_LVL_USER, "Event System Execution");
  PROF_SCOPED(PROF_LVL_ALL, "Event System / Construction");

  // Read environment parameters
  if (const char *env_str = std::getenv("OMPCLUSTER_MPI_FRAGMENT_SIZE")) {
    config::mpi_fragment_size = std::stoi(env_str);
    assertm(config::mpi_fragment_size >= 1,
            "Maximum MPI buffer size must be a least 1");
    assertm(config::mpi_fragment_size < std::numeric_limits<int>::max(),
            "Maximum MPI buffer size must be less then the largest int "
            "value (MPI restrictions)");
  }

  if (const char *env_str = std::getenv("OMPCLUSTER_PACKING_THRESHOLD")) {
    config::packing_threshold = std::stoi(env_str);
    assertm(config::packing_threshold >= 0,
            "Packing threshold can not be a negative number.");
  }

  if (const char *env_str = std::getenv("OMPCLUSTER_NUM_EXEC_EVENT_HANDLERS")) {
    config::num_exec_event_handlers = std::stoi(env_str);
    assertm(config::num_exec_event_handlers >= 1,
            "At least one exec event handler should be spawned");
  }

  if (const char *env_str = std::getenv("OMPCLUSTER_NUM_DATA_EVENT_HANDLERS")) {
    config::num_data_event_handlers = std::stoi(env_str);
    assertm(config::num_data_event_handlers >= 1,
            "At least one data event handler should be spawned");
  }

  if (const char *env_str = std::getenv("OMPCLUSTER_EVENT_POLLING_RATE")) {
    config::event_polling_rate = std::stoi(env_str);
    assertm(config::event_polling_rate >= 0,
            "Event system polling rate should not be negative");
  }

  if (const char *env_str = std::getenv("OMPCLUSTER_NUM_EVENT_COMM")) {
    config::num_event_comm = std::stoi(env_str);
    assertm(config::num_event_comm >= 1,
            "At least on communicator need to be spawned");
  }

  // Set device info
  device_info = device_info_p;

  createLocalMPIContext(base_comm);

  // Initialize packed events custom MPI Datatypes.
  PackedEvent::initMPICustomDatatype();

  // Setups the event system local variables.
  exec_event_handlers.resize(config::num_exec_event_handlers);
  data_event_handlers.resize(config::num_data_event_handlers);

  DPES("System started at rank %d:\n"
       " - Number of exec event handlers: %d\n"
       " - Number of data event handlers: %d\n"
       " - Fragment size: %lu\n"
       " - Maximum packable size: %lu\n"
       " - Event handler polling rate: %dus\n",
       local_rank, config::num_exec_event_handlers,
       config::num_data_event_handlers, config::mpi_fragment_size,
       config::packing_threshold, config::event_polling_rate);
}

EventSystem::~EventSystem() {
  PROF_SCOPED(PROF_LVL_ALL, "Event System / Destruction");

  // Ensures that all process are destroying the Event System at the same time.
  // MPI_Barrier tries to repair to comm, if it succeed, the function will
  // return ft::FT_SUCCESS_NEW_COMM, meaning we should get the new comm.
  if (MPI_Barrier(gate_thread_comm) == ft::FT_SUCCESS_NEW_COMM)
    gate_thread_comm = ft_handler->getMainComm();

  // Ensures that the Event Handlers are stopped.
  es_state = EventSystemState::EXITED;
  for (auto &thread : exec_event_handlers) {
    if (thread.joinable()) {
      thread.join();
    }
  }
  for (auto &thread : data_event_handlers) {
    if (thread.joinable()) {
      thread.join();
    }
  }

  // Free custom MPI datatypes
  PackedEvent::freeMPICustomDatatype();

  destroyLocalMPIContext();

  // Finalize event system profiler.
  PROF_END(PROF_LVL_USER);

  DPES("System finalized at rank %d\n", local_rank);
}

// Assign FT pointer to a handler in event system, so it can be accessed in this
// scope
void EventSystem::assignFT(ft::FaultTolerance *fault_handler) {
  ft_handler = fault_handler;
  // This is a especial case just for failure injection, will be changed when
  // changing the failure injection in the future
  if (ft_handler == nullptr)
    es_state = EventSystemState::EXITED;
}

EventPtr EventSystem::createPackedEvent(EventPtr event) {
  EventPtr packed_event = nullptr;
  switch (event->event_type) {
  case EventType::SUBMIT:
    packed_event = createEvent<PackedSubmitEvent>(event->dest_ranks, event);
    break;
  case EventType::RETRIEVE:
    packed_event = createEvent<PackedRetrieveEvent>(event->dest_ranks, event);
    break;
  case EventType::EXCHANGE:
    packed_event = createEvent<PackedExchangeEvent>(event->dest_ranks, event);
    break;
  case EventType::BCAST:
    packed_event = createEvent<PackedBcastEvent>(event->dest_ranks, event);
    break;
  default:
    assertm(false, "Tried to pack non-packable event.");
    break;
  case event_system::EventType::DYNBCAST:
    packed_event = createEvent<event_system::PackedDynBcastEvent>(
        event->dest_ranks, event);
  }
  assertm(packed_event, "Function expects to receive a valid packable event.");

  return packed_event;
}

void EventSystem::runEventHandler(int handler_idx, const char *thread_name,
                                  EventQueue *event_queue) {
  DPES("%s thread %d started at rank %d\n", thread_name, handler_idx,
       local_rank);

  PROF_INIT_THD(std::string(thread_name) + " " + std::to_string(handler_idx));
  PROF_SCOPED(PROF_LVL_ALL, "Event Handler Execution");

  while (es_state == EventSystemState::RUNNING || event_queue->size() > 0) {
    EventPtr event = event_queue->pop();

    // Re-checks the stop condition when no event was found.
    if (event == nullptr) {
      continue;
    }

    assertm(event != nullptr, "Popped event must not be a nullptr");

    if (event->getEventState() == EventState::CREATED) {
      DPES("%s [%d] at rank %d received a new event\n"
           " - Event type: %s\n"
           " - Event location: %s\n"
           " - Destination rank: %s\n"
           " - Source rank: %d\n"
           " - Tag: %d\n",
           thread_name, handler_idx, local_rank, toString(event->event_type),
           toString(event->event_location), toString(event->dest_ranks).c_str(),
           event->orig_rank, event->mpi_tag);
    }

    event->progress();

    if (!event->isDone()) {
      event_queue->push(event);
    }
  }
}

void EventSystem::runGateThread() {
  PROF_SCOPED(PROF_LVL_ALL, "Gate Thread Execution");

  // Updates the event state and spawns the event handlers
  es_state = EventSystemState::RUNNING;
  for (size_t idx = 0; idx < exec_event_handlers.size(); idx++) {
    exec_event_handlers[idx] =
        std::thread(&EventSystem::runEventHandler, this, idx,
                    "Execute Event Handler", &exec_event_queue);
  }
  for (size_t idx = 0; idx < data_event_handlers.size(); idx++) {
    data_event_handlers[idx] =
        std::thread(&EventSystem::runEventHandler, this, idx,
                    "Data Event Handler", &data_event_queue);
  }

  // Executes the gate thread logic
  while (es_state == EventSystemState::RUNNING) {
    // Checks for new incoming event requests.
    MPI_Message event_req_msg;
    MPI_Status event_status;
    int has_received = false;
    MPI_Improbe(MPI_ANY_SOURCE, static_cast<int>(ControlTags::EVENT_REQUEST),
                gate_thread_comm, &has_received, &event_req_msg,
                MPI_STATUS_IGNORE);

    // If none was received, wait for `event_polling_rate`us for the next
    // check.
    if (!has_received) {
      std::this_thread::sleep_for(
          std::chrono::microseconds(config::event_polling_rate));
      continue;
    }

    // Acquires the event information from the received request, which are:
    // - Event type
    // - Event tag
    // - Target comm
    // - Event source rank
    uint32_t event_info[2];
    MPI_Mrecv(event_info, 2, MPI_UINT32_T, &event_req_msg, &event_status);
    const EventType event_type = static_cast<EventType>(event_info[0]);
    const uint32_t event_tag = event_info[1];
    auto &event_comm = getNewEventComm(event_tag);
    const int orig_rank = event_status.MPI_SOURCE;

    // Creates a new receive event of 'event_type' type.
    EventPtr event;
    switch (event_type) {
    case EventType::DEBUG:
      event = std::make_shared<DebugEvent>(event_tag, event_comm, orig_rank,
                                           std::vector<int>{local_rank});
      break;
    case EventType::ALLOC:
      event = std::make_shared<AllocEvent>(event_tag, event_comm, orig_rank,
                                           std::vector<int>{local_rank});
      break;
    case EventType::DELETE:
      event = std::make_shared<DeleteEvent>(event_tag, event_comm, orig_rank,
                                            std::vector<int>{local_rank});
      break;
    case EventType::RETRIEVE:
      event = std::make_shared<RetrieveEvent>(event_tag, event_comm, orig_rank,
                                              std::vector<int>{local_rank});
      break;
    case EventType::SUBMIT:
      event = std::make_shared<SubmitEvent>(event_tag, event_comm, orig_rank,
                                            std::vector<int>{local_rank});
      break;
    case EventType::EXCHANGE:
      event = std::make_shared<ExchangeEvent>(event_tag, event_comm, orig_rank,
                                              std::vector<int>{local_rank});
      break;
    case EventType::PACKED_SUBMIT:
      event = std::make_shared<PackedSubmitEvent>(
          event_tag, event_comm, orig_rank, std::vector<int>{local_rank});
      break;
    case EventType::PACKED_RETRIEVE:
      event = std::make_shared<PackedRetrieveEvent>(
          event_tag, event_comm, orig_rank, std::vector<int>{local_rank});
      break;
    case EventType::PACKED_EXCHANGE:
      event = std::make_shared<PackedExchangeEvent>(
          event_tag, event_comm, orig_rank, std::vector<int>{local_rank});
      break;
    case EventType::PACKED_BCAST:
      event = std::make_shared<PackedBcastEvent>(
          event_tag, event_comm, orig_rank, std::vector<int>{local_rank});
      break;
    case EventType::PACKED_DYNBCAST:
      event = std::make_shared<PackedDynBcastEvent>(
          event_tag, event_comm, orig_rank, std::vector<int>{local_rank});
      break;
    case EventType::EXECUTE:
      event = std::make_shared<ExecuteEvent>(event_tag, event_comm, orig_rank,
                                             std::vector<int>{local_rank});
      break;
    case EventType::BCAST:
      event = std::make_shared<BcastEvent>(event_tag, event_comm, orig_rank,
                                           std::vector<int>{local_rank});
      break;
    case EventType::REGISTERCPPTRS:
      event = std::make_shared<RegisterCPPtrs>(event_tag, event_comm, orig_rank,
                                               std::vector<int>{local_rank});
      break;
    case EventType::CHECKPOINT:
      event = std::make_shared<CheckpointEvent>(
          event_tag, event_comm, orig_rank, std::vector<int>{local_rank});
      break;
    case EventType::RECOVERY:
      event = std::make_shared<RecoveryEvent>(event_tag, event_comm, orig_rank,
                                              std::vector<int>{local_rank});
      break;
    case EventType::DYNBCAST:
      event = std::make_shared<DynBcastEvent>(event_tag, event_comm, orig_rank,
                                              std::vector<int>{local_rank});
      break;
    case EventType::EXIT:
      event =
          std::make_shared<ExitEvent>(event_tag, event_comm, orig_rank,
                                      std::vector<int>{local_rank}, &es_state);
      break;
    case EventType::SYNC:
      assertm(false, "Trying to create a local event on a remote node");
    }

    assertm(event != nullptr, "Created event must not be a nullptr");
    assertm(event->event_location == EventLocation::DEST,
            "Gate thread must receive only receive events");

    if (event_type == EventType::EXECUTE) {
      exec_event_queue.push(event);
    } else {
      data_event_queue.push(event);
    }
  }

  assertm(es_state == EventSystemState::EXITED,
          "Event State should be EXITED after receiving an Exit event");

  // Waits for the Event Handler threads.
  for (auto &thread : exec_event_handlers) {
    if (thread.joinable()) {
      thread.join();
    } else {
      assertm(false, "Execute Event Handler threads not joinable at the end of "
                     "gate thread logic.");
    }
  }

  for (auto &thread : data_event_handlers) {
    if (thread.joinable()) {
      thread.join();
    } else {
      assertm(false, "Data Event Handler threads not joinable at the end of "
                     "gate thread logic.");
    }
  }
}

// Creates a new event tag of at least 'FIRST_EVENT' value.
// Tag values smaller than 'FIRST_EVENT' are reserved for control
// communication between the event systems of different MPI processes.
int EventSystem::createNewEventTag() {
  uint32_t tag = 0;

  do {
    tag = event_counter.fetch_add(1) % TAG_MAX_VALUE;
  } while (tag < static_cast<int>(ControlTags::FIRST_EVENT));

  return tag;
}

MPI_Comm &EventSystem::getNewEventComm(int mpi_tag) {
  // Retrieve a comm using a round-robin strategy around the event's mpi tag.
  return event_comm_pool[mpi_tag % event_comm_pool.size()];
}

void EventSystem::createLocalMPIContext(const MPI_Comm &base_comm) {
  int mpi_error = MPI_SUCCESS;

  // Create gate thread comm.
  mpi_error = MPI_Comm_dup(base_comm, &gate_thread_comm);
  CHECK_DP(mpi_error == MPI_SUCCESS,
           "Failed to create gate thread MPI comm with error %d\n", mpi_error);

  // Create collective operation comm.
  mpi_error = MPI_Comm_dup(base_comm, &collective_comm);
  CHECK_DP(mpi_error == MPI_SUCCESS,
           "Failed to create gate thread MPI comm with error %d\n", mpi_error);

  // Create event comm pool.
  event_comm_pool.resize(config::num_event_comm, MPI_COMM_NULL);
  for (auto &comm : event_comm_pool) {
    MPI_Comm_dup(base_comm, &comm);
    CHECK_DP(mpi_error == MPI_SUCCESS,
             "Failed to create MPI comm pool with error %d\n", mpi_error);
  }

  // Get local MPI process description.
  mpi_error = MPI_Comm_rank(gate_thread_comm, &local_rank);
  CHECK_DP(mpi_error == MPI_SUCCESS,
           "Failed to acquire the local MPI rank with error %d\n", mpi_error);

  mpi_error = MPI_Comm_size(gate_thread_comm, &world_size);
  CHECK_DP(mpi_error == MPI_SUCCESS,
           "Failed to acquire the world size with error %d\n", mpi_error);

  // Get max value for MPI tags.
  MPI_Aint *value = nullptr;
  int flag = 0;
  mpi_error = MPI_Comm_get_attr(gate_thread_comm, MPI_TAG_UB, &value, &flag);
  assert(flag == 1);
  CHECK_DP(mpi_error == MPI_SUCCESS,
           "Failed to acquire the MPI max tag value with error %d\n",
           mpi_error);
  TAG_MAX_VALUE = *value;
  assert(TAG_MAX_VALUE >= 32767);
}

void EventSystem::destroyLocalMPIContext() {
  int mpi_error = MPI_SUCCESS;

  // Note: We don't need to assert here since application part of the program
  // was finished.
  // Free gate thread comm.
  mpi_error = MPI_Comm_free(&gate_thread_comm);
  if (mpi_error != MPI_SUCCESS) {
    DPES("Failed to destroy the gate thread MPI comm with error %d\n",
         mpi_error);
  }

  // Free collective operation comm.
  mpi_error = MPI_Comm_free(&collective_comm);
  if (mpi_error != MPI_SUCCESS) {
    DPES("Failed to destroy the collective operation comm with error %d\n",
         mpi_error);
  }

  // Free event comm pool.
  for (auto &comm : event_comm_pool) {
    MPI_Comm_free(&comm);
    if (mpi_error != MPI_SUCCESS) {
      DPES("Failed to destroy the gate thread MPI comm with error %d\n",
           mpi_error);
    }
  }
  event_comm_pool.resize(0);
}

} // namespace event_system
