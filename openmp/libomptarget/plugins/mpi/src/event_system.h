//===------- event_system.h - Concurrent MPI communicaiton ------*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file contains the declarations of the MPI Event System used by the MPI
// target.
//
//===----------------------------------------------------------------------===//

#ifndef _OMPTARGET_OMPCLUSTER_EVENT_SYSTEM_H_
#define _OMPTARGET_OMPCLUSTER_EVENT_SYSTEM_H_

#include <cstdint>

#define MPICH_SKIP_MPICXX
#include <mpi.h>

#include <atomic>
#include <future>
#include <list>
#include <map>
#include <memory>
#include <queue>
#include <string>
#include <thread>
#include <type_traits>
#include <vector>

#include "ft.h"

// External forward declarations.
// =============================================================================
class RTLDeviceInfoTy;

namespace ft {
class FaultTolerance;
} // namespace ft

namespace event_system {

// Internal forward declarations and type aliases.
// =============================================================================
class BaseEvent;
enum class EventSystemState;

/// Automaticaly managed event pointer.
///
/// \note: Every event must always be accessed/stored in a shared_ptr structure.
/// This allows for automatic memory management among the many threads of the
/// libomptarget runtime.
using EventPtr = std::shared_ptr<BaseEvent>;

// Event Types
// =============================================================================
/// The event location.
///
/// Enumerates whether an event is executing at its two possible locations:
/// its origin or its destination.
enum class EventLocation : bool { ORIG = 0, DEST };

/// The event type (type of action it will performed).
///
/// Enumerates the available events. Each enum item should be accompanied by an
/// event class derived from BaseEvent. All the events are executed at a remote
/// MPI process target by the event.
enum class EventType : int {
  // Debug.
  DEBUG = 0, // Prints a message at the remote process.

  // Memory management.
  ALLOC,  // Allocates a buffer at the remote process.
  DELETE, // Deletes a buffer at the remote process.

  // Data movement.
  RETRIEVE, // Receives a buffer data from a remote process.
  SUBMIT,   // Sends a buffer data to a remote process.
  BCAST,    // Sends a buffer data to all remote processes.
  DYNBCAST, // Sends a buffer data to all remote processes, dynamically
            // propagating from one remote process to the other.
  EXCHANGE, // Exchange a buffer between two remote processes.

  // Packed data movement.
  PACKED_RETRIEVE, // Receives a packed buffer data from a set of Retrieve
                   // events from a single remote process.
  PACKED_SUBMIT, // Sends a packed buffer data from a set of Submit events to a
                 // single remote process.
  PACKED_BCAST,  // Sends a packed buffer data from a set of Bcast events to
                 // all remote processes.
  PACKED_EXCHANGE, // Exchange a packed buffer between two remote processes.
  PACKED_DYNBCAST, // Sends a packed buffer data from a set of DynBcast events
                   // to all remote processes.

  // Target region execution.
  EXECUTE, // Executes a target region at the remote process.

  // Local event used to wait on other events.
  SYNC,

  // Fault tolerance
  REGISTERCPPTRS, // Register a list o pointers to save in the next CP
  CHECKPOINT,     // Executes checkpointing of the allocated buffers
  RECOVERY,       // Loads a buffer from checkpoint

  // Internal event system commands.
  EXIT // Stops the event system execution at the remote process.
};

/// The event execution state.
///
/// Enumerates the event states during its lifecycle through the event system.
/// New states should be added to the enum in the order they happen at the event
/// system.
enum class EventState : int {
  CREATED = 0, // Event was only create but it was not executed yet.
  EXECUTING,   // Event is currently being executed in the background and it
               // is registering new MPI resquests.
  WAITING,     // Event was executed and is now waiting on the MPI requests
               // complete.
  FAILED,      // Event failed during execution
  FINISHED     // The event and its MPI requests are completed.
};

/// EventLocation to string conversion.
///
/// \returns the string representation of \p location.
const char *toString(EventLocation location);

/// EventType to string conversion.
///
/// \returns the string representation of \p type.
const char *toString(EventType type);

/// EventState to string conversion.
///
/// \returns the string representation of \p state.
const char *toString(EventState state);

// Events
// =============================================================================

/// The base event of all event types.
///
/// This class contains both the common data stored and common procedures
/// executed at all events. New events that derive from this class must comply
/// with the following:
/// - Declare the new EventType item;
/// - Name the derived class as the concatenation of the new EventType name and
///   the "Event" word. E.g.: DEBUG event -> class DebugEvent;
/// - Implement both pure virtual functions:
///    - #runOrigin;
///    - #runDestination;
/// - Implement two constructors (one for each EventLocation) with this
///   prototype:
///
///   Event(int mpi_tag, MPI_Comm target_comm, int orig_rank, int
///   dest_rank,...);
///
class BaseEvent {
public:
  /// The only (non-child) class that can access the Event protected members.
  friend class EventSystem;

  // Event definitions.
  /// Location that the event is being executed: origin or destination ranks.
  const EventLocation event_location;
  /// Event type that represents its actions.
  const EventType event_type;
  /// MPI tag that must be used on every MPI communication of the event.
  const int mpi_tag;
  /// True if a sequence of this type of event can be replaced by an equivalent
  /// PackedEvent.
  bool is_packable = false;

  // MPI definitions.
  /// MPI communicator to be used by the event.
  const MPI_Comm target_comm;
  /// Rank of the process that created the event.
  const int orig_rank;
  /// Rank of the process that was target by the event.
  std::vector<int> dest_ranks;

protected:
  using LabelPointer = void *;
  /// Event coroutine state.
  LabelPointer resume_label = nullptr;

private:
  /// MPI non-blocking requests to be synchronized during event execution.
  std::vector<MPI_Request> pending_requests;
  std::vector<int> pending_requests_ranks;

  /// The event execution state.
  std::atomic<EventState> event_state{EventState::CREATED};

  /// Call-guard for the #progress function.
  ///
  /// This atomic ensures only one thread executes the #progress code at a time.
  std::atomic<bool> progress_guard{false};

  /// Parameters used to create the event at the destination process.
  ///
  /// \note this array is needed so non-blocking MPI messages can be used to
  /// create the event.
  uint32_t event_request_info[2] = {0, 0};

public:
  BaseEvent(BaseEvent &) = delete;

  /// Advance the progress of the event.
  ///
  /// \note calling this function once does not guarantee that the event is
  /// completely executed. One must call this function until #isDone returns
  /// true.
  void progress();

  /// Check if the event is completed.
  ///
  /// \return true if the event is completed.
  bool isDone() const;

  /// Wait for the event to be completed.
  ///
  /// Waits for the completion of the event on both the local and remote
  /// processes. This function will choose between the blocking and tasking
  /// implementations depending if the current code is inside a task or not.
  ///
  /// \note this function is almost equivalent to calling #runStage while
  /// #isDone is returning false.
  void wait();

  /// Get the current event execution state.
  ///
  /// \returns The current EventState.
  EventState getEventState() const;

protected:
  /// BaseEvent constructor.
  BaseEvent(EventLocation event_location, EventType event_type, int mpi_tag,
            MPI_Comm target_comm, int orig_rank, std::vector<int> dest_ranks,
            bool is_packable);

  /// BaseEvent default destructor.
  virtual ~BaseEvent() = default;

  /// Push a new MPI request to the request array.
  ///
  /// \returns a reference to the next available MPI request at
  /// #pending_requests.
  MPI_Request *getNextRequest(int dest_proc);

  /// Test if all requests from the pending_requests have finished.
  ///
  /// \return true if all pending_requests are completed, false otherwise.
  bool checkPendingRequests();

  /// Build a json-like description for profiling points.
  ///
  /// \returns a string with the event description.
  std::string getEventDescription();

  /// Checks if the other side of communication failed.
  ///
  /// \returns true if a failure happened, false otherwise.
  bool checkFailure();

private:
  /// Waits for the event to finished while executing other OpenMP tasks.
  void taskingWait();
  /// Waits for the event to finished by blocking the calling thread.
  void blockingWait();

  /// Sends a new event notification to the destination.
  void notifyNewEvent();

  /// Calls #runOrigin or #runDestination coroutine.
  ///
  /// \returns true if the event run coroutine has finished, false otherwise.
  bool runCoroutine();

  // Event coroutines
  /// Executes the origin side of the event locally.
  virtual bool runOrigin() = 0;
  /// Executes the destination side of the event locally.
  virtual bool runDestination() = 0;
};

/// Prints a message at a remote process.
class DebugEvent final : public BaseEvent {
private:
  /// The string to be printed remotely.
  std::string debug_msg = "";
  /// Indicate an MPI message was received.
  int has_received = false;

public:
  /// Origin constructor.
  DebugEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
             std::vector<int> dest_ranks, std::string debug_msg);

  /// Destination constructor.
  DebugEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
             std::vector<int> dest_ranks);

private:
  /// Sends the debug message.
  bool runOrigin() override;
  /// Receives the debug message and prints it locally.
  bool runDestination() override;
};

/// Allocates a buffer at a remote process.
class AllocEvent final : public BaseEvent {
private:
  /// Size of the buffer to be allocated.
  int64_t size = 0;
  /// Pointer to variable to be filled with the address of the allocated buffer.
  uintptr_t *allocated_address_ptr = nullptr;
  // Allocated address to be send back.
  uintptr_t allocated_address = 0;
  // Aux int for destination rank.
  int dest_rank;

public:
  /// Origin constructor.
  AllocEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
             std::vector<int> dest_ranks, int64_t size,
             uintptr_t *allocated_address);

  /// Destination constructor.
  AllocEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
             std::vector<int> dest_ranks);

private:
  /// Sends the size and receives the allocated address.
  bool runOrigin() override;
  /// Receives the size, allocating the data and sending its address.
  bool runDestination() override;
};

/// Frees a buffer at a remote process.
class DeleteEvent final : public BaseEvent {
private:
  /// Address of the buffer to be freed.
  uintptr_t target_address = 0;
  // Aux int for destination rank.
  int dest_rank;

public:
  /// Origin constructor.
  DeleteEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
              std::vector<int> dest_ranks, uintptr_t target_address);

  /// Destination constructor.
  DeleteEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
              std::vector<int> dest_ranks);

private:
  /// Sends the address and waits for a notification of the data deletion.
  bool runOrigin() override;
  /// Receives the address, frees it and send a completion notification.
  bool runDestination() override;
};

/// Retrieves a buffer from a remote process.
class RetrieveEvent final : public BaseEvent {
private:
  /// Address of the origin's buffer to be filled with the destination's data.
  void *orig_ptr = nullptr;
  /// Address of the destination's buffer to be retrieved.
  const void *dest_ptr = nullptr;
  /// Size of both the origin's and destination's buffers.
  int64_t size = 0;
  // Aux int for destination rank.
  int dest_rank;

public:
  /// Origin constructor.
  RetrieveEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                std::vector<int> dest_ranks, void *orig_ptr,
                const void *dest_ptr, int64_t size);

  /// Destination constructor.
  RetrieveEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                std::vector<int> dest_ranks);

private:
  /// Sends the buffer info and retries its data.
  bool runOrigin() override;
  /// Receives the buffer info and sends its data.
  bool runDestination() override;

  friend class PackedRetrieveEvent;
};

/// Send a buffer to a remote process.
class SubmitEvent final : public BaseEvent {
private:
  /// Address of the origin's buffer to be submitted.
  const void *orig_ptr = nullptr;
  /// Address of the destination's buffer to be filled with the origin's data.
  void *dest_ptr = nullptr;
  /// Size of both the origin's and destination's buffers.
  int64_t size = 0;
  // Aux int for destination rank.
  int dest_rank;

public:
  /// Origin constructor.
  SubmitEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
              std::vector<int> dest_ranks, const void *orig_ptr, void *dest_ptr,
              int64_t size);

  /// Destination constructor.
  SubmitEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
              std::vector<int> dest_ranks);

private:
  /// Sends the buffer info and then the buffer itself.
  bool runOrigin() override;
  /// Receives the buffer info and then the buffer itself.
  bool runDestination() override;

  friend class PackedSubmitEvent;
};

/// Broadcast a buffer exclusively from the host (MPI process 0).
class BcastEvent final : public BaseEvent {
private:
  /// Address of the host's buffer to be submitted.
  const void *host_ptr = nullptr;
  /// Address of the destination's buffer to be filled with the host's data.
  void *dest_ptr = nullptr;
  /// Size of the buffer to be broadcasted.
  int64_t size = 0;
  // Aux int for destination rank.
  int dest_rank;
  /// List of every TgtPtr on every process that the buffer must be written to
  std::vector<void *> tgt_ptrs;

public:
  /// Origin constructor.
  BcastEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
             std::vector<int> dest_ranks, const void *host_ptr, void *dest_ptr,
             int64_t size);

  /// Destination constructor.
  BcastEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
             std::vector<int> dest_ranks);

  /// Adds the tgt_ptr of a given rank to the tgt_ptrs vector
  void addTgtPtr(int device_rank, void *tgt_ptr);

private:
  /// Sends the buffer info and, if orig rank is the host, start the broadcast.
  bool runOrigin() override;
  /// Receives the buffer info and start the broadcast.
  bool runDestination() override;

  friend class PackedBcastEvent;
};

/// Exchange a buffer between two remote processes.
class ExchangeEvent final : public BaseEvent {
private:
  /// MPI rank of the data destination process.
  int data_dst_rank = 0;
  int data_src_rank = 0;
  /// Address of the data at the data source process.
  const void *src_ptr = nullptr;
  /// Address of the data at the data destination process.
  void *dst_ptr = nullptr;
  /// Size of both the data source's and data destination's buffers.
  int64_t size = 0;
  // Aux int for destination rank.
  int dest_rank;

  std::vector<int> data_dest_ranks;
  EventPtr remote_submit_event = nullptr;
  std::vector<int> value_test;

public:
  /// Origin constructor.
  ExchangeEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                std::vector<int> dest_ranks, int data_src_rank,
                int data_dst_rank, const void *src_ptr, void *dst_ptr,
                int64_t size);

  /// Destination constructor.
  ExchangeEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                std::vector<int> dest_ranks);

private:
  /// Sends the buffer info.
  bool runOrigin() override;
  /// Receives the buffer info and start a SubmitEvent to the dest process.
  bool runDestination() override;

  friend class PackedExchangeEvent;
};

/// Broadcast a buffer exclusively from the host (MPI process 0).
class DynBcastEvent final : public BaseEvent {
public:
  /// List of every TgtPtr on every process that the buffer must be written to
  std::vector<void *> tgt_ptrs;
  /// List of ranks to which each rank must send the data to, separated by the
  /// value of slice_marker
  std::vector<int> fwd_list;
  int64_t fwd_list_size;
  /// Separator used by fwd_list
  static const int slice_marker = -1;
  /// Address of the host's buffer to be submitted.
  void *host_ptr;
  /// Address of the destination's buffer to be filled with the host's data.
  void *dest_ptr;
  /// Size of both the data source's and data destination's buffers.
  int64_t size;
  /// List of forwarded bcasts maintained by the origin node.
  std::list<event_system::EventPtr> forwarded_bcasts;
  /// Whether the scheduling of the broadcast event is ready
  bool scheduling_ready = false;
  // Aux int for destination rank.
  int dest_rank;

public:
  /// Origin constructor.
  DynBcastEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                std::vector<int> dest_ranks, void *host_ptr, void *dest_ptr,
                int64_t size);

  /// Destination constructor.
  DynBcastEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                std::vector<int> dest_ranks);

  /// Adds the tgt_ptr of a given rank to the tgt_ptrs vector
  void addTgtPtr(int device_rank, void *tgt_ptr);

  /// Checks if a given buffer is to be broadcast.
  bool isBcastData(void *test_ptr, int test_size);

  void printBcastSchedule();

  // Implements binary tree broadcast, in which each rank forwards the data to
  // two other ranks. E.g.:
  // 0 -> 1 -> 3
  // |     \-> 4
  // |
  //  \-> 2 -> 5
  //       \-> 6
  // The scheduled result is saved on fwd_list.
  void scheduleBcast();

  /// Helper function that returns a vector of ranks to which from_rank must
  /// forward the buffer to.
  static std::vector<int> getSendList(std::vector<int> fwd_list, int from_rank);

private:
  /// Sends the buffer info and, if orig rank is the host, start the broadcast.
  bool runOrigin() override;
  /// Receives the buffer info and start the broadcast.
  bool runDestination() override;

  friend class PackedDynBcastEvent;
};

class PackedEvent : public BaseEvent {
  using BaseEvent::BaseEvent;

protected:
  /// MPI custom datatypes used to pack multiple Events metadata.
  static MPI_Datatype MPI_PACKED_SUBMIT_METADATA_T;
  static MPI_Datatype MPI_PACKED_RETRIEVE_METADATA_T;
  static MPI_Datatype MPI_PACKED_EXCHANGE_METADATA_T;
  static MPI_Datatype MPI_PACKED_BCAST_METADATA_T;

  /// List of packed events.
  std::list<EventPtr> packed_events;

public:
  /// Checks whether event packing is enabled by an env variable.
  ///
  /// \returns true if packing is enabled, false otherwise.
  static bool isPackingEnabled();

  // Packing custom datatypes management.
  /// Initialize/Allocates the MPI custom datatypes for packed events.
  static void initMPICustomDatatype();
  /// Deinitialize/Frees the MPI custom datatypes for packed events.
  static void freeMPICustomDatatype();

  // Packing/Unpacking functions.
  /// Add a new event to be later packed before their execution.
  void packEvent(EventPtr event);
  /// Unpack all the events after their execution.
  virtual void unpackEvents(){};

  /// Acquire the event type of all packed events.
  ///
  /// \returns packed event type.
  EventType packedType();
};

class PackedSubmitEvent final : public PackedEvent {
private:
  /// Packed metadata.
  struct metadata_t {
    int64_t size;
    void *dest_ptr;
  };

  /// Packed array of metadata to be sent at once.
  std::vector<metadata_t> packed_events_metadata;
  /// Packed buffer at once.
  std::vector<char> packed_buffer;

  /// Indicate an MPI message was received.
  int has_received = false;
  // Aux int for destination rank.
  int dest_rank;

public:
  // Send action constructor.
  PackedSubmitEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                    std::vector<int> dest_ranks, EventPtr event);

  // Receive action constructor.
  PackedSubmitEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                    std::vector<int> dest_ranks);

private:
  bool runOrigin() override;
  bool runDestination() override;
  void unpackEvents() override;
};

class PackedRetrieveEvent final : public PackedEvent {
private:
  /// Packed metadata.
  struct metadata_t {
    int64_t size;
    const void *dest_ptr;
  };

  /// Packed array of metadata to be sent at once.
  std::vector<metadata_t> packed_events_metadata;
  /// Packed buffer at once.
  std::vector<char> packed_buffer;

  /// Indicate an MPI message was received.
  int has_received = false;

  // Aux int for destination rank.
  int dest_rank;

public:
  // Send action constructor.
  PackedRetrieveEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                      std::vector<int> dest_ranks, EventPtr event);

  // Receive action constructor.
  PackedRetrieveEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                      std::vector<int> dest_ranks);

private:
  bool runOrigin() override;
  bool runDestination() override;
  void unpackEvents() override;
};

class PackedExchangeEvent final : public PackedEvent {
private:
  std::vector<char> metadata_buffer;
  int num_events = 0;
  int data_dst_rank = 0;
  int data_src_rank = 0;
  // Aux int for destination rank.
  int dest_rank;

public:
  PackedExchangeEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                      std::vector<int> dest_ranks, EventPtr event);

  PackedExchangeEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                      std::vector<int> dest_ranks);

private:
  bool runOrigin() override;
  bool runDestination() override;
};

class PackedDynBcastEvent final : public PackedEvent {
private:
  /// Buffers to hold packed data.
  std::vector<void *> curr_rank_ptrs;
  std::vector<char> packed_buffers;
  std::vector<char> metadata_buffer;

  std::vector<char> rcv_metadata_buffer;
  std::vector<char> rcv_packed_buffer;
  std::vector<char *> buffer_ptrs;
  std::vector<int64_t> buffer_sizes;
  std::vector<int> fwd_ranks;
  int64_t packed_buffer_size = 0;
  int64_t metadata_buffer_size = 0;
  int position = 0;

  std::vector<char> packed_buffer;
  std::vector<void *> tgt_ptrs;
  std::vector<int> packed_events_metadata;
  std::vector<int> fwd_list;
  std::list<event_system::EventPtr> forwarded_bcasts;
  // Aux int for destination rank.
  int dest_rank;

public:
  PackedDynBcastEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                      std::vector<int> dest_ranks,
                      std::shared_ptr<event_system::BaseEvent> event);

  PackedDynBcastEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                      std::vector<int> dest_ranks);

  /// Update send list before execution
  void updateSendList();

private:
  bool runOrigin() override;
  bool runDestination() override;
};

class PackedBcastEvent final : public PackedEvent {
private:
  /// Buffers to hold packed data.
  std::vector<char> packed_buffers;
  std::vector<std::vector<char>> metadata_buffers;

  std::vector<char> rcv_metadata_buffer;
  std::vector<char> rcv_packed_buffer;
  std::vector<char *> buffer_ptrs;
  std::vector<int64_t> buffer_sizes;

  // Aux int for destination rank.
  int dest_rank;
  int64_t metadata_buffer_size = 0;

public:
  // Send action constructor.
  PackedBcastEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                   std::vector<int> dest_ranks, EventPtr event);

  // Receive action constructor.
  PackedBcastEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                   std::vector<int> dest_ranks);

private:
  /// Sends the buffer info and, if orig rank is the host, start the broadcast.
  bool runOrigin() override;
  /// Receives the buffer info and start the broadcast.
  bool runDestination() override;

  void unpackEvents() override;
};

/// Executes a target region at a remote process.
class ExecuteEvent final : public BaseEvent {
private:
  /// Number of arguments of the target region.
  int32_t arg_num = 0;
  /// Arguments of the target region.
  std::vector<void *> arg_ptrs;
  /// Index of the target region.
  uint32_t target_entry_idx = -1;
  // Aux int for destination rank.
  int dest_rank;

public:
  /// Origin constructor.
  ExecuteEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
               std::vector<int> dest_ranks, int32_t arg_num, void **arg_ptrs,
               uint32_t target_entry_idx);

  /// Destination constructor.
  ExecuteEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
               std::vector<int> dest_ranks);

private:
  /// Sends the target region info and wait for the completion notification.
  bool runOrigin() override;
  /// Receives the target region info, executes it and sends the notification.
  bool runDestination() override;
};

/// Local event used to wait on other events.
class SyncEvent final : public BaseEvent {
private:
  EventPtr target_event;

public:
  /// Destination constructor.
  SyncEvent(EventPtr &target_event);

private:
  /// Does nothing.
  bool runOrigin() override;
  /// Waits for target_event to complete.
  bool runDestination() override;
};

/// Performs fault tolerance checkpointing in each process.
class RegisterCPPtrs final : public BaseEvent {
private:
  // Vector containing the ptrs addresses and sizes to save
  std::vector<uintptr_t> ptrs;
  std::vector<int64_t> sizes;
  int32_t num;

  // Aux int for destination rank.
  int dest_rank;

public:
  /// Origin constructor.
  RegisterCPPtrs(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                 std::vector<int> dest_ranks, uintptr_t *ptrs_to_register,
                 int64_t *ptrs_sizes, int32_t num_ptrs);

  /// Destination constructor.
  RegisterCPPtrs(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                 std::vector<int> dest_ranks);

private:
  /// Register the buffers to be saved on the host.
  bool runOrigin() override;
  /// Register the buffers to be saved on the devices.
  bool runDestination() override;
};

/// Performs fault tolerance checkpointing in each process.
class CheckpointEvent final : public BaseEvent {
private:
  int32_t *version; // Checkpoint version returned from FT lib
  int32_t returned_version;
  // Aux int for destination rank.
  int dest_rank;

public:
  /// Origin constructor.
  CheckpointEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                  std::vector<int> dest_ranks, int32_t *cp_version);

  /// Destination constructor.
  CheckpointEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                  std::vector<int> dest_ranks);

private:
  /// Checkpoints any buffer saved on the host.
  bool runOrigin() override;
  /// Checkpoints any buffer saved on the devices.
  bool runDestination() override;
};

/// Performs the load of a buffer in a checkpoint.
class RecoveryEvent final : public BaseEvent {
private:
  std::vector<int32_t> buffer_info;
  uintptr_t buffer_ptr; // Pointer to load the buffer
  int64_t buffer_size;  // Size of the buffer
  int32_t buffer_id;    // Id of the buffer in he checkpoint file
  int32_t cp_rank;      // Rank of the process that saved the cp
  int32_t cp_ver;       // Rank of the process that saved the cp
  // Aux int for destination rank.
  int dest_rank;

public:
  /// Origin constructor.
  RecoveryEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                std::vector<int> dest_ranks, uintptr_t tgt_ptr, int64_t size,
                int32_t rank, int32_t id, int32_t ver);

  /// Destination constructor.
  RecoveryEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
                std::vector<int> dest_ranks);

private:
  /// Waits the completion of the load on the desired device.
  bool runOrigin() override;
  /// Load the buffer on the device.
  bool runDestination() override;
};

/// Notify a remote process to stop its event system.
class ExitEvent final : public BaseEvent {
private:
  /// Pointer to the event system state.
  std::atomic<EventSystemState> *es_state = nullptr;
  // Aux int for destination rank.
  int dest_rank;

public:
  /// Origin constructor.
  ExitEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
            std::vector<int> dest_ranks);

  /// Destination constructor.
  ExitEvent(int mpi_tag, MPI_Comm target_comm, int orig_rank,
            std::vector<int> dest_ranks,
            std::atomic<EventSystemState> *es_state);

private:
  /// Just waits for the completion notification.
  bool runOrigin() override;
  /// Stops its event system and sends the notification.
  bool runDestination() override;
};

// Event Queue
// =============================================================================
/// Event queue for received events.
class EventQueue {
private:
  /// Base internal queue.
  std::queue<EventPtr> base_queue;
  /// Base queue sync mutex.
  std::mutex base_queue_mutex;

  /// Conditional variables to block popping on an empty queue.
  std::condition_variable can_pop_cv;

public:
  /// Event Queue default constructor.
  EventQueue();

  /// Gets current queue size.
  size_t size();

  /// Push an event to the queue, resizing it when needed.
  void push(EventPtr event);

  /// Pops an event from the queue, returning nullptr if the queue is empty.
  EventPtr pop();
};

// Event System
// =============================================================================

/// MPI tags used in control messages.
///
/// Special tags values used to send control messages between event systems of
/// different processes. When adding new tags, please summarize the tag usage
/// with a side comment as done below.
enum class ControlTags : int {
  EVENT_REQUEST = 0, // Used by event handlers to receive new event requests.
  FIRST_EVENT        // Tag used by the first event. Must always be placed last.
};

/// Event system execution state.
///
/// Describes the event system state through the program.
enum class EventSystemState {
  CREATED, // ES was created but it is not ready to receive new events.
  RUNNING, // ES is running and ready to receive new events.
  EXITED   // ES was stopped and receive new events.
};

/// The distributed event system.
class EventSystem {
public:
  /// The largest MPI tag allowed by its implementation.
  static int32_t TAG_MAX_VALUE;
  /// Current process device information.
  static RTLDeviceInfoTy *device_info;
  // Faul handler assignment
  void assignFT(ft::FaultTolerance *fault_handler);

  /// Communicator used by the gate thread.
  // TODO: Find a better way to share this with all the events. static is not
  // that great.
  static MPI_Comm gate_thread_comm;

  /// Communicator used by collective operations.
  static MPI_Comm collective_comm;

private:
  // MPI definitions.
  /// Communicator pool distributed over the events.
  std::vector<MPI_Comm> event_comm_pool;
  /// Number of process used by the event system.
  int world_size;
  /// The local rank of the current instance.
  int local_rank;

  /// Number of event created by the current instance.
  std::atomic<uint32_t> event_counter;

  /// Event queue between the local gate thread and event handlers.
  EventQueue exec_event_queue{};
  EventQueue data_event_queue{};

  /// Event handler threads.
  std::vector<std::thread> exec_event_handlers{};
  std::vector<std::thread> data_event_handlers{};

  /// Event System execution state.
  std::atomic<EventSystemState> es_state;

private:
  /// Function executed by the event handler threads.
  void runEventHandler(int handler_idx, const char *thread_name,
                       EventQueue *event_queue);

  /// Creates a new unique event tag for a new event.
  int createNewEventTag();

  /// Gets a comm for a new event from the comm pool.
  MPI_Comm &getNewEventComm(int mpi_tag);

public:
  /// Creates a local MPI context containing a exclusive comm for the gate
  /// thread, and a comm pool to be used internally by the events. It also
  /// acquires the local MPI process description.
  void createLocalMPIContext(const MPI_Comm &base_comm);

  /// Destroy the local MPI context and all of its comms.
  void destroyLocalMPIContext();

  /// Event System default constructor.
  EventSystem(const MPI_Comm &base_comm, RTLDeviceInfoTy *device_info);
  /// Event System default destructor.
  ~EventSystem();

  /// Creates a new event.
  ///
  /// Creates a new event of 'EventClass' type targeting the 'dest_rank'. The
  /// 'args' parameters are additional arguments that may be passed to the
  /// EventClass origin constructor.
  ///
  /// /note: since this is a template function, it must be defined in
  /// this header.
  template <class EventClass, typename... Args>
  EventPtr createEvent(std::vector<int> dest_ranks, Args &&...args);

  /// Creates a new PackedEvent from a packable event.
  EventPtr createPackedEvent(EventPtr event);

  /// Gate thread procedure.
  ///
  /// Caller thread will spawn the event handlers, execute the gate logic and
  /// wait until the event system receive an Exit event.
  void runGateThread();
};

template <class EventClass, typename... Args>
EventPtr EventSystem::createEvent(std::vector<int> dest_ranks, Args &&...args) {
  static_assert(std::is_convertible<EventClass *, BaseEvent *>::value,
                "Cannot create an event from a class that is not derived from "
                "the BaseEvent class");
  // static_assert(std::is_constructible<EventClass, Args...>::value,
  //               "Cannot create an event from given argument types");

  const int event_tag = createNewEventTag();
  auto &event_comm = getNewEventComm(event_tag);

  EventPtr event =
      std::make_shared<EventClass>(event_tag, event_comm, local_rank,
                                   dest_ranks, std::forward<Args>(args)...);

  return event;
}

} // namespace event_system

#endif // _OMPTARGET_OMPCLUSTER_EVENT_SYSTEM_H_
